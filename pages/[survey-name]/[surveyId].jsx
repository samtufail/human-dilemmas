import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import _, { shuffle } from 'lodash';

// Custom Components and Utils
import {
  SurveyIntro,
  AfterVerification,
  CQCAnswerPage,
  CQCGraphPage,
  WMQCAnswerPage,
  WMQCGraphPage,
  CQOAnswerPage,
  CQOGraphPage,
  WMQOAnswerPage,
  WMQOGraphPage,
  SurveyOutro,
  Layout,
  Loader,
} from 'components';
import { api } from 'utils/api';

export default function Survey() {
  const router = useRouter();

  const { surveyId } = router.query;
  const [active, setActive] = useState(0);
  const [section, setSection] = useState(null);
  const [showGraph, setShowGraph] = useState(false);
  const [graphData, setGraphData] = useState(null);
  const [hasGender, setHasGender] = useState(undefined);
  const [percent, setPercent] = useState(3);
  const [shareableLink, setShareableLink] = useState(false);
  const [verified, setVerified] = useState(false);
  const [questions, setQuestions] = useState([]);
  const [userData, setUserData] = useState([]);
  const [currentSurveyPage, setCurrentSurveyPage] = useState('');
  const [loading, setLoading] = useState(false);
  const [allSurveys, setAllSurveys] = useState(null);

  const user = useSelector((state) => state?.auth?.user);

  const { backFromRegister } = router.query;

  const openStatusSurveys = () => {
    const currentDate = new Date();
    const openSurveys = allSurveys?.filter((el) => {
      const publishedDate = new Date(el?.date_published);
      return el?.status === 'Open' && publishedDate <= currentDate;
    });
    return openSurveys;
  };

  // Get Survey Based on Survey ID
  useEffect(() => {
    (async () => {
      if (surveyId) {
        setLoading(true);
        const data = await api.get(`/v1/surveys/${surveyId}/`);
        setSection(data?.data);
        const questions = _?.shuffle(data?.data?.questions);
        if (data?.data) {
          const surveyId = localStorage?.getItem('surveyId');
          const activeQuestion = localStorage?.getItem('activeQuestion');
          const currentShuffle = JSON.parse(
            localStorage?.getItem('currentShuffle')
          );
          if (surveyId && activeQuestion && currentShuffle) {
            if (backFromRegister) {
              const res = await api.post(`/v1/surveys/${surveyId}/results/`, {
                qid: currentShuffle[activeQuestion - 1]?.unique_QID,
                is_verified: true,
              });
              const sharedGraphData = { data: res?.data, answer: null };
              setActive(Number(activeQuestion));
              setQuestions(currentShuffle);
              setShowGraph(true);
              setGraphData(sharedGraphData);
              localStorage.removeItem('surveyId');
              localStorage.removeItem('activeQuestion');
              localStorage.removeItem('currentShuffle');
            }
            setLoading(false);
          } else {
            setQuestions(questions);
            setLoading(false);
          }
        }
      }
    })();
  }, [router, surveyId, backFromRegister]);

  // Check if user is logged in and he do have gender and year_of_birth
  useEffect(() => {
    if (user) {
      if (user?.gender && user?.year_of_birth) {
        setHasGender(true);
      } else {
        setHasGender(false);
      }
    }
  }, [user]);

  // Set Progress Number for ProgressBar Inside Surveys
  useEffect(() => {
    if (section && active) {
      const percent = (active / section?.questions_count) * 100;
      setPercent(Math.round(percent));
    }
    if (section?.questions_count === active) {
      fetchSurveys();
    }
  }, [active, section, hasGender]);

  // Set Graph Data for Shareable Link
  useEffect(() => {
    (async () => {
      if (typeof window !== 'undefined') {
        const { verified, activeQID } = router.query;
        if (activeQID) {
          setLoading(true);
          try {
            const res = await api.post(`/v1/surveys/${surveyId}/results/`, {
              qid: activeQID,
              is_verified: verified === 'true',
            });
            const sharedGraphData = { data: res?.data, answer: null };
            if (sharedGraphData?.data) {
              const uniqueQuestionIndex = questions?.findIndex(
                (el) => el?.unique_QID === activeQID
              );
              setActive(uniqueQuestionIndex + 1);
              setShowGraph(true);
              setGraphData(sharedGraphData);
              setVerified(verified === 'true');
              setShareableLink(true);
            }
          } catch (err) {
            // console.log(err);
          } finally {
            setLoading(false);
          }
        }
      }
    })();
  }, [router, questions]);

  // Get User Data
  useEffect(() => {
    (async () => {
      if (user) {
        const d = await api.get('/v1/questions/my-attempted-questions/');
        setUserData(d?.data);
      }
    })();
  }, [user]);

  // Switch Survey Type Based on Different Parameters
  // Closed Surveys: (CQC: Closed Questions (Closed), WMQC: World Map Questions (Closed))
  // Open Surveys: (CQO: Open Questions (Open), WMQCO: World Map Questions (Open))
  useEffect(() => {
    if (section && questions) {
      const isClosedQuestion = questions[active - 1]?.type === 'Closed';
      const isWorldMapQuestion = questions[active - 1]?.type === 'World map';

      const commonCondition = active > 0 && active <= section?.questions_count;
      const isClosedSurvey = section?.status === 'Closed';
      if (isClosedSurvey) {
        if (!showGraph && commonCondition && isClosedQuestion) {
          setCurrentSurveyPage('CQCAnswerPage');
        } else if (showGraph && commonCondition && isClosedQuestion) {
          setCurrentSurveyPage('CQCGraphPage');
        } else if (!showGraph && commonCondition && isWorldMapQuestion) {
          setCurrentSurveyPage('WMQCAnswerPage');
        } else if (showGraph && commonCondition && isWorldMapQuestion) {
          setCurrentSurveyPage('WMQCGraphPage');
        }
      } else {
        if (!showGraph && commonCondition && isClosedQuestion) {
          setCurrentSurveyPage('CQOAnswerPage');
        } else if (showGraph && commonCondition && isClosedQuestion) {
          setCurrentSurveyPage('CQOGraphPage');
        } else if (!showGraph && commonCondition && isWorldMapQuestion) {
          setCurrentSurveyPage('WMQOAnswerPage');
        } else if (showGraph && commonCondition && isWorldMapQuestion) {
          setCurrentSurveyPage('WMQOGraphPage');
        }
      }
    }
  }, [section, questions, active, showGraph]);

  const fetchSurveys = async () => {
    const res = await api.get(`/v1/surveys/`);
    const filtered = res?.data?.results?.filter(
      (survey) =>
        survey?.status !== 'Closed' &&
        survey?.cover_image !== null &&
        survey?.release_date !== null
    );
    setAllSurveys(shuffle(filtered));
  };
  // If questions are not loaded or there is any other loading instance then return loader
  if (loading || !section || !questions?.length)
    return (
      <div className="w-screen h-screen flex items-center justify-center">
        <Loader />
      </div>
    );

  // Survey Intro If Active is 0
  if (!showGraph && active === 0) {
    return (
      <Layout title={section?.name}>
        <div className="WorldMap-scree-main py-[40px] pt-[15px]">
          <SurveyIntro
            image={section?.intro_screen_image}
            author={section?.author}
            intro={section?.intro_screen_text}
            active={active}
            setActive={setActive}
            type={section?.status === 'Closed'}
          />
        </div>
      </Layout>
    );
  }

  // Survey Outro if Active is more than current questions count
  if (!showGraph && active > section?.questions_count) {
    return (
      <Layout title={section?.name}>
        <SurveyOutro
          openStatusSurveys={openStatusSurveys()}
          setActive={setActive}
        />
      </Layout>
    );
  }

  // Show After Verification if Year of Birth and Gender is not added to logged in users
  if (hasGender !== undefined && hasGender === false) {
    return (
      <Layout title={section?.name}>
        <AfterVerification
          setHasGender={setHasGender}
          active={active}
          section={section}
        />
      </Layout>
    );
  }

  // Common Props for All Answer Pages
  const commonAnswerPageProps = {
    active,
    setShowGraph,
    setGraphData,
    surveyId,
    percent,
    userData,
  };

  // Common Props for All Graph Pages
  const commonGraphPageProps = {
    graphData,
    setGraphData,
    setShowGraph,
    active,
    setActive,
    surveyId,
    shareableLink,
    verified,
  };

  return (
    <Layout title={section?.name}>
      <div className="Question_world_map_scree scrollbar-style py-[40px] pt-[15px]">
        <div className="SurveyOpen-scree-main">
          {/* Closed Questions (Closed) Answer Page */}
          {currentSurveyPage === 'CQCAnswerPage' ? (
            <CQCAnswerPage questions={questions} {...commonAnswerPageProps} />
          ) : (
            <></>
          )}
          {/* Closed Questions (Closed) Graph Page */}
          {currentSurveyPage === 'CQCGraphPage' ? (
            <CQCGraphPage questions={questions} {...commonGraphPageProps} />
          ) : (
            <></>
          )}
          {/* Closed Questions (Open) Answer Page */}
          {currentSurveyPage === 'CQOAnswerPage' ? (
            <CQOAnswerPage questions={questions} {...commonAnswerPageProps} />
          ) : (
            <></>
          )}
          {/* Closed Questions (Open) Graph Page */}
          {currentSurveyPage === 'CQOGraphPage' ? (
            <CQOGraphPage questions={questions} {...commonGraphPageProps} />
          ) : (
            <></>
          )}
          {/* World Map Questions (Closed) Answer Page */}
          {currentSurveyPage === 'WMQCAnswerPage' ? (
            <WMQCAnswerPage questions={questions} {...commonAnswerPageProps} />
          ) : (
            <></>
          )}
          {/* World Map Questions (Closed) Graph Page */}
          {currentSurveyPage === 'WMQCGraphPage' ? (
            <WMQCGraphPage questions={questions} {...commonGraphPageProps} />
          ) : (
            <></>
          )}
          {/* World Map Questions (Open) Answer Page */}
          {currentSurveyPage === 'WMQOAnswerPage' ? (
            <WMQOAnswerPage questions={questions} {...commonAnswerPageProps} />
          ) : (
            <></>
          )}
          {/* World Map Questions (Open) Graph Page */}
          {currentSurveyPage === 'WMQOGraphPage' ? (
            <WMQOGraphPage questions={questions} {...commonGraphPageProps} />
          ) : (
            <></>
          )}
        </div>
      </div>
    </Layout>
  );
}
