import { useRouter } from 'next/router';

// Custom Components and Utils
import { Layout, PanelistsOutro } from 'components';

import { selectionMachineSurveys, versionControlFlow } from 'utils/data';

export default function PanelistsOutroScreen() {
  const router = useRouter();

  const version = router.query.version;
  const controlFlow = versionControlFlow[version];
  const section = selectionMachineSurveys[controlFlow?.surveyIndex];

  return (
    <Layout title={section?.name}>
      <PanelistsOutro type={section?.status === 'Closed'} />
    </Layout>
  );
}
