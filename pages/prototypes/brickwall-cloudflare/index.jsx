import React, { useEffect, useState } from 'react';
import { BrickNav, Heading, Layout, Loader, ZoomButton } from 'components';
import { BrickWall, MobileBrickWall } from 'sections/Surveys';
import { BrickWallPortal } from 'HOC';
import { Genre } from 'sections/Surveys/Genre.section';
import Head from 'next/head';
import { api, cloudflareSurveys as surveys } from 'utils';
import { useMediaQuery } from 'react-responsive';

export default function Brick_wall() {
  const [zoomLevel, setZoomLevel] = useState(8);
  const [releaseDate, setReleaseDate] = useState(false);
  const [active, setActive] = useState('release');
  const [activeSurveys, setActiveSurveys] = useState([]);
  const [genres, setGenres] = useState([]);
  const [loading, setLoading] = useState(false);
  const [releaseSurveys, setReleaseSurveys] = useState([]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const res = await api.get('/v1/surveys/genres-with-surveys-set');
      //   const surveys = await api.get('/v1/surveys');
      setReleaseSurveys(surveys);
      setGenres(res?.data?.results);
      setActiveSurveys(res?.data?.results);
      setLoading(false);
    })();
  }, []);

  const isMoreThanLargeMobile = useMediaQuery({
    query: '(min-width: 1026px)',
  });

  const isLessThanMobile = useMediaQuery({
    query: '(max-width: 376px)',
  });

  const isMoreThanNestHub = useMediaQuery({
    query: '(min-width: 1270px)',
  });

  return (
    <>
      {loading || (!genres.length && !releaseSurveys.length) ? (
        <>
          <Head>
            <title>Questions - Human Dilemmas</title>
          </Head>
          <div className="w-full h-[100vh] flex items-center justify-center">
            <Loader />
          </div>
        </>
      ) : (
        <>
          {active === 'genre' ? (
            <Layout title="Genres">
              <div
                className={`${
                  isMoreThanLargeMobile ? 'overflow-x-auto' : 'overflow-x-auto'
                }`}
              >
                <div className="mdbw:mt-[60px] px-[45px] py-[0px]">
                  <div className="flex justify-between items-center">
                    <Heading
                      releaseDate={releaseDate}
                      setReleaseDate={setReleaseDate}
                      active={active}
                      setActive={setActive}
                      setActiveSurveys={setActiveSurveys}
                      genres={genres}
                    />
                    <ZoomButton
                      zoomLevel={zoomLevel}
                      setZoomLevel={setZoomLevel}
                      active={active}
                      isMoreThanLargeMobile={isMoreThanLargeMobile}
                    />
                  </div>
                </div>
                {active === 'genre' ? (
                  <div className="min-h-[calc(100vh_-_170px)] mdbw:min-h-[calc(100vh_-_235px)]">
                    {activeSurveys?.length ? (
                      activeSurveys?.map((survey) => (
                        <Genre
                          key={survey?.unique_GID}
                          activeSurveys={survey}
                          isMoreThanLargeMobile={isMoreThanLargeMobile}
                          isLessThanMobile={isLessThanMobile}
                        />
                      ))
                    ) : (
                      <Genre
                        activeSurveys={activeSurveys}
                        isMoreThanLargeMobile={isMoreThanLargeMobile}
                        isLessThanMobile={isLessThanMobile}
                      />
                    )}
                  </div>
                ) : null}
              </div>
            </Layout>
          ) : (
            <BrickWallPortal>
              {/* <Layout> */}
              <Head>
                <title>Questions - Human Dilemmas</title>
                <meta
                  name="description"
                  content="With brick wall you can search all the questions available in the app easily by using mini-map"
                />
                <meta name="google" content="notranslate" />
              </Head>
              <BrickNav />
              <div className="surveys-main scrollbar-hide">
                <div className="px-[45px] py-[0px]">
                  <div className="flex justify-between items-center">
                    <Heading
                      releaseDate={releaseDate}
                      setReleaseDate={setReleaseDate}
                      active={active}
                      setActive={setActive}
                      setActiveSurveys={setActiveSurveys}
                      genres={genres}
                    />
                    <ZoomButton
                      zoomLevel={zoomLevel}
                      setZoomLevel={setZoomLevel}
                      active={active}
                      isMoreThanLargeMobile={isMoreThanLargeMobile}
                    />
                  </div>
                </div>
                {/* Brickwall */}
                {active === 'release' ? (
                  <div>
                    {isMoreThanNestHub ? (
                      <>
                        {zoomLevel === 8 ? (
                          <BrickWall
                            zoomLevel={zoomLevel}
                            isMoreThanLargeMobile={isMoreThanLargeMobile}
                            isLessThanMobile={isLessThanMobile}
                            releaseSurveys={releaseSurveys}
                          />
                        ) : null}
                        {zoomLevel === 7 ? (
                          <BrickWall
                            zoomLevel={zoomLevel}
                            isMoreThanLargeMobile={isMoreThanLargeMobile}
                            isLessThanMobile={isLessThanMobile}
                            releaseSurveys={releaseSurveys}
                          />
                        ) : null}
                        {zoomLevel === 6 ? (
                          <BrickWall
                            zoomLevel={zoomLevel}
                            isMoreThanLargeMobile={isMoreThanLargeMobile}
                            isLessThanMobile={isLessThanMobile}
                            releaseSurveys={releaseSurveys}
                          />
                        ) : null}
                        {zoomLevel === 5 ? (
                          <BrickWall
                            zoomLevel={zoomLevel}
                            isMoreThanLargeMobile={isMoreThanLargeMobile}
                            isLessThanMobile={isLessThanMobile}
                            releaseSurveys={releaseSurveys}
                          />
                        ) : null}
                        {zoomLevel === 4 ? (
                          <BrickWall
                            zoomLevel={zoomLevel}
                            isMoreThanLargeMobile={isMoreThanLargeMobile}
                            isLessThanMobile={isLessThanMobile}
                            releaseSurveys={releaseSurveys}
                          />
                        ) : null}
                        {zoomLevel === 3 ? (
                          <BrickWall
                            zoomLevel={zoomLevel}
                            isMoreThanLargeMobile={isMoreThanLargeMobile}
                            isLessThanMobile={isLessThanMobile}
                            releaseSurveys={releaseSurveys}
                          />
                        ) : null}
                        {zoomLevel === 2 ? (
                          <BrickWall
                            zoomLevel={zoomLevel}
                            isMoreThanLargeMobile={isMoreThanLargeMobile}
                            isLessThanMobile={isLessThanMobile}
                            releaseSurveys={releaseSurveys}
                          />
                        ) : null}
                        {zoomLevel === 1 ? (
                          <BrickWall
                            zoomLevel={zoomLevel}
                            isMoreThanLargeMobile={isMoreThanLargeMobile}
                            isLessThanMobile={isLessThanMobile}
                            releaseSurveys={releaseSurveys}
                          />
                        ) : null}
                      </>
                    ) : (
                      <>
                        {/* Mobile Logic */}
                        <MobileBrickWall
                          zoomLevel={zoomLevel}
                          isMoreThanLargeMobile={isMoreThanLargeMobile}
                          isLessThanMobile={isLessThanMobile}
                          releaseSurveys={releaseSurveys}
                        />
                      </>
                    )}
                  </div>
                ) : null}
              </div>
              {/* </Layout> */}
            </BrickWallPortal>
          )}
        </>
      )}
    </>
  );
}
