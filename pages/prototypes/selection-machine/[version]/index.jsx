import { useState } from 'react';
import { useRouter } from 'next/router';

import _ from 'lodash';

// Custom Components and Utils
import {
  SurveyIntroPrototype,
  CQOAnswerPagePrototype,
  CQOAnswerPagePrototypeTwo,
  Layout,
  PanelistsOutro,
} from 'components';

import { selectionMachineSurveys, versionControlFlow } from 'utils/data';

const prototypeOne = ['v1_large_survey', 'v1_small_survey', 'v1_medium_survey'];
const prototypeTwo = ['v2_large_survey', 'v2_small_survey', 'v2_medium_survey'];

export default function Survey() {
  const router = useRouter();
  const [active, setActive] = useState(0);

  const version = router.query.version;
  const controlFlow = versionControlFlow[version];

  const section = selectionMachineSurveys[controlFlow?.surveyIndex];

  const getRandomizedQuestions = () => {
    const questions = _.shuffle(section?.questions);
    return questions;
  };

  if (active === 0) {
    return (
      <Layout title={section?.name}>
        <div className="WorldMap-scree-main py-[40px] pt-[15px]">
          <SurveyIntroPrototype
            questions_count={section?.questions_count}
            active={active}
            setActive={setActive}
            type={section?.status === 'Closed'}
          />
        </div>
      </Layout>
    );
  }

  // Common Props for All Answer Pages
  const commonAnswerPageProps = {
    active,
  };

  return (
    <Layout title={section?.name}>
      <div className="Question_world_map_scree scrollbar-style py-[40px] pt-[15px]">
        <div className="SurveyOpen-scree-main">
          {prototypeOne.includes(version) && (
            <CQOAnswerPagePrototype
              questions={getRandomizedQuestions()}
              {...commonAnswerPageProps}
            />
          )}

          {prototypeTwo.includes(version) && (
            <CQOAnswerPagePrototypeTwo
              questions={getRandomizedQuestions()}
              questionsToReview={getRandomizedQuestions()}
              {...commonAnswerPageProps}
            />
          )}
        </div>
      </div>
    </Layout>
  );
}
