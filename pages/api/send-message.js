const sgMail = require('@sendgrid/mail');

// sgMail.setApiKey(process.env.NEXT_PUBLIC_SENDGRID_API_KEY);

export default async function handler(req, res) {
  const msg = {
    to: 'prncsammian@gmail.com', // Change to your recipient
    from: 'samtufail726@gmail.com', // Change to your verified sender
    subject: 'A Message From Contact Form On Human Dilemmas',
    html: `
      <p>Email: ${req?.body?.email}</p>
      <p>Message: ${req?.body?.message}</p>
    `,
    
  };

  try {
    await sgMail.send(msg);
    res.status(200).json({ message: 'Thanks' });
  } catch (e) {
    res.json({ message: 'Error' });
  }
}
