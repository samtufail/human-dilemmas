import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSelector, useDispatch } from 'react-redux';

import {
  setFirstQuestionID,
  getSurveybyGID,
  getOpenVideoSurveys,
  getMyAttemptedQuestions,
} from 'store/surveys';

import { VideoPlayerContainer, Loader } from 'components';

export default function Home() {
  const dispatch = useDispatch();

  const router = useRouter();
  const questionID = router?.query?.questionID;
  const user = useSelector((state) => state?.auth?.user);

  const { openVideoSurveyQuestions, isLoading, error, firstSurveyGID } =
    useSelector((state) => state?.surveys);

  useEffect(() => {
    dispatch(getSurveybyGID(firstSurveyGID));
    dispatch(setFirstQuestionID(questionID));

    if (!openVideoSurveyQuestions?.length) {
      dispatch(getOpenVideoSurveys());
    }

    if (user) {
      dispatch(getMyAttemptedQuestions());
    }
  }, [questionID, user]);

  return (
    <div>
      {isLoading && (
        <div className="w-screen h-screen flex items-center justify-center">
          <Loader />
        </div>
      )}

      {error && (
        <div className="w-screen h-screen flex items-center justify-center">
          <h1 className="text-2xl text-red-500">{error}</h1>
        </div>
      )}

      {openVideoSurveyQuestions && openVideoSurveyQuestions.length > 0 && (
        <VideoPlayerContainer />
      )}
    </div>
  );
}
