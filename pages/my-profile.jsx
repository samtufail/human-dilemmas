import { useState } from 'react';
import { Layout } from 'components';
import { Spin } from 'antd';
import { VerifyNumber, YourProfile } from 'sections';

const MyProfile = () => {
  const [loading, setLoading] = useState(false);
  const [active, setActive] = useState('your-profile');

  return (
    <Spin spinning={loading}>
      <Layout title="My Profile">
        <div>
          {/* <div className="-mt-[90px] 2xl:mt-[45px] flex items-center justify-center"> */}
          <div className="flex items-center justify-center">
            {active === 'your-profile' ? (
              <YourProfile setLoading={setLoading} setActive={setActive} />
            ) : (
              <></>
            )}
            {active === 'verification' ? (
              <VerifyNumber setActive={setActive} />
            ) : (
              <></>
            )}
          </div>
        </div>
      </Layout>
    </Spin>
  );
};

export default MyProfile;
