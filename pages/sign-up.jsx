import { Layout } from 'components';
import { useState } from 'react';
import { Password, PhoneInput, PhoneVerification } from 'sections';

// Reset state on page load
import { resetState } from 'store/surveys';
import { useDispatch } from 'react-redux';

const CreateProfile = () => {
  const [active, setActive] = useState('phone');

  const dispatch = useDispatch();
  dispatch(resetState());

  return (
    <Layout title="Create Profile">
      {active === 'phone' ? (
        <PhoneInput setActive={setActive} />
      ) : active === 'verification' ? (
        <PhoneVerification setActive={setActive} />
      ) : active === 'password' ? (
        <Password />
      ) : (
        <></>
      )}
    </Layout>
  );
};

export default CreateProfile;
