import React, { useState } from 'react';
import { Field, Form, Formik } from 'formik';
import { AiOutlineClose } from 'react-icons/ai';
import { Layout } from 'components';
import Link from 'next/link';
import { api } from 'utils';
import { Portal } from 'HOC';

const ContactForm = () => {
  const [thanks, setThanks] = useState(false);
  const [label2, setLabel2] = useState(false);
  return (
    <Layout title="Contact humans">
      {thanks ? (
        <Portal>
          <div
            className="absolute bg-popupBg top-0 z-10 h-full w-full"
            style={{ opacity: '0.8' }}
          >
            <div className="absolute bottom-[30px] flex flex-col sm:flex-row space-y-[10px] items-center justify-between w-full px-[50px]">
              <button className="text-dark bg-white border-0 rounded-full py-[5px] sm:py-3 px-[15px] text-[13px] sm:text-[15px] font-normal">
                Thanks for reaching out.
              </button>
              <Link href="/" passHref>
                <a className="text-white underline text-[13px] sm:text-[15px] hover:text-white hover:underline">
                  Back to Homepage
                </a>
              </Link>
            </div>
          </div>
        </Portal>
      ) : (
        ''
      )}
      <div className="w-full flex justify-center relative pb-[20px] lg:-mt-[60px] 2xl:mt-[0px]">
        <div className="mx-[15px] xxs:mx-[20px] max-w-[760px] pt-4 md:pt-8 lg:pt-4 pb-7 md:pb-12 lg:pb-6 relative text-start md:text-center bg-popupBg rounded">
          <Link href="/" passHref>
            <div className="flex justify-end text-3xl cursor-pointer mb-[15px] lg:mb-[0px] pr-3 xxs:pr-8 relative">
              <AiOutlineClose className="text-xl sm:text-2xl" />
            </div>
          </Link>
          <div className="max-w-[679px] mx-auto px-3 xxs:px-8 ">
            <h2 className="mt-[20px] pb-[5px] text-white md:text-[36px] text-xl  lg:leading-[48px] font-normal mb-2 md:mb-6 lg:mb-5">
              Contact us
            </h2>
            <p className="lg:pb-[10px] mb-8 md:mb-12 lg:mb-5 md:text-[21px] text-sm md:leading-8 font-[300] text-left">
              Perhaps you have a proposal, some feedback or maybe a dilemma you want posted.
            </p>
            <div>
              <Formik
                initialValues={{ email: '', message: '' }}
                onSubmit={async (values) => {
                  // await axios.post('/api/send-message', values);
                  await api.post('/v1/website-admin/contact-us', {
                    email: values?.email,
                    message: values?.message,
                  });
                  setThanks(true);
                }}
              >
                <Form>
                  <div className="relative mb-4">
                    <label
                      htmlFor="email"
                      className={` font-medium pt-[5px] text-[10px] text-textGray transition-all duration-200 ease-in absolute flex pl-[15px]`}
                    >
                      {label2 ? 'Email' : ''}
                    </label>
                    <div className="mt-1">
                      <Field
                        id="email"
                        name="email"
                        type="email"
                        placeholder="Enter your email address"
                        required
                        onFocus={() => setLabel2(true)}
                        onBlur={() => setLabel2(false)}
                        className={` ${label2 ? 'pt-[24px]' : ''
                          } placeholder:textGray text-dark bg-white  border-white focus:border-boxtext  border-2 focus:shadow-inputShadow block w-full h-[52px] appearance-none rounded-md  px-3 py-4  shadow-sm  focus:outline-none text-[15px] placeholder-[15px]`}
                      />
                    </div>
                  </div>

                  <Field
                    name="message"
                    as="textarea"
                    rows="4"
                    cols="50"
                    placeholder="Type your message or question.."
                    className="lg:mt-[5px] focus:outline-none mb-[20px] md:mb-10 lg:mb-5 px-4 placeholder:text-textGray text-dark pt-2 placeholder:text-[15px] text-[15px] font-normal w-full bg-white rounded border-white focus:border-boxtext  border-2 focus:shadow-inputShadow"
                  />

                  <div className="flex justify-end">
                    <button
                      type="submit"
                      className="lg:mb-[25px] lg:mt-[20px] cursor-pointer bg-boxtext text-white placeholder:font-normal mb-[0px] md:max-w-[202px] w-full h-[35px] sm:h-[48px] leading-7 capitalize border-0 items-center flex justify-center rounded-[8px] font-semibold text-sm sm:text-lg"
                    >
                      Send
                    </button>
                  </div>
                </Form>
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default ContactForm;
