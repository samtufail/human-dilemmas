import 'styles/globals.scss';
import { useDispatch, useSelector } from 'react-redux';
import { wrapper } from '../store';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useEffect, useState, useRef } from 'react';
import { setAuthToken, loggedInUser } from 'store';
import { store } from '../store';
import setUpInterceptor from 'utils/interceptors';
import LogRocket from 'logrocket';
import Bowser from 'bowser';
import { Layout } from 'components';
import { useRouter } from 'next/router';
import { GoogleAnalytics } from 'nextjs-google-analytics';
import { lockOrientation } from 'utils/alertOrientation';
import { OrientationAlert } from 'components/Alert';

function MyApp({ Component, pageProps }) {
  const dispatch = useDispatch();

  const [notSupported, setNotSupported] = useState(false);

  useEffect(() => {
    const access = localStorage.getItem('AuthToken');
    const refresh = localStorage.getItem('RefreshToken');
    if (access && refresh) {
      dispatch(setAuthToken({ access, refresh }));
    }
  }, [dispatch]);

  const access = useSelector((state) => state?.auth?.auth?.access);

  const user = useSelector((state) => state?.auth?.user);

  useEffect(() => {
    if (access) {
      dispatch(loggedInUser());
    }
  }, [dispatch, access]);

  useEffect(() => {
    setUpInterceptor({ store });
  }, []);

  // Router Previous Path Reminder
  const router = useRouter();
  useEffect(() => storePathValues, [router.asPath]);

  function storePathValues() {
    const storage = globalThis?.sessionStorage;
    if (!storage) return;
    // Set the previous path as the value of the current path.
    const prevPath = storage.getItem('currentPath');
    storage.setItem('prevPath', prevPath);
    // Set the current path value by looking at the browser's location object.
    storage.setItem('currentPath', globalThis.location.pathname);

    if (prevPath === globalThis.location.pathname) {
      localStorage?.removeItem('surveyId');
      localStorage?.removeItem('activeQuestion');
      localStorage?.removeItem('currentShuffle');
    }
  }

  useEffect(() => {
    if (process.env.NEXT_PUBLIC_ENVIRONMENT_MODE !== 'dev') {
      // Hotjar Script Instance
      const hotjarScript = document.createElement('script');
      hotjarScript.innerHTML = `
      (function(h,o,t,j,a,r){
          h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
          h._hjSettings={hjid:${3480669},hjsv:${6}};
          a=o.getElementsByTagName('head')[0];
          r=o.createElement('script');r.async=1;
          r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
          a.appendChild(r);
      })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    `;
      document.head.appendChild(hotjarScript);

      // Log Rocket Script Instance
      LogRocket.init(process.env.NEXT_PUBLIC_LGR);
      if (user) {
        // Log Rocket User attributes
        LogRocket.identify(user?.uuid, {
          name: user?.name,
          phone: user?.phone_number,
          country: user?.country,
        });
      }
    }
  }, [user]);

  useEffect(() => {
    lockOrientation();
  }, []);

  // Check Browser
  useEffect(() => {
    if (typeof window !== undefined) {
      const browser = Bowser.getParser(window.navigator.userAgent);
      const invalidBrowser = browser.satisfies({
        ie: '>0',
        edge: '<90',
        chrome: '<80',
        firefox: '<80',
      });
      const final = invalidBrowser === undefined ? false : invalidBrowser;
      setNotSupported(final);
    }
  }, []);

  if (notSupported) {
    return (
      <Layout>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: 'calc(80vh - 300px)',
            width: '100vw',
          }}
        >
          <div style={{ width: '80%', textAlign: 'center' }}>
            <img
              src="/monkey.png"
              alt="pic of monkey telling what's wrong"
              style={{ width: '200px', height: 'auto' }}
            />
            <h2 style={{ color: 'white', marginTop: '20px' }}>
              You are using an outdated browser.
              <br />
              It can be updated via your browser settings.
            </h2>
          </div>
        </div>
      </Layout>
    );
  }

  return (
    <>
      <div id="main-body">
        {process.env.NEXT_PUBLIC_ENVIRONMENT_MODE === 'dev' ? (
          <></>
        ) : (
          <GoogleAnalytics trackPageViews={{ ignoreHashChange: true }} />
        )}
        <Component {...pageProps} />
        <ToastContainer
          toastStyle={{ backgroundColor: 'rgb(63, 59, 78)', color: '#fff' }}
        />
      </div>
      <OrientationAlert />
    </>
  );
}

export default wrapper.withRedux(MyApp);
