import { Layout } from 'components';
import { Hero, Questions, What } from 'sections';

export default function HomePage() {
  return (
    <Layout title="">
      <Hero />
      <What />
      <Questions />
      {/* Footer */}
      <footer className="w-full mt-[100px] py-[12px] bg-[#3F3B4E] flex items-center justify-center">
        <img src="/svg/footer.svg" alt="Made by Humans" />
      </footer>
    </Layout>
  );
}
