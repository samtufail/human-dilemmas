import { api } from 'utils';

//pages/sitemap.xml.js
const EXTERNAL_DATA_URL = process.env.NEXT_PUBLIC_APP_URL;

function generateSiteMap(surveys) {
  return `<?xml version="1.0" encoding="UTF-8"?>
   <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
     <!--We manually set the two URLs we know already-->
     <url>
       <loc>${EXTERNAL_DATA_URL}</loc>
     </url>
     <url>
       <loc>${EXTERNAL_DATA_URL}/questions</loc>
     </url>
     <url>
       <loc>${EXTERNAL_DATA_URL}/contact-us</loc>
     </url>
     <url>
       <loc>${EXTERNAL_DATA_URL}/about</loc>
     </url>
     ${surveys
       .map((survey) => {
         return `
       <url>
           <loc>${`${EXTERNAL_DATA_URL}/${encodeURIComponent(survey?.name)}/${
             survey?.gid
           }`}</loc>
       </url>
     `;
       })
       .join('')}
   </urlset>
 `;
}

function SiteMap() {
  // getServerSideProps will do the heavy lifting
}

export async function getServerSideProps({ res }) {
  // We make an API call to gather the URLs for our site
  const response = await api.get('/v1/surveys');
  const surveys = await response?.data;

  // We generate the XML sitemap with the posts data
  const sitemap = generateSiteMap(surveys?.results);

  res.setHeader('Content-Type', 'text/xml');
  // we send the XML to the browser
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
}

export default SiteMap;
