// Lib Imports
import { useState } from 'react';
import { Field, Form, Formik } from 'formik';
import { AiOutlineClose } from 'react-icons/ai';
import { FaEyeSlash, FaEye } from 'react-icons/fa';
import { useDispatch, useSelector } from 'react-redux';
import Link from 'next/link';

// Custom Imports
import { useRouter } from 'next/router';
import { Layout, CustomPhoneInput, Loader } from 'components';
import { loginUser, loggedInUser } from 'store';
import { countryCodes } from 'utils';
import { phone } from 'phone';

// Reset state on page load
import { resetState } from 'store/surveys';

const Login = () => {
  const [label1, setLabel1] = useState(false);
  const [label2, setLabel2] = useState(false);
  const [pass, setPass] = useState(false);
  const [error, setError] = useState('');

  const dispatch = useDispatch();

  const { loading } = useSelector((state) => state?.auth);
  const { countryCode } = useSelector((state) => state?.global);

  const [phoneNumber, setPhoneNumber] = useState('');

  const router = useRouter();

  dispatch(resetState());

  return (
    <Layout title="Log in">
      {loading && (
        <div className="w-screen h-[450px] flex items-center justify-center">
          <Loader />
        </div>
      )}
      {!loading && (
        <div className="flex flex-col justify-start items-center px-4 mt-[18px] -md:mt-[65px] signin_form">
          <div className="bg-tabbg rounded-2xl md:mt-0 p-4 pb-[24px] md:p-8 max-w-[547px] mx-auto w-full mb-[16px] md:mb-[24px]">
            <div className="flex justify-between items-center mb-[25px] md:mb-[50px]">
              <h2 className="text-white font-normal md:font-medium text-[22px] md:text-3xl leading-[33px] md:leading-[42px]">
                Login
              </h2>
              <Link href="/" passHref>
                <div className="flex justify-end text-3xl cursor-pointer max-w-[28px] max-h-[28px] md:max-w-[40px] md:max-h-[40px]">
                  <AiOutlineClose />
                </div>
              </Link>
            </div>
            <Formik
              initialValues={{ phone: '', password: '' }}
              onSubmit={async (values) => {
                const fullNumber = `${countryCode}${phoneNumber}`.trim();
                const result = phone(fullNumber, {
                  country: countryCodes?.find(
                    (code) => code?.dial_code === countryCode
                  )?.code,
                });
                if (!result?.isValid) {
                  if (!phoneNumber) {
                    setError('Error: Invalid phone number.');
                  } else {
                    setError(
                      `Error: "${phoneNumber}" is not a valid phone number.`
                    );
                  }
                } else {
                  await dispatch(
                    loginUser({
                      phone: fullNumber,
                      router,
                      password: values?.password,
                    })
                  );
                  await dispatch(loggedInUser());
                }
              }}
            >
              <Form action="#" method="POST">
                <CustomPhoneInput
                  phoneNumber={phoneNumber}
                  setPhoneNumber={setPhoneNumber}
                  showLabel={label2}
                  setShowLabel={setLabel2}
                  phoneInputClassName={'phone-input'}
                  phoneFlagClassName={'phone-flag'}
                />
                {/* Error */}
                <div className="flex items-center justify-end sm:mt-[5px] sm:mb-[15px]">
                  <p className="text-[13px]">{error ? error : <>&nbsp;</>}</p>
                </div>

                <div className="relative">
                  <label
                    htmlFor="password"
                    className={` font-medium text-[10px] pt-[5px]  text-textGray absolute flex pl-[15px]`}
                  >
                    {label1 ? 'Password' : ''}
                  </label>
                  <div className="mt-1">
                    <Field
                      id="password"
                      name="password"
                      type={pass ? 'text' : 'password'}
                      placeholder="Enter Password"
                      autoComplete="current-password"
                      onBlur={() => setLabel1(false)}
                      onFocus={() => setLabel1(true)}
                      required
                      className={` ${
                        label1 ? 'pt-[24px]' : ''
                      } placeholder:text-textGray border-white focus:border-boxtext  border-2 focus:shadow-inputShadow  h-[52px] text-textLogin bg-white block w-full appearance-none rounded-md  px-3 py-4 placeholder-textGray shadow-sm  focus:outline-none  sm:text-sm`}
                    />
                    <button
                      className="border-0 bottom-0 m-auto h-max absolute top-0 right-1 text-dark text-lg bg-transparent inline-flex"
                      onClick={() => setPass(!pass)}
                      type="button"
                    >
                      {pass ? <FaEye /> : <FaEyeSlash />}
                    </button>
                  </div>
                </div>

                <div className="text-[13px] md:text-[15px] font-normal mt-[14px] md:mt-[24px] mb-[61px] md:mb-[24px] text-end sm:flex items-center justify-end gap-1 forgot_password_signin">
                  <p className=" text-white">Forgot your password?</p>
                  <p>
                    Click&nbsp;
                    <Link href="/reset-password/login" passHref>
                      <span className="underline cursor-pointer text-white font-semibold">
                        here.
                      </span>
                    </Link>
                  </p>
                </div>

                <div className="flex justify-end">
                  <button
                    type="submit"
                    className="cursor-pointer bg-boxtext text-white placeholder:font-normal md:mb-[42px] md:max-w-[202px] w-full h-[48px] leading-7 capitalize border-0 items-center flex justify-center rounded-[16px] font-semibold text-[16px] md:text-lg"
                  >
                    Log in
                  </button>
                </div>
              </Form>
            </Formik>
          </div>
        </div>
      )}
    </Layout>
  );
};

export default Login;
