import { Layout } from 'components';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { NewPassword, NewPhoneInput, NewPhoneVerification } from 'sections';

const ResetPassword = () => {
  const router = useRouter();
  const { from } = router.query;

  const [active, setActive] = useState('');

  useEffect(() => {
    if (from === 'login') {
      setActive('phone');
    } else if (from === 'my-profile') {
      setActive('verification');
    }
  }, [from]);

  return (
    <Layout title="Reset Password">
      {active === 'phone' ? (
        <NewPhoneInput setActive={setActive} />
      ) : active === 'verification' ? (
        <NewPhoneVerification setActive={setActive} />
      ) : active === 'password' ? (
        <NewPassword setActive={setActive} />
      ) : (
        <></>
      )}
    </Layout>
  );
};

export default ResetPassword;
