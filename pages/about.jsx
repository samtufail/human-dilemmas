import { useState } from 'react';
import { Layout } from 'components';
import { About1, About2 } from 'sections';

export default function About() {
  const [current, setCurrent] = useState(1);
  return (
    <Layout title="About humans">
      {current === 1 ? <About1 setCurrent={setCurrent} /> : <></>}
      {current === 2 ? <About2 /> : <></>}
    </Layout>
  );
}
