/** @type {import('tailwindcss').Config} */

const plugin = require('tailwindcss/plugin');
const tailwind_scrollbar = require('tailwind-scrollbar');

module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './sections/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      backgroundColor: {
        transparent: 'transparent',
        boxbg: '#845EC2',
        boxbg2: '#262D35',
        tabbg: '#3F3B4E',
        black: {
          survey: '#0E161F',
          survey2: '#262D35',
          survey3: '#0E161F',
          cta: '#212139',
          cta2: '#2B3B4E',
          cta3: '#6E7378',
          hover: '#00000017',
        },
      },
      colors: {
        transparent: 'transparent',
        boxtext: '#845EC2',
        white: '#ffffff',
        heading: '#93969A',
        popupBg: '#0E161F',
        textGray: '#6E7378',
        textGray2: '#808080',
        dark: '#010202',
        textLogin: '#20262C',
      },
      width: {
        'w-80': '80px',
        'w-56': '56px',
        'w-100': '100px',
        'w-110': '110px',
        'w-120': '120px',
        'w-130': '130px',
        'w-140': '140px',
        'w-150': '150px',
        'w-160': '160px',
        'w-170': '170px',
        'w-180': '180px',
        'w-1500': '1500px',
      },
      height: {
        'h-48': '48px',
      },
      maxWidth: {
        750: '750px',
        950: '950px',
        '3/4': '75%',
      },
      borderRadius: {
        0: '0px',
        4: '4px',
        8: '8px',
        50: '50px',
      },
      boxShadow: {
        inputShadow: ' 0px 0px 0px 5px #845EC259',
      },
      fontFamily: {
        display: ['Poppins', 'sans-serif'],
        body: ['Poppins', 'sans-serif'],
      },
      screens: {
        xxs: '300px',
        xs: '540px',
        mdbw: '1026px',
        nestXl: '1281px',
      },
    },
  },
  corePlugins: {
    backgroundOpacity: false,
    textOpacity: false,
  },
  plugins: [
    plugin(function ({ addUtilities, addComponents, e, config }) {
      // Add your custom styles here
    }),
    function ({ addUtilities }) {
      const dropdownHome = {
        '.Question_world_map_scree': {
          // height: "calc(100vh - 137px)",
          // overflow: "auto",
        },
      };
      addUtilities(dropdownHome, ['responsive', 'hover']);
    },
    tailwind_scrollbar({ nocompatible: true }),
  ],
};
