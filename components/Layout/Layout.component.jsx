import Head from 'next/head';
import { Navbar } from 'components';
import { useEffect } from 'react';
import { string, node } from 'prop-types';
import { useRouter } from 'next/router';
export const Layout = ({
  children,
  title = 'Home',
  description = 'Human Dilemmas is a platform where you can find questions related to your dilemmas and find answers.',
}) => {
  const { pathname } = useRouter();
  useEffect(() => {
    document.documentElement.lang = 'en-us';
  });
  return (
    <>
      <Head>
        <title>{title ? `${title} - ` : ''}Human Dilemmas</title>
        <meta name="description" content={description} />
        <meta name="google" content="notranslate" />
      </Head>
      <div>
        {pathname !== '/[survey-name]/[surveyId]' ? <Navbar /> : <></>}
        {children}
      </div>
    </>
  );
};

Layout.propTypes = {
  children: node,
  description: string,
};
