import dynamic from 'next/dynamic';

// Loader
export * from './Loader.component';
// Buttons
export * from './Buttons';
// Surveys
export * from './AllSurveys';
export * from './ClosedQuestions';
export * from './VideoQuestions';
export * from './WorldMapQuestions';

// AfterVerification
export const AfterVerification = dynamic(
  () =>
    import('./AfterVerification/AfterVerification.component').then(
      (module) => module.AfterVerification
    ),
  { ssr: false }
);
// Navbar
export const Navbar = dynamic(
  () => import('./Navbar/Navbar.component').then((module) => module.Navbar),
  { ssr: false }
);
// BrickNav
export const BrickNav = dynamic(
  () => import('./Navbar/BrickNav.component').then((module) => module.BrickNav),
  { ssr: false }
);
// Layout
export const Layout = dynamic(
  () => import('./Layout/Layout.component').then((module) => module.Layout),
  { ssr: false }
);

// Custom Phone Input
export const CustomPhoneInput = dynamic(
  () =>
    import('./PhoneInput/PhoneInput.component').then(
      (module) => module.CustomPhoneInput
    ),
  { ssr: false }
);
// Surveys (Common)
export const SurveyIntro = dynamic(
  () =>
    import('./SurveyIntro/SurveyIntro').then((module) => module.SurveyIntro),
  { ssr: false }
);

export const SurveyIntroPrototype = dynamic(
  () =>
    import('./SurveyIntro/SurveyIntroPrototype').then(
      (module) => module.SurveyIntroPrototype
    ),
  { ssr: false }
);

export const SurveyOutro = dynamic(
  () =>
    import('./SurveyOutro/SurveyOutro').then((module) => module.SurveyOutro),
  { ssr: false }
);

export const PanelistsOutro = dynamic(
  () =>
    import('./SurveyOutro/PanelistsOutro').then(
      (module) => module.PanelistsOutro
    ),
  { ssr: false }
);

export const QuestionFeedback = dynamic(
  () =>
    import('./QuestionFeedback/QuestionFeedback').then(
      (module) => module.QuestionFeedback
    ),
  { ssr: false }
);

export const QuestionFeedbackTwo = dynamic(
  () =>
    import('./QuestionFeedback/QuestionFeedbackTwo').then(
      (module) => module.QuestionFeedbackTwo
    ),
  { ssr: false }
);

export const PageHead = dynamic(
  () =>
    import('./PageHead/PageHead.component').then((module) => module.PageHead),
  { ssr: false }
);
