import React, { useEffect, useRef, useState, useCallback } from 'react';
import { useMediaQuery } from 'react-responsive';
import { useRouter } from 'next/router';
import { Radio } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { setQuestionAsAnswered } from 'store/surveys';
import { startGraphTime, endGraphTime } from 'store/global';
import Slider from '@mui/material/Slider';
import CircularProgress from '@mui/material/CircularProgress';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PauseIcon from '@mui/icons-material/Pause';
import ShareIcon from '@mui/icons-material/Share';
import Replay10Icon from '@mui/icons-material/Replay10';
import Forward10Icon from '@mui/icons-material/Forward10';
import ReplayIcon from '@mui/icons-material/Replay';
import { api, timeAgo } from 'utils';

import Styles from './VidoePlayer.module.scss';
import { VQOGraphPage } from '../VQGraphPage/VQOGraphPage.component';
import { Modal } from 'antd';
import { useAnswerTime } from 'hooks';
import { answerTimeStart, answerTimeEnd } from 'utils';
import { CloseOutlined } from '@ant-design/icons';
import GetShareableLink from '../VQGraphPage/GetShareableLink.component';
import ViewResultsMesssage from './ViewResultsMesssage.component';

export const VideoPlayer = ({
  currentQuestion,
  surveyId,
  handleNext,
  index,
}) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const fullPath = window.location.href;

  const video = useRef();

  const [, sendAnswerTimeEndRequest] = useAnswerTime(
    currentQuestion.qid,
    currentQuestion.gid,
    answerTimeStart,
    answerTimeEnd,
  );

  const { user } = useSelector((state) => state?.auth);

  async function processAnswerTimeEnd() {
    sendAnswerTimeEndRequest();
  }

  const { answeredQuestions, currentIndex, myAttemptedQuestions } = useSelector(
    (state) => state?.surveys,
  );

  const { graphTimeID } = useSelector((state) => state?.global);

  const getAttemptedQuestion = useCallback(
    (qidToCheck) => {
      const attemptedQuestion = myAttemptedQuestions?.find(
        (item) => item.qid === qidToCheck,
      );
      return attemptedQuestion ? attemptedQuestion : null;
    },
    [myAttemptedQuestions],
  );

  function isAlreadyAnswered(qidToCheck) {
    return answeredQuestions.some((item) => item.qid === qidToCheck);
  }

  function graphDataFromStore(qid) {
    const answeredQuestion = answeredQuestions.find((item) => item.qid === qid);
    return answeredQuestion ? answeredQuestion.graphData : null;
  }

  const [answer, setAnswer] = useState();

  const [initalState, setInitialState] = useState(true);
  const [playStatus, setPlayStatus] = useState(false);
  const [currentTime, setCurrentTime] = useState(0);
  const [remainingTime, setRemainingTime] = useState();
  const [videoDuration, setVideoDuration] = useState();
  const [isBuffering, setIsBuffering] = useState(false);
  const [hasEnded, setHasEnded] = useState(false);
  const [linkCopied, setLinkCopied] = useState(false);
  const [showGraph, setShowGraph] = useState(false);

  const [showControls, setShowControls] = useState(true);

  const [graphData, setGraphData] = useState(
    graphDataFromStore(currentQuestion.qid),
  );

  useEffect(() => {
    if (isAlreadyAnswered(currentQuestion.qid)) {
      setGraphData(graphDataFromStore(currentQuestion.qid));
    }
    if (currentIndex !== index && playStatus) {
      video.current.pause();
      setPlayStatus(false);
    }
  }, [currentQuestion, currentIndex, index]);

  const getRemainingTime = (duration, currentTime) => {
    setCurrentTime(currentTime);
    const timeInSeconds = duration - currentTime;
    const hours = Math.floor(timeInSeconds / 3600);
    const minutes = Math.floor((timeInSeconds % 3600) / 60);
    const seconds = Math.floor(timeInSeconds % 60);
    return `${hours > 0 ? hours + ':' : ''}${
      minutes < 10 ? '0' : ''
    }${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
  };

  const handleRewind = () => {
    const newTime = video.current.currentTime - 10;
    video.current.currentTime = newTime > 0 ? newTime : 0;
  };

  const handleForward = () => {
    const newTime = video.current.currentTime + 10;
    const duration = video.current.duration;
    video.current.currentTime = newTime < duration ? newTime : duration;
  };

  const handlePlay = () => {
    // Set initial state to false
    if (initalState) {
      setInitialState(false);
    }

    if (playStatus) {
      video.current.pause();
    } else {
      video.current.play();
    }
    setPlayStatus(!playStatus);
  };

  useEffect(() => {
    if (!isBuffering) {
      hideControlsAfterDelay(); // Call the function to hide controls
    }
  }, [isBuffering]);

  useEffect(() => {
    let vidDuration = 0;
    let current = 0;

    video.current.oncanplay = () => {
      setIsBuffering(false);
      vidDuration = video?.current?.duration;
      setVideoDuration(vidDuration);
      current = video?.current?.currentTime;
      setRemainingTime(getRemainingTime(vidDuration, current));
    };

    video.current.ontimeupdate = () => {
      const newTime = video?.current?.currentTime;
      if (newTime - current > 1) {
        current = newTime;
        setRemainingTime(getRemainingTime(vidDuration, newTime));
      }
    };

    video.current.onended = () => {
      video?.current?.pause();
      setPlayStatus(false);
      setHasEnded(true);
    };

    video.current.onwaiting = () => {
      setIsBuffering(true);
      setShowControls(true);
    };

    video.current.onplaying = () => {
      setIsBuffering(false);
      setShowControls(false);
    };

    return () => {
      if (!video) return; // If video is null, return early (this is a safeguard)
      if (!video.current) return; // If video.current is null, return early (this is a safeguard)
      // Cleaning up the listeners
      video.current.oncanplay = () => {};
      video.current.ontimeupdate = () => {};
      video.current.onwaiting = () => {};
      video.current.onplaying = () => {};
      video.current.onended = () => {};
    };
  }, [video, currentQuestion]);

  const BufferingIndicator = () => (
    <div className="bg-[#845EC2] h-16 w-16 sm:h-20 sm:w-20 rounded-full flex items-center justify-center">
      <CircularProgress
        sx={{
          color: '#ffffff',
        }}
      />
    </div>
  );

  const ReplayButton = () => (
    <button
      onClick={() => {
        video.current.currentTime = 0;
        handlePlay();
        setHasEnded(false);
      }}
      className="bg-[#845EC2] h-16 w-16 sm:h-20 sm:w-20 rounded-full"
    >
      <ReplayIcon
        className={Styles['video-control-icons']}
        sx={{
          fontSize: '55px',
        }}
      />
    </button>
  );

  const RewindButton = () => (
    <button onClick={handleRewind}>
      <Replay10Icon
        className={Styles['video-control-icons']}
        sx={{ fontSize: '55px' }}
      />
    </button>
  );

  const PlayPauseButton = () => (
    <button
      onClick={handlePlay}
      className="bg-[#845EC2] h-16 w-16 sm:h-20 sm:w-20 rounded-full"
    >
      {playStatus ? (
        <PauseIcon
          className={Styles['video-control-icons']}
          sx={{
            fontSize: '55px',
          }}
        />
      ) : (
        <PlayArrowIcon
          className={Styles['video-control-icons']}
          sx={{ fontSize: '55px' }}
        />
      )}
    </button>
  );

  const ForwardButton = () => (
    <button onClick={handleForward}>
      <Forward10Icon
        className={Styles['video-control-icons']}
        sx={{ fontSize: '55px' }}
      />
    </button>
  );

  const hideControlsAfterDelay = () => {
    if (!initalState && !hasEnded) {
      setTimeout(() => {
        setShowControls(false);
      }, 300);
    }
  };

  const saveAnswer = async () => {
    const data = await api.put(`/v1/surveys/${surveyId}/update/`, {
      qid: currentQuestion.qid,
      answer: answer,
    });

    processAnswerTimeEnd();

    setGraphData({
      data: data?.data,
      answer: answer,
    });

    dispatch(
      setQuestionAsAnswered({
        qid: currentQuestion.qid,
        graphData: {
          data: data?.data,
          answer: answer,
        },
      }),
    );

    setShowGraph(true);
  };

  const getResults = async () => {
    const requestData = {
      qid: currentQuestion.qid,
      is_verified: user ? true : false,
    };

    const data = await api.post(
      `/v1/surveys/${currentQuestion.gid}/results/`,
      requestData,
    );

    const gData = {
      data: data?.data,
      answer: getAttemptedQuestion(currentQuestion.qid)
        ? getAttemptedQuestion(currentQuestion.qid).answer
        : '',
    };

    setGraphData(gData);
    setShowGraph(true);

    dispatch(
      setQuestionAsAnswered({
        qid: currentQuestion.qid,
        graphData: gData,
      }),
    );
  };

  useEffect(() => {
    if (answer && !getAttemptedQuestion(currentQuestion.qid)) {
      saveAnswer();
    }
  }, [answer]);

  const handleCancel = () => {
    setShowGraph(false);
    // Set answer to null
    setAnswer(null);
  };

  const handleOk = () => {
    setShowGraph(false);
  };

  useEffect(() => {
    if (isAlreadyAnswered(currentQuestion.qid)) {
      // Show Graph
      if (!playStatus) {
        if (graphData) {
          setShowGraph(true);
        }
      }
    }

    if (!playStatus) {
      if (graphData) {
        setShowGraph(true);
      }
    }

    if (user && getAttemptedQuestion(currentQuestion.qid)) {
      setAnswer(getAttemptedQuestion(currentQuestion.qid).answer);
    }
  }, [playStatus]);

  useEffect(() => {
    if (showGraph)
      dispatch(
        startGraphTime({
          qid: currentQuestion.qid,
          gid: currentQuestion.gid,
          type: currentQuestion.status === 'Closed' ? 0 : 1,
        }),
      );
  }, [showGraph]);

  const handleNextSurvey = () => {
    handleNext();
    dispatch(
      endGraphTime(graphTimeID, {
        qid: currentQuestion.qid,
      }),
    );
  };

  function getDomainFromUrl(fullPath) {
    try {
      const url = new URL(fullPath);
      return url.origin;
    } catch (error) {
      console.error('Invalid URL:', error);
      return null; // You can return an appropriate value or handle the error as needed
    }
  }

  const domain = getDomainFromUrl(fullPath);

  const isModalClosable = () => {
    if (currentQuestion.status === 'Closed') {
      return true;
    } else {
      if (user && getAttemptedQuestion(currentQuestion.qid)) {
        return true;
      }
      return false;
    }
  };

  const isRadiosAreDisabled = (qid) => {
    if (isAlreadyAnswered(qid)) {
      // Non authenticated for that particular session
      return true;
    }

    if (user && getAttemptedQuestion(qid)) {
      // Authenticated user who already have answered the question
      return true;
    }

    return false;
  };

  // Check if it's mobile or desktop
  const isMobile = useMediaQuery({ query: '(max-width: 428px)' });

  const widthRelativeToChoicesLength =
    currentQuestion.choices.length > 3 ? '680px' : '680px';

  const heightRelativeToChoicesLength =
    currentQuestion.choices.length > 3 ? '680px' : '700px';

  const modalWidth = isMobile ? '384px' : widthRelativeToChoicesLength;
  const modalHeight = isMobile ? '660px' : heightRelativeToChoicesLength;
  const modalTranslateX = isMobile ? '-1.5%' : '-10%';

  return (
    <div>
      <article
        className={`bg-${
          currentQuestion?.status === 'Closed' ? '[#262D35]' : '[#0E161F]'
        } max-h-[85vh] sm:max-h-[95vh] w-fit max-w-[100%] sm:max-w-[600px] rounded-md  text-white mb-8 mx-auto ${
          isMobile
            ? Styles['snap-scroll-item-mobile']
            : Styles['snap-scroll-item-desktop']
        }`}
      >
        <div
          style={{
            minWidth: 'min(95vw, 350px)',
          }}
          onClick={() => {
            setShowControls(true);
          }}
          className={`relative h-[70vh] md:h-[75vh] max-h-[75vh] aspect-[720/1080] group max-w-full`}
        >
          <video
            poster={currentQuestion?.thumbnail}
            ref={video}
            className={`h-full w-full object-cover  ${Styles['rounded-poster']}`}
            controls={false} // Disable default controls
            playsInline // Encourage browser to play inline

            // webkit-playsinline="true" // Specific for iOS Safari
          >
            <source src={currentQuestion?.video_file} />
          </video>
          <div
            className={`${playStatus ? 'hidden' : 'flex'} ${
              isBuffering && 'flex'
            } group-hover:flex justify-center items-center absolute inset-0 z-10 `}
          >
            <>
              {currentQuestion.status !== 'Closed' ? (
                <>
                  {' '}
                  {/* Hide the radio buttons and progress bar on initial state */}
                  {!playStatus && !isBuffering && !initalState && (
                    <div
                      style={{ backgroundColor: 'rgba(63,59,78,0.75)' }}
                      className={` absolute top-0 z-20 w-full p-4  object-cover  ${Styles['rounded-poster']}`}
                    >
                      <Radio.Group
                        name="radiogroup1"
                        className={`flex flex-col align-start gap-[16px] sm:gap-[24px]`}
                        value={answer}
                        onChange={(e) => {
                          setAnswer(e?.target?.value);
                        }}
                      >
                        {currentQuestion.choices.map((val, ind) => (
                          <div
                            key={val + ind}
                            className={`video-player-radio ${Styles['radio-wrapper']}`}
                            style={{
                              marginTop:
                                currentQuestion.choices.length > 4
                                  ? `${isMobile ? '-6px' : '-11px'}`
                                  : '0px',
                            }}
                          >
                            <Radio
                              value={val}
                              disabled={isRadiosAreDisabled(
                                currentQuestion.qid,
                              )}
                            >
                              <span className={Styles['radio-text']}>
                                {val}
                              </span>
                            </Radio>
                          </div>
                        ))}
                      </Radio.Group>
                    </div>
                  )}
                </>
              ) : (
                <>
                  {!playStatus && !isBuffering && !initalState && (
                    <div
                      style={{
                        position: 'absolute',
                        bottom: isMobile ? 45 : 140,
                        left: 55,
                        width: 'calc(100% - 70px)',
                        top: isMobile ? '300px' : '400px', // Adjust this value to move it further down
                      }}
                      className={` absolute top-0 z-20 w-full p-4  object-cover  `}
                    >
                      <ViewResultsMesssage
                        onShowResults={getResults}
                        answer={null}
                        closed={true}
                      />
                    </div>
                  )}
                </>
              )}
            </>

            {isBuffering ? (
              <BufferingIndicator />
            ) : hasEnded ? (
              <ReplayButton />
            ) : (
              <>
                {showControls && (
                  <div className="space-x-5">
                    {!initalState && <RewindButton />}
                    <PlayPauseButton />
                    {!initalState && <ForwardButton />}
                  </div>
                )}
              </>
            )}

            {user && (
              <>
                {currentQuestion.status !== 'Closed' &&
                getAttemptedQuestion(currentQuestion.qid) ? (
                  <>
                    {' '}
                    {!playStatus && !isBuffering && !initalState && (
                      <div
                        // For Authenticated user's who already have answered the question
                        style={{
                          position: 'absolute',
                          bottom: isMobile ? 45 : 140,
                          left: 45,
                          width: 'calc(100% - 50px)',
                          top: isMobile ? '300px' : '400px', // Adjust this value to move it further down
                        }}
                        className={` absolute z-20 w-full p-4 object-cover space-x-5 `}
                      >
                        <ViewResultsMesssage
                          onShowResults={getResults}
                          answer={getAttemptedQuestion(currentQuestion.qid)}
                          closed={false}
                        />
                      </div>
                    )}
                  </>
                ) : (
                  <>
                    {!playStatus && !isBuffering && !initalState && (
                      <div
                        style={{
                          position: 'absolute',
                          bottom: isMobile ? 45 : 140,
                          left: 55,
                          width: 'calc(100% - 70px)',
                          top: isMobile ? '300px' : '400px', // Adjust this value to move it further down
                        }}
                        className={` absolute top-0 z-20 w-full p-4  object-cover  `}
                      >
                        <ViewResultsMesssage
                          onShowResults={getResults}
                          answer={null}
                          closed={true}
                        />
                      </div>
                    )}
                  </>
                )}
              </>
            )}

            {!initalState && (
              <Slider
                aria-label="time-indicator"
                size="small"
                value={currentTime}
                min={0}
                step={1}
                max={videoDuration}
                onChange={(_, value) => {
                  setCurrentTime(value);
                  video.current.currentTime = value;
                }}
                sx={{
                  position: 'absolute',
                  bottom: 0,
                  padding: 0,
                  color: '#574081',
                  '&.MuiSlider-root': {
                    padding: 0,
                  },
                  '& .MuiSlider-thumb': {
                    width: 8,
                    height: 8,
                    transition: '0.3s cubic-bezier(.47,1.64,.41,.8)',
                    '&:before': {
                      boxShadow: '0 2px 12px 0 rgba(0,0,0,0.4)',
                    },
                    '&:hover, &.Mui-focusVisible': {
                      boxShadow: '0px 0px 0px 8px rgb(255 255 255 / 16%)',
                    },
                    '&.Mui-active': {
                      width: 14,
                      height: 14,
                    },
                  },
                  '& .MuiSlider-rail': {
                    backgroundColor: '#ffffff',
                    opacity: 0.28,
                  },
                }}
              />
            )}
          </div>
        </div>
        <div className={isMobile ? 'p-3' : 'p-4'}>
          <div className="max-w-[325px] flex items-center justify-between">
            <h1 style={{ lineHeight: '23px' }} className="heading-M text-white">
              {currentQuestion?.name}
            </h1>
          </div>
          <div className="flex items-center justify-between mt-1">
            <p className="text-sm sm:text-base font-light">
              {timeAgo(currentQuestion?.submitted)}
            </p>
            <div className="flex items-center gap-5">
              <p className="text-[#9AA0A6] text-base sm:text-xl">
                {remainingTime}
              </p>
              <button
                onClick={() => {
                  // navigator not defined error on safari - yet to figure out efficient solution
                  try {
                    navigator.clipboard.writeText(
                      domain + '/dilemmas/' + currentQuestion?.qid,
                    );
                    setLinkCopied(true);
                    setTimeout(() => {
                      setLinkCopied(false);
                    }, 3000);
                  } catch (err) {
                    console.log(err);
                  }
                }}
              >
                <ShareIcon />
              </button>
            </div>
          </div>

          <div className="flex justify-between items-center flex-wrap pb-0">
            <div
              className="bg-boxbg cursor-pointer "
              style={{
                borderRadius: '8px',
                height: '32px',
                width: '68px',
                marginTop: '8px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                right: '20px',
              }}
              onClick={() => router.push('/questions')}
            >
              Home
            </div>

            {linkCopied && (
              <p className="bg-white rounded-xl text-black px-3">
                Link copied to clipboard
              </p>
            )}
          </div>
        </div>
      </article>

      <Modal
        open={showGraph}
        onOk={handleOk}
        onCancel={handleCancel}
        centered={isMobile}
        closable={isModalClosable}
        closeIcon={<CloseOutlined style={{ color: '#fff', margin: '3px' }} />}
        maskClosable={false}
        className={
          !isMobile
            ? `top-[8%] ${Styles['disable-scrollbar-safari-chrome-mobile']}`
            : ''
        }
        maskStyle={{
          backgroundColor: 'rgba(0, 0, 0, 0.7)',
        }}
        bodyStyle={{
          marginRight: !isMobile ? '' : '-24px',
          // paddingTop: 'px',
          background:
            currentQuestion.status === 'Closed' ? '#262D35' : '#0E161F',
          borderRadius: '4px',
          border: `1px solid ${
            currentQuestion.status === 'Closed' ? '#262D35' : '#0E161F'
          }`,
          width: modalWidth,
          left: '30%',
          height: modalHeight,
          translate: modalTranslateX,
        }}
      >
        <div
          style={{
            position: 'absolute',
            bottom: isMobile ? '10px' : '20px',
            left: isMobile ? '10px' : '10px',
            cursor: 'pointer',
          }}
        >
          <GetShareableLink
            qid={currentQuestion.qid}
            sruveyID={currentQuestion.gid}
            user={user}
            isMobile={isMobile}
          />
        </div>

        <VQOGraphPage
          graphData={graphData}
          currentQuestion={currentQuestion}
          modalWidth={modalWidth}
          surveyId={surveyId}
          isMobile={isMobile}
          setShowGraph={setShowGraph}
          handleNext={handleNextSurvey}
          choicesCount={currentQuestion.choices.length}
          closed={currentQuestion.status === 'Closed'}
        />
      </Modal>
    </div>
  );
};
