import React from 'react';

const ViewResultsMesssage = ({ onShowResults, answer, closed }) => {
  const handleSeeResults = () => {
    onShowResults();
  };

  return (
    <div
      className="flex items-center justify-between mt-[3px]"
      style={{
        backgroundColor: closed ? '#262D35' : '#0E161F',
        padding: '10px',
      }}
    >
      <p className="text-[#fff] text-[15px] heading-M">
        {closed ? (
          <>
            <div>
              <span
                className="text-[#fff] "
                style={{
                  zIndex: 0,
                  fontSize: '15px',
                }}
              >
                {' '}
                Survey is closed
              </span>
              <span
                className="text-[#845EC2]  cursor-pointer"
                onClick={handleSeeResults}
                style={{
                  zIndex: 0,
                  top: '3%',
                  right: '4%',
                  fontSize: '15px',
                }}
              >
                {' '}
                View results
              </span>
            </div>
          </>
        ) : (
          <>
            <div>
              <span
                className="text-[#fff] "
                style={{
                  zIndex: 0,

                  fontSize: '15px',
                }}
              >
                {' '}
                You&apos;ve already answered the question on{' '}
                <b>{answer?.date}</b>
              </span>
              <span
                className="text-[#845EC2]  cursor-pointer"
                onClick={handleSeeResults}
                style={{
                  zIndex: 0,
                  top: '3%',
                  right: '4%',
                  fontSize: '15px',
                }}
              >
                {' '}
                View results
              </span>
            </div>{' '}
          </>
        )}
      </p>
    </div>
  );
};

export default ViewResultsMesssage;
