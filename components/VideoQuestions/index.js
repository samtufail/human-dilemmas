import dynamic from 'next/dynamic';

export const VideoPlayerContainer = dynamic(
  () =>
    import('./VideoPlayerContainer/VideoPlayerContainer.component').then(
      (module) => module.VideoPlayerContainer
    ),
  { ssr: false }
);

export const VideoPlayer = dynamic(
  () =>
    import('./VideoPlayer/VideoPlayer.component').then(
      (module) => module.VideoPlayer
    ),
  { ssr: false }
);
