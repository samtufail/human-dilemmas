import React, { useState, useEffect } from 'react';
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Text,
} from 'recharts';

const CustomizedAxisTick = (props) => {
  const { x, y, payload } = props;

  const textHeight = 14;
  const lineHeight = textHeight * 1;

  const totalPayloadChar = payload.value.length;
  // Adjust the offset to move the tick labels lower
  const yOffset = totalPayloadChar > 10 ? 10 : 3;

  // Calculate the new y-coordinate with the offset
  const newY = y - textHeight / 2 + lineHeight / 2 + yOffset;

  return (
    <Text
      x={x}
      y={newY}
      width={85}
      lineHeight={`${lineHeight}px`}
      fill="#fff"
      textAnchor="end"
      horizontalAnchor="end"
      fontSize={`${textHeight}px`}
    >
      {payload.value}
    </Text>
  );
};

export const BarChartDesktopVertical = ({
  shareableLink,
  results,
  auth,
  verified,
  closed,
  choicesCount,
}) => {
  const [barData, setBarData] = useState(null);

  const multiplier = choicesCount > 4 ? 100 : 170;
  const chartWidth = multiplier * choicesCount;

  useEffect(() => {
    if (!results) return;
    const keysArr = Object.keys(
      auth || verified ? results?.results : results?.data?.results
    );

    const valuesArr = Object.values(
      auth || verified ? results?.results : results?.data?.results
    );
    const updatedData = keysArr.map((val, ind) => ({
      name: val,
      pv: valuesArr[ind],
    }));
    setBarData(updatedData);
  }, [results, auth, verified]);
  if (!barData) return null;

  return (
    <BarChart
      width={chartWidth}
      height={380}
      data={barData}
      margin={{
        top: -10,
        right: 35,
        left: 50,
        bottom: -5,
      }}
      layout="vertical" // Make the chart vertical
    >
      <CartesianGrid overlineThickness="5" horizontal={false} vertical={true} />
      <YAxis dataKey="name" type="category" tick={<CustomizedAxisTick />} />
      <XAxis
        type="number"
        tick={{ fill: 'white' }}
        axisLine={false}
        tickCount={6}
        tickFormatter={(tick) => {
          return `${tick}%`;
        }}
      />

      <Bar dataKey="pv" barSize={40} fill="#C4C4C4" radius={[0, 4, 4, 0]}>
        {barData.map((entry, index) => (
          <Cell
            key={`cell-${index}`}
            fill={
              entry?.name === results?.answer && !shareableLink && !closed
                ? '#804771'
                : '#C4C4C4'
            }
          />
        ))}
      </Bar>
    </BarChart>
  );
};
