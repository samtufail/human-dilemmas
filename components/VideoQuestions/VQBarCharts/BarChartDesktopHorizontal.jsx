import React, { useState, useEffect } from 'react';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Cell,
  Text,
} from 'recharts';

const CustomizedAxisTick = (props) => {
  const { x, y, payload } = props;

  return (
    <Text
      x={x}
      y={y}
      width={120}
      lineHeight="20px"
      fill="#fff"
      textAnchor="middle"
      verticalAnchor="start"
    >
      {payload.value}
    </Text>
  );
};

export const BarChartDesktopHorizontal = ({
  shareableLink,
  results,
  auth,
  verified,
  closed, // True if the survey is closed
  choicesCount,
}) => {
  const [barData, setBarData] = useState(null);

  const lessThanThree = 200;
  const uptoFour = 170;
  const moreThanFour = 130;

  const multiplier =
    choicesCount < 4
      ? lessThanThree
      : choicesCount === 4
      ? uptoFour
      : moreThanFour;
  const chartWidth = multiplier * choicesCount;

  useEffect(() => {
    if (!results) return;
    const keysArr = Object.keys(
      auth || verified ? results?.results : results?.data?.results,
    );

    const valuesArr = Object.values(
      auth || verified ? results?.results : results?.data?.results,
    );
    const updatedData = keysArr.map((val, ind) => ({
      name: val,
      pv: valuesArr[ind],
    }));
    setBarData(updatedData);
  }, [results, auth, verified]);
  if (!barData) return null;

  return (
    <>
      <BarChart
        width={chartWidth}
        height={400}
        data={barData}
        margin={{
          top: 10,
          right: 50,
          left: 20,
          bottom: 0,
        }}
      >
        <CartesianGrid
          overlineThickness="5"
          horizontal={true}
          vertical={false}
        />
        <XAxis
          dataKey="name"
          tick={<CustomizedAxisTick />}
          interval={0}
          height={40}
        />
        <YAxis
          type="number"
          tick={{ fill: 'white' }}
          axisLine={false}
          tickCount={6} // Set the desired number of ticks here
          tickFormatter={(tick) => {
            return `${tick}%`;
          }}
        />

        <Bar dataKey="pv" barSize={50} fill="#C4C4C4" radius={[4, 4, 0, 0]}>
          {barData.map((entry, index) => (
            <Cell
              key={`cell-${index}`}
              fill={
                entry?.name === results?.answer && !shareableLink && !closed
                  ? '#804771'
                  : '#C4C4C4'
              }
            />
          ))}
        </Bar>
      </BarChart>
    </>
  );
};
