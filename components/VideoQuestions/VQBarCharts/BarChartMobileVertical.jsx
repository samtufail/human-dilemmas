import React, { useState, useEffect } from 'react';
import { useMediaQuery } from 'react-responsive';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Cell,
  Text,
} from 'recharts';

const CustomizedAxisTickGalaxyfold = (props) => {
  const isMobile = useMediaQuery({ query: '(max-width: 424px)' });

  const { x, y, payload } = props;

  return (
    <Text
      x={x}
      y={y}
      width={75}
      lineHeight={isMobile ? '15px' : '20px'}
      fill="#fff"
      textAnchor="middle"
      verticalAnchor="start"
      fontSize={10}
    >
      {payload.value}
    </Text>
  );
};

const CustomizedAxisTick = (props) => {
  const isGalaxyfold = useMediaQuery({ query: '(max-width: 280px)' });

  const { x, y, payload } = props;

  const textHeight = isGalaxyfold ? 10 : 14;
  const lineHeight = textHeight * 1;

  const totalPayloadChar = payload.value.length;
  // Adjust the offset to move the tick labels lower
  const yOffset = totalPayloadChar > 10 ? 10 : 3;

  // Calculate the new y-coordinate with the offset
  const newY = y - textHeight / 2 + lineHeight / 2 + yOffset;

  return (
    <Text
      x={x}
      y={newY}
      width={85}
      lineHeight={`${lineHeight}px`}
      fill="#fff"
      textAnchor="end"
      horizontalAnchor="end"
      fontSize={`${textHeight}px`}
    >
      {payload.value}
    </Text>
  );
};

export const BarChartMobileVertical = ({
  shareableLink,
  results,
  auth,
  verified,
  closed,
  choicesCount,
}) => {
  const [barData, setBarData] = useState(null);

  const multiplier = choicesCount > 4 ? 60 : 95;
  const chartWidth = multiplier * choicesCount;

  useEffect(() => {
    if (!results) return;
    const keysArr = Object.keys(
      auth || verified ? results?.results : results?.data?.results,
    );

    const valuesArr = Object.values(
      auth || verified ? results?.results : results?.data?.results,
    );
    const updatedData = keysArr.map((val, ind) => ({
      name: val,
      pv: valuesArr[ind],
    }));
    setBarData(updatedData);
  }, [results, auth, verified]);

  // Media Queries
  const isGalaxyfold = useMediaQuery({ query: '(max-width: 280px)' });
  const isGalaxy8plus = useMediaQuery({ query: '(max-width: 360px)' });
  if (!barData) return null;

  return (
    <ChartInstance
      barData={barData}
      shareableLink={shareableLink}
      width={chartWidth}
      results={results}
      isGalaxyfold={isGalaxyfold}
      closed={closed}
    />
  );
};

const ChartInstance = ({
  barData,
  shareableLink,
  width,
  results,
  isGalaxyfold,
  closed,
}) => {
  return (
    <BarChart
      width={width}
      height={410}
      data={barData}
      barGap={1}
      barCategoryGap="4%"
      margin={{
        top: 25,
        right: 30,
        left: 30,
        bottom: 0,
      }}
      layout="vertical" // Make the chart vertical
    >
      <CartesianGrid overlineThickness="5" horizontal={false} vertical={true} />
      <YAxis
        dataKey="name"
        type="category"
        tick={
          isGalaxyfold ? (
            <CustomizedAxisTickGalaxyfold />
          ) : (
            <CustomizedAxisTick />
          )
        }
        axisLine={false}
        interval={0}
        height={60}
      />
      <XAxis
        type="number"
        tick={{ fill: 'white' }}
        axisLine={false}
        tickCount={6}
        tickFormatter={(tick) => {
          return `${tick}%`;
        }}
      />

      <Bar dataKey="pv" barSize={40} fill="#C4C4C4" radius={[0, 4, 4, 0]}>
        {barData?.map((entry, index) => (
          <Cell
            key={`cell-${index}`}
            fill={
              entry?.name === results?.answer && !shareableLink && !closed
                ? '#804771'
                : '#C4C4C4'
            }
          />
        ))}
      </Bar>
    </BarChart>
  );
};
