import { useMediaQuery } from 'react-responsive';

const ScrollButton = ({ direction, onClick, isHidden }) => {
  const isDesktop = useMediaQuery({ query: '(min-width: 768px)' });
  const isUp = direction === 'up';

  return (
    isDesktop &&
    !isHidden && (
      <div
        className="bg-boxbg cursor-pointer"
        style={{
          borderRadius: '32%',
          height: '52px',
          width: '52px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          position: 'absolute',
          [isUp ? 'top' : 'bottom']: '20px',
          right: '20px',
        }}
        onClick={onClick}
      >
        <img
          className="object-contain"
          width="32px"
          height="32px"
          src={isUp ? '/svg/upside-arrows.svg' : '/svg/jump-icon.svg'}
          alt="logo"
        />
      </div>
    )
  );
};

export default ScrollButton;
