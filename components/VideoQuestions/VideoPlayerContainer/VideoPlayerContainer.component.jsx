import React, { useEffect, useRef, useState } from 'react';
import { Modal } from 'antd';

import { VideoPlayer } from '../VideoPlayer/VideoPlayer.component';
import { VQSharedGraph } from '../VQGraphPage/VQSharedGraph.component';

import { useMediaQuery } from 'react-responsive';
import { api } from 'utils';

import { useRouter } from 'next/router';
import { useSelector, useDispatch } from 'react-redux';
import { setCurrentIndex } from 'store/surveys';

import ScrollButton from './ScrollButton.component';

import Head from 'next/head';
import Styles from './ContainerStyles.module.scss';

export const VideoPlayerContainer = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const containerRef = useRef(null);
  const initialScrollTop = useRef(0);
  const touchStartY = useRef(null);

  const showGraphResults = Object.keys(router.query).length > 0;
  const { questionID, gid, verified } = router.query;

  const [showGraph, setShowGraph] = useState(false);
  const [graphData, setGraphData] = useState();

  const isDesktop = useMediaQuery({ query: '(min-width: 768px)' });
  const isMobile = useMediaQuery({ query: '(max-width: 428px)' });

  const [current, setCurrent] = useState(0);

  const { openVideoSurveyQuestions, firstQuestion } = useSelector(
    (state) => state?.surveys,
  );

  const openVideoSurveyQuestionsWithoutFirstQuestion =
    openVideoSurveyQuestions.filter(
      (question) => question.qid !== firstQuestion,
    );

  const [videoQuestionsBank, setVideoQuestionBank] = useState([
    ...openVideoSurveyQuestionsWithoutFirstQuestion,
  ]);

  const handleScroll = () => {
    // Update initialScrollTop when the user scrolls
    if (containerRef.current) {
      initialScrollTop.current = containerRef.current.scrollTop;
    }
  };

  function setCurrentIndexInStore(index) {
    dispatch(setCurrentIndex(index));
  }

  const handleMoveDown = () => {
    const scrolIntensityMultiplier = isMobile ? 0.6131 : 0.9131;
    const scrollIntensitiy = window.innerHeight * scrolIntensityMultiplier;
    const scrollOptions = {
      top: initialScrollTop.current + scrollIntensitiy,
      behavior: 'smooth',
    };
    containerRef.current.scrollTo(scrollOptions);

    const nextIndex = (current + 1) % videoQuestionsBank.length;
    setCurrent(nextIndex);
    setCurrentIndexInStore(nextIndex); // Dispatch the index directly

    const currentObject = videoQuestionsBank[nextIndex];
    const url = `/dilemmas/${currentObject.qid}/`;
    window.history.pushState({}, '', url);
    setShowGraph(false);
    setGraphData(null);
  };

  const handleMoveUp = () => {
    const scrolIntensityMultiplier = isMobile ? 0.6131 : 0.9131;
    const scrollIntensitiy = -window.innerHeight * scrolIntensityMultiplier;
    const scrollOptions = {
      top: initialScrollTop.current + scrollIntensitiy,
      behavior: 'smooth',
    };
    containerRef.current.scrollTo(scrollOptions);

    const prevIndex =
      (current - 1 + videoQuestionsBank.length) % videoQuestionsBank.length;
    setCurrent(prevIndex);
    setCurrentIndexInStore(prevIndex); // Dispatch the index directly

    const currentObject = videoQuestionsBank[prevIndex];
    const url = `/dilemmas/${currentObject.qid}/`;
    window.history.pushState({}, '', url);
  };

  const handleTouchStart = (e) => {
    touchStartY.current = e.touches[0].clientY;
  };

  const handleTouchEnd = (e) => {
    const touchEndY = e.changedTouches[0].clientY;
    const touchDistance = touchEndY - touchStartY.current;

    if (touchDistance > 50) {
      // Swiped down
      handleMoveUp();
    } else if (touchDistance < -50) {
      // Swiped up
      handleMoveDown();
    }
  };

  const getResults = async () => {
    const requestData = {
      qid: questionID,
      is_verified: verified === 'true' ? true : false,
    };

    const data = await api.post(`/v1/surveys/${gid}/results/`, requestData);

    setGraphData({
      data: data?.data,
      answer: '',
    });
  };

  useEffect(() => {
    if (showGraphResults) {
      getResults();
    }
  }, [showGraphResults]);

  useEffect(() => {
    if (graphData) {
      setShowGraph(true);
    }
  }, [graphData]);

  const currentQuestion = videoQuestionsBank[current];

  const widthRelativeToChoicesLength =
    currentQuestion.choices.length > 3 ? '680px' : '680px';

  const heightRelativeToChoicesLength =
    currentQuestion.choices.length > 3 ? '680px' : '700px';

  const modalWidth = isMobile ? '384px' : widthRelativeToChoicesLength;
  const modalHeight = isMobile ? '660px' : heightRelativeToChoicesLength;
  const modalTranslateX = isMobile ? '-1.5%' : '-10%';

  return (
    <div>
      <Head>
        <title>
          {videoQuestionsBank[current].name} |{' '}
          {videoQuestionsBank[current].question} - Human Dilemmas
        </title>
        <meta
          name="description"
          content={`${videoQuestionsBank[current].question} Human Dilemmas is a platform where you can find questions related to your dilemmas and find answers.`}
        />
        <meta name="google" content="notranslate" />
      </Head>

      <div
        className={`${
          isDesktop
            ? Styles['player-container-desktop']
            : Styles['player-container-mobile']
        } pt-[15px] ${Styles['disable-scrollbar-safari-chrome-mobile']} `}
        ref={containerRef}
        onScroll={handleScroll}
        onTouchStart={handleTouchStart}
        onTouchEnd={handleTouchEnd}
      >
        <div>
          {videoQuestionsBank.map((object, index) => (
            <div className={`${Styles['video-survey-list']}`} key={index}>
              <VideoPlayer
                currentQuestion={object}
                type={object?.type}
                surveyId={object?.gid}
                handleNext={handleMoveDown}
                index={index}
              />
            </div>
          ))}
        </div>
      </div>

      <Modal
        open={showGraph}
        onOk={() => setShowGraph(false)}
        onCancel={() => setShowGraph(false)}
        centered={isMobile}
        closable={false}
        maskClosable={false}
        className={
          !isMobile
            ? `top-[8%] ${Styles['disable-scrollbar-safari-chrome-mobile']}`
            : ''
        }
        maskStyle={{
          backgroundColor: 'rgba(0, 0, 0, 0.7)',
        }}
        bodyStyle={{
          marginRight: !isMobile ? '' : '-24px',
          // paddingTop: 'px',
          background:
            currentQuestion.status === 'Closed' ? '#262D35' : '#0E161F',
          borderRadius: '4px',
          border: `1px solid ${
            currentQuestion.status === 'Closed' ? '#262D35' : '#0E161F'
          }`,
          width: modalWidth,
          left: '30%',
          height: modalHeight,
          translate: modalTranslateX,
        }}
      >
        <VQSharedGraph
          graphData={graphData}
          currentQuestion={currentQuestion}
          modalWidth={modalWidth}
          surveyId={gid}
          isMobile={isMobile}
          setShowGraph={setShowGraph}
          handleNext={handleMoveDown}
          choicesCount={currentQuestion.choices.length}
          closed={currentQuestion.status === 'Closed'}
          verified={verified === 'true' ? true : false}
        />
      </Modal>

      <ScrollButton
        direction="up"
        onClick={handleMoveUp}
        isHidden={current === 0}
      />
      <ScrollButton
        direction="down"
        onClick={handleMoveDown}
        isHidden={current === videoQuestionsBank.length - 1}
      />
    </div>
  );
};
