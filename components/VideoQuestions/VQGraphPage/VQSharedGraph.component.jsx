import { useState, useEffect } from 'react';
import { Modal } from 'antd';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { useGraphTime } from 'hooks';
import { start, end } from 'utils';
import styles from '../VideoPlayer/VidoePlayer.module.scss';
import {
  CQVerifiedButtons,
  CQVerifiedButtonsMobile,
} from './VQVerifiedButtons.component';

import { useMediaQuery } from 'react-responsive';

import { BarChartDesktopHorizontal } from '../VQBarCharts/BarChartDesktopHorizontal';
import { BarChartDesktopVertical } from '../VQBarCharts/BarChartDesktopVertical';
import { BarChartMobileHorizontal } from '../VQBarCharts/BarChartMobileHorizontal';
import { BarChartMobileVertical } from '../VQBarCharts/BarChartMobileVertical';

export const VQSharedGraph = ({
  graphData,
  currentQuestion,
  shareableLink,
  isMobile,
  setShowGraph,
  handleNext,
  choicesCount,
  closed,
  verified,
}) => {
  const { auth, user } = useSelector((state) => state?.auth);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [activeTab, setActiveTab] = useState('Unverified');
  const [results, setResults] = useState(null);

  const [touchStartY, setTouchStartY] = useState(0);
  const [touchMoveY, setTouchMoveY] = useState(0);

  const [, sendEndRequest] = useGraphTime(
    currentQuestion.qid,
    currentQuestion.status === 'Open' ? 'open' : 'closed',
    start,
    end
  );

  async function processGraphTimeEnd() {
    sendEndRequest();
  }

  const router = useRouter();

  useEffect(() => {
    if (verified) {
      setResults({
        results: graphData?.data?.results?.[0]?.all,
        answer: graphData?.answer,
      });
      if ((user?.gender && user?.year_of_birth) || verified) {
        setActiveTab('All');
      } else {
        setActiveTab('Verified');
      }
    } else {
      setResults(graphData);
    }
  }, [graphData]);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const isDesktop = useMediaQuery({ query: '(min-width: 768px)' });
  const isNestHub = useMediaQuery({ query: '(max-width: 1024px)' });

  // Function to handle touch start
  const handleTouchStart = (e) => {
    setTouchStartY(e.touches[0].clientY);
  };

  // Function to handle touch move
  const handleTouchMove = (e) => {
    setTouchMoveY(e.touches[0].clientY);
  };

  // Function to handle touch end
  const handleTouchEnd = () => {
    const swipeThreshold = 60;
    if (touchStartY - touchMoveY > swipeThreshold) {
      handleMoveDown();
    }
  };

  const handleMoveDown = () => {
    setShowGraph(false);
    // wait for 200 ms for animation to complete and then move to next video
    setTimeout(() => {
      handleNext();
      processGraphTimeEnd();
    }, 200);
  };

  if (!results) return null;
  return (
    <div>
      <div>
        <div
          className={`${styles['cqo_scroll-container']} survey-open-main max-w-950 mx-auto md:my-auto rounded-8 pb-[30px] pt-[1px]`}
        >
          {isMobile && (
            <div
              style={{
                marginTop: '28px',
              }}
            >
              {/* Empty div on mobile to bring question further down */}
            </div>
          )}

          <p
            className={`font-semibold text-white ${
              isMobile ? '' : 'mt-40px'
            } px-20px line-height-30px ${
              isMobile ? '' : 'display-flex items-center justify-center'
            }`}
            style={{
              fontSize: '20px',
              marginLeft: isMobile ? '-13px' : '0px',
              marginTop: isMobile ? '20px' : '0px',
            }}
          >
            {currentQuestion?.question}
          </p>
          {isDesktop ? (
            <div
              className={`map-img-otr flex justify-center mt-[50px] px-[80px] ${
                isNestHub ? 'mb-[10px]' : 'mb-[40px]'
              }`}
              style={isNestHub ? { zoom: '0.75' } : null}
            >
              {choicesCount > 4 ? (
                <BarChartDesktopVertical
                  results={results}
                  auth={auth}
                  verified={verified}
                  shareableLink={shareableLink}
                  question={currentQuestion?.question}
                  closed={closed}
                  choicesCount={choicesCount}
                />
              ) : (
                <BarChartDesktopHorizontal
                  results={results}
                  auth={auth}
                  verified={verified}
                  shareableLink={shareableLink}
                  question={currentQuestion?.question}
                  closed={closed}
                  choicesCount={choicesCount}
                />
              )}
            </div>
          ) : (
            <div className="map-img-otr flex justify-center">
              {choicesCount > 3 ? (
                <BarChartMobileVertical
                  results={results}
                  auth={auth}
                  verified={verified}
                  shareableLink={shareableLink}
                  question={currentQuestion?.question}
                  closed={closed}
                  choicesCount={choicesCount}
                />
              ) : (
                <BarChartMobileHorizontal
                  results={results}
                  auth={auth}
                  verified={verified}
                  shareableLink={shareableLink}
                  question={currentQuestion?.question}
                  closed={closed}
                  choicesCount={choicesCount}
                />
              )}
            </div>
          )}

          {isDesktop ? (
            <CQVerifiedButtons
              graphData={graphData}
              user={user}
              activeTab={activeTab}
              verified={verified}
              setResults={setResults}
              setActiveTab={setActiveTab}
              showModal={showModal}
              auth={auth}
            />
          ) : (
            <CQVerifiedButtonsMobile
              graphData={graphData}
              user={user}
              activeTab={activeTab}
              verified={verified}
              setResults={setResults}
              setActiveTab={setActiveTab}
              showModal={showModal}
              auth={auth}
            />
          )}

          {isDesktop && (
            <div className="w-full">
              <div className="video-icon-otr flex justify-end items-center ">
                {/* Container for the "Move Down" button */}
                <div
                  className="bg-boxbg cursor-pointer"
                  style={{
                    // Round the corners of the arrow
                    borderRadius: '32%', // Reduce the width
                    height: '52px',
                    width: '52px',
                    justifyItems: 'center',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',

                    // Fixed positioning
                    position: 'fixed',
                    bottom: '20px', // Adjust this value for vertical positioning
                    right: '20px', // Adjust this value for horizontal positioning
                    zIndex: 1000, // Adjust this value as needed
                  }}
                  onClick={handleMoveDown}
                >
                  <img
                    className="object-contain"
                    width="32px"
                    height="32px"
                    src="/svg/jump-icon.svg"
                    alt="logo"
                  />
                </div>
              </div>
            </div>
          )}

          {isMobile && (
            <div>
              <div className="video-icon-otr flex justify-center items-center">
                <div
                  style={{
                    borderRadius: '16%',
                    backgroundColor: 'transparent',
                  }}
                  className={`${styles['cqo_next-icon']} mt-3 next-icon-inr flex w-w-66 h-h-58 justify-center bg-boxbg cursor-pointer items-center jumping-icon`}
                  onClick={handleMoveDown}
                  onTouchStart={handleTouchStart}
                  onTouchMove={handleTouchMove}
                  onTouchEnd={handleTouchEnd}
                >
                  <img
                    className="object-contain"
                    width="32px"
                    height="32px"
                    src="/svg/jump-icon.svg"
                    alt="logo"
                  />
                </div>
              </div>
            </div>
          )}

          <Modal
            title="Basic Modal"
            open={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
          >
            <p className="heading-h3 font-normal">
              If you want to see the answers given by verified users then make a
              profile{' '}
              <span
                className="underline cursor-pointer"
                onClick={() => {
                  localStorage.setItem('questionID', currentQuestion?.qid);
                  router.push('/sign-up');
                }}
              >
                here
              </span>
              .
            </p>
          </Modal>
        </div>
      </div>
    </div>
  );
};
