import React, { useState, useEffect } from 'react';

const GetShareableLink = ({ qid, sruveyID, user, isMobile }) => {
  const [copied, setCopied] = useState(false);
  const [hideShare, setHideShare] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      if (copied) {
        setHideShare(true);
      }
    }, 2000);
    return () => clearTimeout(timer);
  }, [copied]);

  return (
    <div className="flex items-center justify-between mt-[16px]">
      {copied &&
        (hideShare ? (
          <div className="share-text-otr">
            <p className="heading-XS"></p>
          </div>
        ) : (
          <div className="action-otr flex relative">
            <a className="text-[#010202] font-semibold mb-[10px] md:mb-10  w-fit whitespace-nowrap md:font-semibold rounded-50 bg-white hover:text-[#010202] pt-[8px] pl-[14px] pr-[14px] pb-[8px] sm:absolute am:get_shareable_link left-0 bottom-[-30px] md:bottom-[-20px]">
              Link copied to clipboard.
            </a>
          </div>
        ))}
      {!copied ? (
        <a
          className="share-text-otr share_txt_graph flex items-center gap-[16px]"
          onClick={() => {
            navigator?.clipboard?.writeText(
              `${
                process.env.NEXT_PUBLIC_APP_URL
              }/dilemmas/${qid}?gid=${sruveyID}&verified=${
                user?.year_of_birth && user?.gender ? 'true' : 'false'
              }`
            );
            setCopied(!copied);
          }}
        >
          {isMobile ? (
            <>
              🔗
              <p
                style={{
                  fontSize: '15px',
                  lineHeight: '3px',
                }}
                className="text-white "
              >
                Get shareable link
              </p>
            </>
          ) : (
            <>
              🔗
              <p
                style={{
                  fontSize: '16px',
                  lineHeight: '3px',
                  fontWeight: '300',
                  marginBottom: '15px',
                  padding: '8px',
                }}
                className="text-white "
              >
                Get shareable link
              </p>
            </>
          )}
        </a>
      ) : null}
    </div>
  );
};

export default GetShareableLink;
