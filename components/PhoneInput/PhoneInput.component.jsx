import { Input } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useOnClickOutside } from 'hooks/useOutsideClick.hook';
import { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setCountryCode, setCountryDetails } from 'store/global';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import {
  specialCountries,
  countryCodes,
} from 'utils/data/login-page/countryCodes';

const theme = createTheme({
  palette: {
    primary: {
      // Purple and green play nicely together.
      main: '#845EC2',
    },
  },
});

const SearchInput = ({ setFilterText }) => {
  return (
    <ThemeProvider theme={theme}>
      <div className="px-[12px]">
        <Input
          autoFocus
          onChange={(e) => setFilterText(e?.target?.value)}
          sx={{
            height: 50,
            display: 'flex',
            alignItems: 'center',
            borderBottom: '1px solid #845EC2',
            '&::before': {
              borderBottom: '1px solid #845EC2 !important',
            },
            '& .MuiInputBase-input': {
              paddingLeft: '8px',
              paddingTop: 0,
              paddingBottom: 0,
            },
          }}
          placeholder="Search country"
          color="primary"
          startAdornment={
            <>
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M20.0007 20.0007L16.377 16.377"
                  stroke="#323232"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M4 11.25C4 15.2541 7.24594 18.5 11.25 18.5C15.2541 18.5 18.5 15.2541 18.5 11.25C18.5 7.24594 15.2541 4 11.25 4V4C7.24606 4.00029 4.00029 7.24606 4 11.25"
                  stroke="#323232"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </>
          }
        />
      </div>
    </ThemeProvider>
  );
};

const CountryBox = ({
  code,
  setCountryDetails,
  setCountryCode,
  setShowDropdown,
  setFilterText,
}) => {
  return (
    <div
      className="flex items-center justify-between min-h-[45px] px-[12px] select-none hover:bg-[#F4EEFF] active:bg-[#d6c4f6] bg-[#fff] transition-all cursor-pointer"
      key={code?.code}
      onClick={() => {
        setCountryDetails({
          name: code?.name,
          code: code?.code,
        });
        setCountryCode(code?.dial_code);
        setShowDropdown(false);
        setFilterText('');
      }}
    >
      <div className="flex items-center gap-[8px]">
        <img
          src={`/img/flag-icons/${code?.code}.png`}
          alt={code?.code}
          style={{ width: '20.826px', height: '15.3px' }}
        />
        <div className="text-[#010202] text-[12px] xxs:text-[14px] sm:text-[14px] text-ellipsis w-[105px] xxs:w-[120px] sm:w-[95px] whitespace-nowrap inline-block overflow-hidden self-auto">
          {code?.name}
        </div>
      </div>
      <div className="text-[#010202] text-[12px] xxs:text-[14px] sm:text-[14px]">
        {code?.dial_code}
      </div>
    </div>
  );
};

export const CustomPhoneInput = ({
  phoneNumber,
  setPhoneNumber,
  showLabel,
  setShowLabel,
  countryCodeWidth,
  passwordType,
  phoneInputClassName,
  phoneFlagClassName,
  dropdownWidthClassName,
  dropdownHeightClassName,
  inputFontSizeClassName,
}) => {
  const dispatch = useDispatch();

  const user = useSelector((state) => state?.auth?.user);
  const { countryDetails } = useSelector((state) => state?.global);

  const [showDropdown, setShowDropdown] = useState(false);
  const [filterText, setFilterText] = useState('');
  const [filteredCountries, setFilteredCountries] = useState([]);
  const [pass, setPass] = useState(true);

  const ref = useRef();

  useOnClickOutside(ref, () => setShowDropdown(false));

  const handleCountryCode = (code) => {
    dispatch(setCountryCode(code));
  };

  const handleCountryDetails = (details) => {
    dispatch(setCountryDetails(details));
  };

  useEffect(() => {
    if (user) {
      const fullCountries = [...countryCodes, ...specialCountries];
      const userCountry = fullCountries?.find(
        (country) => country?.name === user?.country
      );
      handleCountryDetails({
        code: userCountry?.code,
        name: userCountry?.name,
      });
    }
  }, [user]);

  useEffect(() => {
    if (filterText) {
      const fullCountries = [...countryCodes, ...specialCountries];
      const filtered = fullCountries?.filter((country) => {
        const value =
          country?.name?.toLowerCase()?.startsWith(filterText?.toLowerCase()) ||
          country?.dial_code
            ?.toLowerCase()
            ?.includes(filterText?.toLowerCase());
        return value;
      });
      const sortedFiltered = filtered?.sort(function (a, b) {
        const first = a?.dial_code?.substring(1, a.dial_code.length);
        const second = b?.dial_code?.substring(1, b.dial_code.length);
        return first - second;
        // return a?.name - b?.name;
      });
      setFilteredCountries(sortedFiltered);
    } else {
      setFilteredCountries([]);
    }
  }, [filterText]);

  let width = countryCodeWidth;
  if (!countryCodeWidth) {
    width = 180;
  }

  return (
    <div
      className={`grid gap-[17px] ${phoneInputClassName}`}
      style={{ gridTemplateColumns: `${width}px auto` }}
    >
      <div className={`relative h-[52px] ${dropdownHeightClassName}`} ref={ref}>
        {/* DropDown Button */}
        <div
          className="bg-[#fff] rounded-[4px] flex items-center justify-between px-[14px] cursor-pointer h-full"
          style={{ userSelect: 'none' }}
          onClick={() => setShowDropdown((dropdown) => !dropdown)}
        >
          <div className={`flex items-center gap-[7px] ${phoneFlagClassName}`}>
            <img
              src={`/img/flag-icons/${countryDetails?.code}.png`}
              alt={countryDetails?.code}
              style={{ width: '20.826px', height: '15.3px' }}
            />
            <p className="text-[15px] font-[300] text-black text-ellipsis w-[100px] whitespace-nowrap inline-block overflow-hidden">
              {countryDetails?.name}
            </p>
          </div>
          <div className="inline-flex">
            <svg
              width="10"
              height="5"
              viewBox="0 0 10 5"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M0 0L5 5L10 0H0Z" fill="#010202" />
            </svg>
          </div>
        </div>
        {/* Dropdown Content */}
        {showDropdown ? (
          <div
            className={`w-[210px] rounded-[8px] bg-[#fff] absolute top-[64px] max-h-[40vh] overflow-y-scroll countries-dropdown  ${dropdownWidthClassName}`}
            style={{ zIndex: 999 }}
          >
            {/* Input */}
            <SearchInput setFilterText={setFilterText} />
            {/* If Filter Countries Then Show Them, Else show all */}
            {filteredCountries?.length ? (
              <>
                {/* Filtered Country Codes */}
                {filteredCountries?.map((code) => (
                  <CountryBox
                    key={code?.name}
                    code={code}
                    setShowDropdown={setShowDropdown}
                    setCountryDetails={handleCountryDetails}
                    setCountryCode={handleCountryCode}
                    setFilterText={setFilterText}
                  />
                ))}
              </>
            ) : (
              <>
                {/* Special Country Codes */}
                {specialCountries?.map((code) => (
                  <CountryBox
                    key={code?.name}
                    code={code}
                    setShowDropdown={setShowDropdown}
                    setCountryDetails={handleCountryDetails}
                    setCountryCode={handleCountryCode}
                    setFilterText={setFilterText}
                  />
                ))}
                {/* Divider */}
                <div
                  className="my-[12px]"
                  style={{ border: '1px solid #dedede' }}
                />
                {/* All Country Codes */}
                {countryCodes?.map((code) => (
                  <CountryBox
                    key={code?.name}
                    code={code}
                    setShowDropdown={setShowDropdown}
                    setCountryDetails={handleCountryDetails}
                    setCountryCode={handleCountryCode}
                    setFilterText={setFilterText}
                  />
                ))}
              </>
            )}
          </div>
        ) : (
          <></>
        )}
      </div>
      <div className="relative h-[52px]">
        <label
          htmlFor="phone"
          className={` font-medium pt-[5px] text-[10px] text-textGray transition-all duration-200 ease-in absolute flex pl-[15px]`}
        >
          {showLabel ? 'Mobile Phone' : ''}
        </label>
        <div className="w-full ">
          <input
            contentEditable="true"
            id="phone"
            name="phone"
            placeholder="Your mobile phone number"
            type={!passwordType ? 'tel' : pass ? 'password' : 'tel'}
            required
            value={phoneNumber}
            onChange={(e) => {
              setPhoneNumber(e?.target?.value);
            }}
            onKeyDown={(e) => {
              const isNumber = /^\d+$/.test(e?.key);
              const isCtrlA = e?.ctrlKey && e?.code === 'KeyA';
              const isCommandA = e?.metaKey && e?.code === 'KeyA';
              const isCtrlV = e?.ctrlKey && e?.code === 'KeyV';
              const isCommandV = e?.metaKey && e?.code === 'KeyV';
              const isCtrlC = e?.ctrlKey && e?.code === 'KeyC';
              const isCommandC = e?.metaKey && e?.code === 'KeyC';
              const isCtrlX = e?.ctrlKey && e?.code === 'KeyX';
              const isCommandX = e?.metaKey && e?.code === 'KeyX';
              const isCtrlZ = e?.ctrlKey && e?.code === 'KeyZ';
              const isCommandZ = e?.metaKey && e?.code === 'KeyZ';

              const isDelete = e?.key === 'Delete';

              const isBackSpace = e?.key === 'Backspace';
              const isTab = e?.key === 'Tab';

              const isArrowLeft = e?.key === 'ArrowLeft';
              const isArrowRight = e?.key === 'ArrowRight';

              const isPaste = e?.type === 'paste';
              const isCut = e?.type === 'cut';
              const isCopy = e?.type === 'copy';
              const isUndo = e?.type === 'undo';
              const isRedo = e?.type === 'redo';
              const isSelectAll = e?.type === 'selectall';
              const isDeleteAll = e?.type === 'deleteall';
              const isDeleteContent = e?.type === 'deletecontent';
              const isDeleteByCut = e?.type === 'deletebycut';
              // Edit on mobile
              const isEdit = e?.type === 'edit';
              const isInput = e?.type === 'input';
              const isBeforeInput = e?.type === 'beforeinput';
              const isCompositionStart = e?.type === 'compositionstart';

              if (
                !(
                  isNumber ||
                  isCtrlA ||
                  isCommandA ||
                  isBackSpace ||
                  isTab ||
                  isCtrlV ||
                  isCommandV ||
                  isCtrlC ||
                  isCommandC ||
                  isCtrlX ||
                  isCommandX ||
                  isCtrlZ ||
                  isCommandZ ||
                  isDelete ||
                  isPaste ||
                  isCut ||
                  isCopy ||
                  isUndo ||
                  isRedo ||
                  isSelectAll ||
                  isDeleteAll ||
                  isDeleteContent ||
                  isDeleteByCut ||
                  isArrowLeft ||
                  isArrowRight ||
                  isEdit ||
                  isInput ||
                  isBeforeInput ||
                  isCompositionStart
                )
              ) {
                e?.preventDefault();
              }
            }}
            onFocus={() => setShowLabel(true)}
            style={{ WebkitUserModify: 'read-write-plaintext-only' }}
            className={`auth-phone-input-height ${
              showLabel ? 'pt-[24px]' : ''
            } placeholder:textGray text-textLogin w-full border-white focus:border-boxtext  border-2 focus:shadow-inputShadow bg-white  block h-[52px] appearance-none rounded-md px-3 py-4  shadow-sm  focus:outline-none text-[15px] placeholder-[15px] ${inputFontSizeClassName}`}
          />
          {passwordType ? (
            <button
              className="border-0 bottom-0 m-auto h-mx absolute top-0 right-1 text-dark text-lg bg-transparent inline-flex cursor-pointer"
              onClick={() => setPass((prev) => !prev)}
              type="button"
            >
              {pass ? <FaEyeSlash /> : <FaEye />}
            </button>
          ) : null}
        </div>
      </div>
    </div>
  );
};
