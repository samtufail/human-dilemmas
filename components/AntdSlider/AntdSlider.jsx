import { Slider } from 'antd';

const marks = {
  0: { label: '0', style: { color: '#FEFE', padding: '20px' } },
  1: { label: '1', style: { color: '#FEFE', padding: '20px' } },
  2: { label: '2', style: { color: '#FEFE', padding: '20px' } },
  3: { label: '3', style: { color: '#FEFE', padding: '20px' } },
  4: { label: '4', style: { color: '#FEFE', padding: '20px' } },
  5: { label: '5', style: { color: '#FEFE', padding: '20px' } },
  6: { label: '6', style: { color: '#FEFE', padding: '20px' } },
  7: { label: '7', style: { color: '#FEFE', padding: '20px' } },
  8: { label: '8', style: { color: '#FEFE', padding: '20px' } },
  9: { label: '9', style: { color: '#FEFE', padding: '20px' } },
  10: { label: '10', style: { color: '#FEFE', padding: '20px' } },
};

export const AntdSlider = ({
  feedbackPoints,
  setFeedPoints,
  setGotIninitalFeedback,
}) => {
  const handleSliderChange = (value) => {
    setFeedPoints(value);
    setGotIninitalFeedback(true);
  };

  return (
    <div className="text-center flex items-center justify-center flex-col gap-[30px]">
      <Slider
        min={0}
        max={10}
        step={1}
        marks={marks}
        value={feedbackPoints} // Add value prop here
        defaultValue={feedbackPoints} // Add defaultValue prop here
        onChange={handleSliderChange}
        style={{ width: 500, height: 60 }}
        trackStyle={{
          backgroundColor: '#18355A',
          height: 10,
        }}
        handleStyle={{
          borderColor: '#51677D',
          backgroundColor: '#52687E',
          width: 12,
          height: 40,
          marginTop: -15,
          borderRadius: 5,
        }}
        railStyle={{ backgroundColor: '#18355A', height: 10 }}
        dotStyle={{ display: 'none' }}
      />
    </div>
  );
};
