import { CloseOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import { AntdSlider } from 'components/AntdSlider/AntdSlider';
import { useState, useEffect } from 'react';
import { ProgressBar } from 'components/ProgressBar/ProgressBar.component';

const fixedKeywords = [
  'boring',
  'negative',
  'biased',
  'harmful',
  'age sensitive',
  'racist',
  "i don't know",
];

const RenderFixedKeywords = ({
  fixedKeywords,
  setFixedKeywordAsFeedback,
  fixedKeywordAsFeedback,
}) => {
  return (
    <div className="action-otr flex flex-wrap mt-[20px] mb-[20px] w-full justify-start">
      {fixedKeywords.map((keyword) => (
        <p
          key={keyword}
          className={`cursor-pointer heading-S text-white rounded-50 bg-${
            fixedKeywordAsFeedback === keyword ? '[#845EC2]' : 'gray-700'
          } hover:bg-[#263547] hover:text-white px-3 py-1 mr-2 mb-2`}
          style={{ maxWidth: '200px', fontSize: '17px', fontWeight: 14 }}
          onClick={() => {
            setFixedKeywordAsFeedback(keyword);
          }}
        >
          {keyword}
        </p>
      ))}
    </div>
  );
};

export const QuestionFeedbackTwo = ({
  questionsToReview,
  visible,
  setVisible,
}) => {
  const [index, setIndex] = useState(0);

  const [questionToReview, setQuestionToReview] = useState(
    questionsToReview[index]
  );
  const percent = Math.round(((index + 1) / questionsToReview?.length) * 100);
  const [feedbackPoints, setFeedPoints] = useState(0); // Saves the feedback points from Slider
  const [feedbackPointsIndependent, _] = useState(feedbackPoints); // Makes the feedbackPoints independent of the state
  const [fixedKeywordAsFeedback, setFixedKeywordAsFeedback] = useState(''); // Saves the fixed keyword as feedback
  const [customFeedback, setCustomFeedback] = useState(''); // Saves the custom feedback using the input field

  const [gotIninitalFeedback, setGotIninitalFeedback] = useState(false); // Decides to show the second feedback in combination with feedbackPoints
  const [isBadReview, setIsBadReview] = useState(false); // Decider to get fixedKeywordAsFeedback or customFeedback
  const [gotFeedback, setGotFeedback] = useState(false); // Decider to move for the next question

  useEffect(() => {
    // Evaluate the feedback points if they are less than 5 then set the isBadReview to true
    // This setup the stage for the second feedback if the user has given a bad review
    if (gotIninitalFeedback && feedbackPoints < 6) {
      setIsBadReview(true);
    } else {
      setGotFeedback(true);
    }
    if (fixedKeywordAsFeedback !== '') {
      setGotFeedback(true);
    }
    if (customFeedback !== '') {
      setGotFeedback(true);
    }
  }, [fixedKeywordAsFeedback, customFeedback, feedbackPointsIndependent]);

  const handleNextQuestionForFeedback = () => {
    let totalQuestions = questionsToReview.length;
    if (index < totalQuestions - 1) {
      //   setFeedPoints(0);
      setIndex(index + 1);
      //   setQuestionToReview(questionsToReview[index + 1]);

      setGotIninitalFeedback(false);
      setIsBadReview(false);
      setGotFeedback(false);
      setFixedKeywordAsFeedback('');
      setCustomFeedback('');
    } else {
      setVisible(false);
    }
  };

  useEffect(() => {
    setQuestionToReview(questionsToReview[index]);
  }, [index]);

  const handleFeedbackEvaluation = () => {
    if (gotIninitalFeedback && feedbackPoints < 5) {
      setIsBadReview(true);
    }
    if (gotFeedback) {
      handleNextQuestionForFeedback();
    }
    if (feedbackPoints > 5) {
      setGotFeedback(true);
      handleNextQuestionForFeedback();
    }
  };

  return (
    <Modal
      open={visible}
      onCancel={() => setVisible(false)}
      bodyStyle={{
        padding: 25,
        background: '#0E161F',
        border: '1px solid #6E7378',
        borderRadius: '16px',
      }}
      centered
      className="max-w-[837px] top-[28%]"
      closeIcon={<CloseOutlined style={{ color: '#fff' }} />}
    >
      <div style={{ marginTop: '30px' }}>
        <ProgressBar percent={percent} />
      </div>

      <div className="flex items-center justify-start flex-col gap-[50px]">
        {!isBadReview ? (
          <>
            <h2
              className="text-white text-[22px] font-[200] mt-[32px]"
              style={{ color: '#ffff', fontStyle: 'italic' }}
            >
              Question: {`"${questionToReview.question}"`}
            </h2>
            <h2 className="text-white text-[13px] font-[300] mt-[1px]">
              How interesting did you find this question on a scale from 0 to 10
              (where 10 is the most interesting)?
            </h2>

            <AntdSlider
              feedbackPoints={feedbackPoints}
              setFeedPoints={setFeedPoints}
              setGotIninitalFeedback={setGotIninitalFeedback}
            />
          </>
        ) : null}

        {/* Map RenderFixedKeywords here  */}

        {isBadReview && (
          <h2 className="text-white text-[22px] font-[300] mt-[32px]">
            Please tell us why you found this question less interesting
          </h2>
        )}

        {isBadReview && (
          <RenderFixedKeywords
            fixedKeywords={fixedKeywords}
            setFixedKeywordAsFeedback={setFixedKeywordAsFeedback}
            fixedKeywordAsFeedback={fixedKeywordAsFeedback}
          />
        )}

        {isBadReview && (
          <form onSubmit={(e) => e.preventDefault()} className="w-full">
            <input
              type="text"
              id="feedback"
              style={{
                outline: 'none',
                border: '4px  #6E7378',
                textAlign: 'start',
                fontSize: '17px',
                fontWeight: 14,
              }}
              onChange={(e) => setCustomFeedback(e?.target?.value)}
              value={customFeedback}
              className="bg-[#0E161F] text-white rounded-[16px] text-[20px] w-full p-[8px]"
              placeholder="Or type something here ..."
            />
          </form>
        )}

        <div className="w-full">
          <div className="video-icon-otr flex justify-end">
            <div
              className="mr-[10px] sm:mr-[15px] sm:mb-[15px] sm:mt-[35px] p-[10px] rounded-[12px] sm:rounded-[16px] h-[36px] w-[42px] sm:h-h-48  sm:w-[56px] flex justify-center bg-boxbg cursor-pointer items-center"
              onClick={handleFeedbackEvaluation}
            >
              <img
                className="object-contain"
                width={32}
                height={32}
                src="/svg/next-arrow.svg"
                alt="logo"
              />
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
};
