import React, { useState } from 'react';

import { CloseOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import { AntdSlider } from 'components/AntdSlider/AntdSlider';

const fixedKeywords = [
  'Boring',
  'Badly formulated',
  'Biased',
  'Insensitive',
  'Hard to say',
  'Interesting',
  'Engaging',
  'Poorly Written',
  'It’s relatable',
];

export const QuestionFeedback = ({
  question,
  visible,
  setVisible,
  evaluateFeedback,
  feedbackPoints,
  setFeedPoints,
  setGotIninitalFeedback,
  secondFeedback,
  setCustomFeedback,
  setFixedKeywordsAsFeedback,
  fixedKeywordsAsFeedback,
}) => {
  const [isArrowNextHovered, setIsArrowNextHovered] = useState(false);

  const handleArrowNextHover = () => {
    setIsArrowNextHovered(true);
  };

  const handleArrowNextMouseLeave = () => {
    setIsArrowNextHovered(false);
  };

  const RenderFixedKeywords = ({
    fixedKeywords,
    setFixedKeywordsAsFeedback,
    fixedKeywordsAsFeedback,
  }) => {
    const isSelected = (keyword) => fixedKeywordsAsFeedback.includes(keyword);

    const toggleKeywordSelection = (keyword) => {
      const selectedKeywords = isSelected(keyword)
        ? fixedKeywordsAsFeedback.filter((selected) => selected !== keyword)
        : [...fixedKeywordsAsFeedback, keyword];
      setFixedKeywordsAsFeedback(selectedKeywords);
    };
    return (
      <div className="action-otr flex flex-wrap mt-[20px] mb-[20px] w-full justify-start">
        {fixedKeywords.map((keyword) => (
          <p
            key={keyword}
            className={`cursor-pointer heading-S text-white rounded-50 bg-${
              isSelected(keyword) ? '[#845EC2]' : '[#263547]'
            } hover:bg-[#263547] hover:text-white px-3 py-1 mr-2 mb-2`}
            style={{ maxWidth: '200px', fontSize: '17px', fontWeight: 14 }}
            onClick={() => {
              toggleKeywordSelection(keyword);
            }}
          >
            {keyword}
          </p>
        ))}
      </div>
    );
  };

  return (
    <Modal
      open={visible}
      onCancel={() => setVisible(false)}
      bodyStyle={{
        padding: 25,
        background: '#0E161F',
        border: '1px solid #6E7378',
        borderRadius: '16px',
      }}
      centered
      className="max-w-[837px] top-[28%]"
      closeIcon={<CloseOutlined style={{ color: '#fff' }} />}
    >
      <div className="flex items-center justify-start flex-col gap-[50px]">
        {!secondFeedback ? (
          <>
            <h2 className="text-white text-[20px] font-[300] mt-[32px]">
              How interesting did you find this question on a scale from 0 to
              10? Where 10 is the most interesting.
            </h2>

            <AntdSlider
              feedbackPoints={feedbackPoints}
              setFeedPoints={setFeedPoints}
              setGotIninitalFeedback={setGotIninitalFeedback}
            />
          </>
        ) : null}

        {secondFeedback && (
          <h2 className="text-white text-[22px] font-[300] mt-[32px]">
            If you want to give a reason why...
          </h2>
        )}

        {secondFeedback && (
          <RenderFixedKeywords
            fixedKeywords={fixedKeywords}
            setFixedKeywordsAsFeedback={setFixedKeywordsAsFeedback}
            fixedKeywordsAsFeedback={fixedKeywordsAsFeedback}
          />
        )}

        {secondFeedback && (
          <form onSubmit={(e) => e.preventDefault()} className="w-full">
            <textarea
              id="feedback"
              style={{
                outline: 'none',
                border: '2px solid #6E7378',
                textAlign: 'start',
                fontSize: '17px',
                fontWeight: 16,
              }}
              onChange={(e) => setCustomFeedback(e?.target?.value)}
              className="bg-[#0E161F] text-white rounded-[16px] text-[20px] w-full p-[10px]"
              placeholder="Please tell us..."
              rows="3"
            ></textarea>
          </form>
        )}

        <div className="w-full">
          <div className="video-icon-otr flex justify-end items-center">
            <h className="text-white text-sm font-[280] flex-grow mt-5 mr-9 w-[10px]">
              {`"${question}"`}
            </h>

            {!secondFeedback && (
              <div
                className="mr-[10px] sm:mr-[15px] sm:mb-[15px] sm:mt-[35px] p-[10px] rounded-[12px] sm:rounded-[16px] h-[36px] w-[42px] sm:h-h-48  sm:w-[56px] flex justify-center bg-boxbg cursor-pointer items-center"
                onClick={() => evaluateFeedback()}
              >
                <img
                  className="object-contain"
                  width={32}
                  height={32}
                  src="/svg/next-arrow.svg"
                  alt="logo"
                />
              </div>
            )}

            {secondFeedback && (
              <>
                <div
                  className={`mr-[10px] sm:mr-[15px] sm:mb-[15px] sm:mt-[35px] p-[10px] rounded-[12px] sm:rounded-[16px] h-[36px] w-[64px] sm:h-[48px] sm:w-[174px] flex justify-center  cursor-pointer items-center font-semibold  hover:text-[#845EC2] hover:bg-[#F9F9F9] ${
                    isArrowNextHovered ? 'bg-[#F9F9F9]' : 'bg-[#845EC2]'
                  }
                  ${isArrowNextHovered ? 'text-[#845EC2]' : 'text-[#F9F9F9]'}

                  `}
                  onClick={() => evaluateFeedback()}
                >
                  Don’t know
                </div>

                <div
                  className="mr-[10px] sm:mr-[15px] sm:mb-[15px] sm:mt-[35px] p-[10px] rounded-[12px] sm:rounded-[16px] h-[36px] w-[42px] sm:h-h-48  sm:w-[56px] flex justify-center cursor-pointer items-center bg-[#F9F9F9] hover:bg-[#845EC2]"
                  onMouseEnter={handleArrowNextHover}
                  onMouseLeave={handleArrowNextMouseLeave}
                  onClick={() => evaluateFeedback()}
                >
                  <img
                    className="object-contain"
                    width={32}
                    height={32}
                    src={
                      isArrowNextHovered
                        ? '/svg/next-arrow.svg'
                        : '/svg/next-arrow-v2.svg'
                    }
                    alt="logo"
                  />
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </Modal>
  );
};
