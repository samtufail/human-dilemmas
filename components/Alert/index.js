import dynamic from 'next/dynamic';

export const OrientationAlert = dynamic(
  import('./orientationAlert').then((mod) => mod.OrientationAlert)
);
