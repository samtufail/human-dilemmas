import React from 'react';
import Image from 'next/image';
import OrientationImage from '../../public/svg/orientation.svg';

export const OrientationAlert = () => {
  return (
    <div
      id="landscape-alert"
      className="h-screen w-screen items-center justify-center p-5 fixed z-50 inset-0 bg-[#00000080]"
    >
      <div className=" bg-black-survey2 px-10 py-8 rounded-md">
        <div className="flex items-center justify-center my-4">
          <Image
            height={60}
            src={OrientationImage}
            alt="change orientation icon"
          />
        </div>
        <p className='text-center'>Switch to portrait mode</p>
      </div>
    </div>
  );
};
