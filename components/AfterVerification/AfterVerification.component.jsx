import { useEffect, useState } from 'react';
import { Select } from 'antd';
import { CaretDownFilled } from '@ant-design/icons';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import Image from 'next/image';
import { api, countryCodes, getSeparatedNumber } from 'utils';
import { ProgressBar } from '../ProgressBar/ProgressBar.component';
import { loggedInUser } from 'store';

const { Option } = Select;

const CustomSelectComponent = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors, setFieldValue }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
}) => (
  <div className="sex-select">
    <Select
      // open={true}
      onChange={(e) => setFieldValue(field?.name, e)}
      popupClassName="sex-select-popup"
      placeholder="Sex"
      className="rounded-[8px] font-[400] mt-[20px] md:mt-[18px] w-full max-w-[310px] md:max-w-[288px] placeholder:text-[#fff] text-[18px]"
      suffixIcon={<CaretDownFilled style={{ color: 'white' }} />}
    >
      <Option value="male">Male</Option>
      <Option value="female">Female</Option>
      <Option value="intersex">Intersex</Option>
      <Option value="notToSay">Prefer not to say</Option>
    </Select>
    {touched[field.name] && errors[field.name] && (
      <div className="mt-[12px] text-[12px]">{errors[field.name]}</div>
    )}
  </div>
);

const CustomInputComponent = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  ...props
}) => (
  <div>
    <input
      type="text"
      {...field}
      {...props}
      className="rounded-[8px] font-[400] mt-[20px] md:mt-[18px] w-full max-w-[310px] md:max-w-[288px] h-[48px] pl-[15px] placeholder:text-[#fff] text-[18px]"
      placeholder="YYYY"
      style={{
        border: '1.5px solid white',
        background: 'transparent',
      }}
    />
    {touched[field.name] && errors[field.name] && (
      <div className="mt-[12px] text-[12px]">{errors[field.name]}</div>
    )}
  </div>
);

export const AfterVerification = ({ active, section }) => {
  const { user } = useSelector((state) => state?.auth);
  const [percent, setPercent] = useState(3);
  const dispatch = useDispatch();
  useEffect(() => {
    const content = document.querySelector('.progressBar-otr .ant-progress-bg');
    if (content && active === 1) {
      content.setAttribute('title', `3%`);
    } else if (content && active !== 1) {
      const percent = (active / section?.questions_count) * 100;
      setPercent(Math.round(percent));
      content.setAttribute('title', `${Math.round(percent)}%`);
    }
  }, [active, section]);

  return (
    <div className="mx-[16px] flex flex-col justify-center h-screen">
      <div
        className={`my-auto bg-[#0E161F] rounded-8 pb-[34px] md:pb-[30px] pt-[47px] md:pt-[70px]`}
      >
        <div className="progressBar-otr px-[30px] md:px-[100px]">
          <ProgressBar percent={percent} />
        </div>

        {/* Questions */}
        <div className="px-[30px] md:px-[100px]">
          <Formik
            initialValues={{ year_of_birth: '', sex: undefined }}
            validationSchema={Yup.object().shape({
              year_of_birth: Yup.number()
                .min(1900, 'Error: Try Again.')
                .typeError('Error: Please enter a number.')
                .required('Error: Field is reuqired.'),
              sex: Yup.string().required(
                'Please select a value from dropdown.'
              ),
            })}
            onSubmit={async (values) => {
              const separated = getSeparatedNumber(user?.phone_number);
              const twoDigitCode = countryCodes?.find(
                (code) =>
                  code?.name === user?.country ||
                  code?.code === user?.country ||
                  code?.dial_code === separated?.countryCode
              )?.code;
              // This is where we save values and proceed;
              const res = await api.put('/v1/user/update/', {
                name: user?.name,
                country: twoDigitCode,
                year_of_birth: values?.year_of_birth,
                gender: values?.sex,
              });
              if (res?.status === 200) {
                await dispatch(loggedInUser());
              } else {
                setName(user?.name);
              }
            }}
          >
            {({ values }) => {
              return (
                <Form className="mt-[39px] md:mt-[64px]">
                  {/* BirthYear */}
                  <div>
                    <label
                      htmlFor="year_of_birth"
                      className="block font-[600] md:font-[500] text-[16px] md:text-[20px]"
                    >
                      What is your year of birth?
                    </label>
                    <Field
                      name="year_of_birth"
                      component={CustomInputComponent}
                      id="year_of_birth"
                    />
                  </div>
                  {/* Gender */}
                  <div className="mt-[39px] md:mt-[56px]">
                    <label
                      htmlFor="year_of_birth"
                      className="block font-[600] md:font-[500] text-[16px] md:text-[20px]"
                    >
                      Your sex assigned at birth?
                    </label>
                    <Field
                      name="sex"
                      component={CustomSelectComponent}
                      id="year_of_birth"
                    />
                  </div>
                  <div className="next-icon-otr flex items-center justify-end pt-[101px] md:pt-[100px]">
                    {values?.year_of_birth && values?.sex ? (
                      <button
                        style={{
                          outline: 'none',
                          border: 'none',
                          display: 'flex',
                          alignItems: 'center',
                        }}
                        className="next-icon-inr flex w-w-56 h-h-48 justify-center bg-boxbg rounded-8 cursor-pointer"
                        type="submit"
                      >
                        <Image
                          className="object-contain"
                          width={32}
                          height={32}
                          src="/svg/next-arrow.svg"
                          alt="logo"
                        />
                      </button>
                    ) : (
                      <></>
                    )}
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </div>
  );
};
