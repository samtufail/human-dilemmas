import { useState, useEffect } from 'react';
import { Modal } from 'antd';
import Image from 'next/image';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { ClosedQuestionsBarChart } from '..';
import { useGraphTime } from 'hooks';
import { start, end } from 'utils';
import styles from '../CQOAnswerPage/CQOAnswerPage.module.scss';
import { useMediaQuery } from 'react-responsive';
import {
  CQVerifiedButtons,
  CQVerifiedButtonsMobile,
} from '../CQOGraphPage/CQOVerifiedButtons.component';
import { ClosedQuestionsBCMobile } from '../ClosedQuestionsBarChart/ClosedQuestionsBCMobile.component';

export const CQCGraphPage = ({
  graphData,
  setGraphData,
  setShowGraph,
  active,
  setActive,
  questions,
  verified,
  shareableLink,
}) => {
  const [copied, setCopied] = useState(false);
  const { auth } = useSelector((state) => state?.auth);
  const { user } = useSelector((state) => state?.auth);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [activeTab, setActiveTab] = useState('Unverified');
  const [results, setResults] = useState(null);
  const [hideShare, setHideShare] = useState(false);
  const [, sendEndRequest] = useGraphTime(
    questions[active - 1].unique_QID,
    'closed',
    start,
    end
  );

  const router = useRouter();

  async function processGraphTimeEnd() {
    sendEndRequest();
  }

  const { surveyId } = router.query;
  useEffect(() => {
    if (auth || verified) {
      setResults({
        results: graphData?.data?.results?.[0]?.all,
        answer: graphData?.answer,
      });
      if ((user?.gender && user?.year_of_birth) || verified) {
        setActiveTab('All');
      } else {
        setActiveTab('Verified');
      }
    } else {
      setResults(graphData);
    }
  }, [graphData]);

  useEffect(() => {
    const timer = setTimeout(() => {
      if (copied) {
        setHideShare(true);
      }
    }, 2000);
    return () => clearTimeout(timer);
  }, [copied]);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const isDesktop = useMediaQuery({ query: '(min-width: 768px)' });
  const isNestHub = useMediaQuery({ query: '(max-width: 1024px)' });

  if (!results) return null;
  return (
    <div className={styles['cqo_main-container']}>
      <div className={styles['cqo_container']}>
        <div
          className={`${styles['cqo_scroll-container']} survey-open-main max-w-950 mx-auto md:my-auto bg-black-survey rounded-8 pb-[30px] pt-[1px]`}
        >
          <p
            className={`${styles['cqo_question']} heading-M font-semibold text-white mt-[40px] px-[100px]`}
          >
            {questions[active - 1].question}
          </p>
          {isDesktop ? (
            <div
              className={`map-img-otr flex justify-center mt-[32px] px-[80px] ${
                isNestHub ? 'mb-[10px]' : 'mb-[40px]'
              }`}
              style={isNestHub ? { zoom: '0.75' } : null}
            >
              <ClosedQuestionsBarChart
                results={results}
                auth={auth}
                verified={verified}
                shareableLink={shareableLink}
              />
            </div>
          ) : (
            <div className="map-img-otr flex justify-center">
              <ClosedQuestionsBCMobile
                results={results}
                auth={auth}
                verified={verified}
                shareableLink={shareableLink}
              />
            </div>
          )}
          {/* <div className="survey-tab-main flex justify-center px-[100px]"> */}
          {isDesktop ? (
            <CQVerifiedButtons
              graphData={graphData}
              user={user}
              activeTab={activeTab}
              verified={verified}
              setResults={setResults}
              setActiveTab={setActiveTab}
              showModal={showModal}
              auth={auth}
            />
          ) : (
            <CQVerifiedButtonsMobile
              graphData={graphData}
              user={user}
              activeTab={activeTab}
              verified={verified}
              setResults={setResults}
              setActiveTab={setActiveTab}
              showModal={showModal}
              auth={auth}
            />
          )}
          <div>
            {!isDesktop ? (
              <div
                className="content-otr date-content mt-[20px]"
                style={{ zoom: 0.9 }}
              >
                <p className="heading-XS text-white text-center text-[14px] font-normal leading-[21px] mb-[2px]">
                  {/* Respondents = {graphData?.data.respondents} */}
                </p>
                <p className="heading-XS text-white text-center text-[14px] font-normal leading-[21px]">
                  {/* {graphData?.data.range} */}
                </p>
              </div>
            ) : (
              <></>
            )}
          </div>
          {/* </div> */}
          <div
            className={`${styles['cqo_next-icon-container']} next-icon-otr flex items-center justify-between pt-[20px] px-[40px] relative`}
            style={shareableLink ? { height: '80px' } : null}
          >
            <div>
              {copied && !hideShare ? (
                <div className="action-otr flex">
                  <a className="heading-S text-[#010202] rounded-50 bg-white hover:text-[#010202] pt-[8px] pl-[14px] pr-[14px] pb-[8px]">
                    Link copied to clipboard.
                  </a>
                </div>
              ) : (
                <></>
              )}
              {!copied && !shareableLink ? (
                <a
                  className="share-text-otr flex align-center gap-[12px]"
                  onClick={() => {
                    navigator?.clipboard?.writeText(
                      `${
                        process.env.NEXT_PUBLIC_APP_URL
                      }/questions/${surveyId}?activeQID=${
                        questions[active - 1].unique_QID
                      }&verified=${
                        user?.year_of_birth && user?.gender ? 'true' : 'false'
                      }`
                    );
                    setCopied(!copied);
                  }}
                >
                  <Image
                    className="object-contain"
                    width={18}
                    height={18}
                    src="/svg/share-icon.svg"
                    alt="logo"
                  />
                  <p className="heading-XS text-white">Get shareable link</p>
                </a>
              ) : (
                <></>
              )}
            </div>
            {isDesktop ? (
              <div
                className="content-otr date-content absolute left-[50%]"
                style={{ transform: 'translate(-50%, -50px)' }}
              >
                <p className="heading-XS text-white text-center text-[14px] font-normal leading-[21px] mb-[2px]">
                  {/* Respondents = {graphData?.data?.respondents} */}
                </p>
                <p className="heading-XS text-white text-center text-[14px] font-normal leading-[21px]">
                  {/* {graphData?.data?.range} */}
                </p>
              </div>
            ) : (
              <></>
            )}
            {!shareableLink ? (
              <div
                className={`${styles['cqo_next-icon']} next-icon-inr flex w-w-56 h-h-48 justify-center bg-boxbg rounded-8`}
                // TODO: Add time to survey data
                onClick={() => {
                  setActive(active + 1);
                  setShowGraph(false);
                  setGraphData(null);
                  processGraphTimeEnd();
                }}
              >
                <Image
                  className="object-contain"
                  width={32}
                  height={32}
                  src="/svg/next-arrow.svg"
                  alt="logo"
                />
              </div>
            ) : (
              <div className="absolute right-[25px] bottom-[15px] -mb-[15px]">
                Source:{' '}
                <u onClick={() => router.push('/')} className="cursor-pointer">
                  humandilemmas.com
                </u>
              </div>
            )}
          </div>
          <>
            <Modal
              title="Basic Modal"
              open={isModalVisible}
              onOk={handleOk}
              onCancel={handleCancel}
            >
              <p className="heading-h3 font-normal">
                If you want to see the answers given by verified users then make
                a profile{' '}
                <span
                  className="underline cursor-pointer"
                  onClick={() => {
                    localStorage.setItem('surveyId', surveyId);
                    localStorage.setItem('activeQuestion', active);
                    localStorage.setItem(
                      'currentShuffle',
                      JSON.stringify(questions)
                    );
                    router.push('/sign-up');
                  }}
                >
                  here
                </span>
                .
              </p>
            </Modal>
          </>
        </div>
      </div>
    </div>
  );
};
