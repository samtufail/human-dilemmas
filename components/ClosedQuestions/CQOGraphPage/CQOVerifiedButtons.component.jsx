import { useMediaQuery } from 'react-responsive';
import styles from '../CQOAnswerPage/CQOAnswerPage.module.scss';

export const CQVerifiedButtons = ({
  user,
  graphData,
  activeTab,
  verified,
  setResults,
  setActiveTab,
  showModal,
  auth,
}) => {
  const isNestHub = useMediaQuery({ query: '(min-width: 1024px)' });

  return (
    <div className="survey-tab-main flex justify-center px-[80px]">
      {(user?.gender && user?.year_of_birth) || verified ? (
        <>
          <ul className="flex items-center rounded-8">
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button w-w-110 border ${
                  activeTab === 'All' && 'active-survey-tab'
                } py-[6px] px-[8px] bg-tabbg border-x-white border-x-0 border-y-0 rounded-l-8`}
                onClick={() => {
                  setResults({
                    results: graphData?.data?.results?.[0]?.all,
                    answer: graphData?.answer,
                  });
                  setActiveTab('All');
                }}
              >
                All
              </button>
            </li>
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === 'Women' && 'active-survey-tab'
                } w-w-110 border py-[6px] px-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-0`}
                onClick={() => {
                  setResults({
                    results: graphData?.data?.results[1].women,
                    answer: graphData?.answer,
                  });
                  setActiveTab('Women');
                }}
              >
                Women
              </button>
            </li>
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === 'Men' && 'active-survey-tab'
                } w-w-110 border py-[6px] px-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-0`}
                onClick={() => {
                  setResults({
                    results: graphData?.data?.results[2].men,
                    answer: graphData?.answer,
                  });
                  setActiveTab('Men');
                }}
              >
                Men
              </button>
            </li>
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === 'Under 40' && 'active-survey-tab'
                } w-w-110 border py-[6px] px-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-0`}
                onClick={() => {
                  setResults({
                    results: graphData?.data?.results[3].under_forty,
                    answer: graphData?.answer,
                  });
                  setActiveTab('Under 40');
                }}
              >
                Under 40
              </button>
            </li>
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === '40 or above' && 'active-survey-tab'
                } w-w-110 border py-[6px] px-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-r-8`}
                onClick={() => {
                  setResults({
                    results: graphData?.data?.results[4].forty_or_above,
                    answer: graphData?.answer,
                  });
                  setActiveTab('40 or above');
                }}
              >
                40 or above
              </button>
            </li>
          </ul>
        </>
      ) : (
        <>
          {isNestHub ? (
            <ul className="flex items-center rounded-8" style={{ zoom: '0.8' }}>
              <li className="survey-tab-li">
                <button
                  className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                    activeTab === 'Unverified' && `active-survey-tab`
                  } w-w-110 border px-[6px] py-[8px] bg-tabbg border-x-white font-semibold border-x-0 border-y-0 rounded-l-8`}
                >
                  Unverified
                </button>
              </li>
              <li className="survey-tab-li">
                <button
                  className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                    activeTab === 'Verified' && `active-survey-tab`
                  } w-w-110 border px-[6px] py-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-r-8`}
                  onClick={() => {
                    if (!auth) {
                      showModal();
                    }
                  }}
                >
                  Verified
                </button>
              </li>
            </ul>
          ) : (
            <ul className="survey-tab-ul flex items-center rounded-8">
              <li className="survey-tab-li">
                <button
                  className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                    activeTab === 'Unverified' && `active-survey-tab`
                  } w-w-110 border px-[6px] py-[8px] bg-tabbg border-x-white font-semibold border-x-0 border-y-0 rounded-l-8`}
                >
                  Unverified
                </button>
              </li>
              <li className="survey-tab-li">
                <button
                  className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                    activeTab === 'Verified' && `active-survey-tab`
                  } w-w-110 border px-[6px] py-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-r-8`}
                  onClick={() => {
                    if (!auth) {
                      showModal();
                    }
                  }}
                >
                  Verified
                </button>
              </li>
            </ul>
          )}
          {/* <ul className="survey-tab-ul flex items-center rounded-8">
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === 'Unverified' && `active-survey-tab`
                } w-w-110 border px-[6px] py-[8px] bg-tabbg border-x-white font-semibold border-x-0 border-y-0 rounded-l-8`}
              >
                Unverified
              </button>
            </li>
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === 'Verified' && `active-survey-tab`
                } w-w-110 border px-[6px] py-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-r-8`}
                onClick={() => {
                  if (!auth) {
                    showModal();
                  }
                }}
              >
                Verified
              </button>
            </li>
          </ul> */}
        </>
      )}
    </div>
  );
};

export const CQVerifiedButtonsMobile = ({
  user,
  graphData,
  activeTab,
  verified,
  setResults,
  setActiveTab,
  showModal,
  auth,
}) => {
  const isGalaxyfold = useMediaQuery({ query: '(max-width: 280px)' });
  return (
    <div
      className={`survey-tab-main flex justify-center ${styles['cqo_progress-bar']} ${styles['cqo_verified-button']}`}
      style={{ zoom: isGalaxyfold ? 0.74 : 0.85 }}
    >
      {(user?.gender && user?.year_of_birth) || verified ? (
        <div
          className="flex items-center w-full flex-col"
          style={{ gap: isGalaxyfold ? '24px' : '8px' }}
        >
          {/* FirstRow */}
          <ul className="flex rounded-8">
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button w-w-110 border ${
                  activeTab === 'All' && 'active-survey-tab'
                } py-[6px] px-[8px] bg-tabbg border-x-white border-x-0 border-y-0 rounded-l-8`}
                onClick={() => {
                  setResults({
                    results: graphData?.data?.results?.[0]?.all,
                    answer: graphData?.answer,
                  });
                  setActiveTab('All');
                }}
              >
                All
              </button>
            </li>
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === 'Women' && 'active-survey-tab'
                } w-w-110 border py-[6px] px-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-0`}
                onClick={() => {
                  setResults({
                    results: graphData?.data?.results[1].women,
                    answer: graphData?.answer,
                  });
                  setActiveTab('Women');
                }}
              >
                Women
              </button>
            </li>
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === 'Men' && 'active-survey-tab'
                } w-w-110 border py-[6px] px-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-r-8`}
                onClick={() => {
                  setResults({
                    results: graphData?.data?.results[2].men,
                    answer: graphData?.answer,
                  });
                  setActiveTab('Men');
                }}
              >
                Men
              </button>
            </li>
          </ul>
          {/* SecondRow */}
          <ul className="flex justify-center rounded-8">
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === 'Under 40' && 'active-survey-tab'
                } w-w-110 border py-[6px] px-[8px] bg-tabbg border-r-0 border-y-0 rounded-l-8`}
                onClick={() => {
                  setResults({
                    results: graphData?.data?.results[3].under_forty,
                    answer: graphData?.answer,
                  });
                  setActiveTab('Under 40');
                }}
              >
                Under 40
              </button>
            </li>
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === '40 or above' && 'active-survey-tab'
                } w-w-110 border py-[6px] px-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-r-8`}
                onClick={() => {
                  setResults({
                    results: graphData?.data?.results[4].forty_or_above,
                    answer: graphData?.answer,
                  });
                  setActiveTab('40 or above');
                }}
              >
                40 or above
              </button>
            </li>
          </ul>
        </div>
      ) : (
        <>
          <ul className="flex items-center rounded-8">
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === 'Unverified' && `active-survey-tab`
                } w-w-110 border px-[6px] py-[8px] bg-tabbg border-x-white font-semibold border-x-0 border-y-0 rounded-l-8`}
              >
                Unverified
              </button>
            </li>
            <li className="survey-tab-li">
              <button
                className={`heading-XS outline-transparent border-transparent survey-tab-button ${
                  activeTab === 'Verified' && `active-survey-tab`
                } w-w-110 border px-[6px] py-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-r-8`}
                onClick={() => {
                  if (!auth) {
                    showModal();
                  }
                }}
              >
                Verified
              </button>
            </li>
          </ul>
        </>
      )}
    </div>
  );
};
