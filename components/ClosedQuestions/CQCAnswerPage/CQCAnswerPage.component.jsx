import Image from 'next/image';
import { api } from 'utils/api';
import { ProgressBar } from '../../ProgressBar/ProgressBar.component';
import { useRouter } from 'next/router';
import styles from '../CQOAnswerPage/CQOAnswerPage.module.scss';
import { useMediaQuery } from 'react-responsive';

export const CQCAnswerPage = ({
  questions,
  active,
  setGraphData,
  setShowGraph,
  surveyId,
  percent,
}) => {
  const router = useRouter();

  const moveNext = async () => {
    const data = await api.put(`/v1/surveys/${surveyId}/update/`, {
      qid: questions[active - 1].unique_QID,
    });
    setGraphData({
      data: data?.data,
      answer: null,
    });
    setShowGraph(true);
  };
  const isNestHub = useMediaQuery({ query: '(max-width: 1024px)' });

  return (
    <div className={styles['cqo_main-container']}>
      <div className={`${styles['cqo_container']} max-w-950 mx-auto my-auto`}>
        <div
          className={`${styles['cqo_scroll-container']} ${
            isNestHub ? 'mb-[10px]' : 'mb-[40px]'
          } scrollbar-style survey-open-main bg-black-survey2 rounded-8 pb-[30px] pt-[1px]`}
        >
          <div
            className={`${styles['cqo_progress-bar']} progressBar-otr px-[100px] pt-[45px] sm:pt-[55px]`}
          >
            <ProgressBar percent={percent} />
          </div>
          <p
            className={`${styles['cqo_question']} heading-M font-semibold text-white mt-[20px] sm:mt-[30px] mb-[25px] px-[100px]`}
          >
            {questions[active - 1].question}
          </p>
          <div
            className={`${styles['cqo_radio-box']} flex flex-col align-start gap-[16px] sm:gap-[24px] px-[100px] mb-[120px]`}
          >
            {questions[active - 1].choices.map((val, ind) => (
              <div
                key={ind}
                className="flex align-center gap-[16px] sm:gap-[24px]"
              >
                <Image
                  className="object-contain"
                  width={26}
                  height={26}
                  src="/svg/lock-img.svg"
                  alt="logo"
                />
                <p className="heading-M text-white">{val}</p>
              </div>
            ))}
          </div>
          <div
            className={`${styles['cqo_next-icon-container']} next-icon-otr flex items-center justify-between pt-[10px] px-[40px]`}
          >
            <p className="share-text-otr"></p>
            <div className="content-otr"></div>
            <div
              className={`${styles['cqo_next-icon']} next-icon-inr flex w-w-56 h-h-48 justify-center bg-boxbg rounded-8 cursor-pointer items-center`}
              onClick={moveNext}
            >
              <img
                className="object-contain"
                width={32}
                height={32}
                src="/svg/next-arrow.svg"
                alt="logo"
              />
            </div>
          </div>
        </div>
        <div className="flex items-center justify-between mt-[16px]">
          <div
            className="exit-text-otr cursor-pointer underline heading-M2 text-textGray2 text-[12px]"
            onClick={() => router.push('/questions')}
          >
            Exit
          </div>
        </div>
      </div>
    </div>
  );
};

// http://localhost:3000/questions/G-vsraZx9ns1S
