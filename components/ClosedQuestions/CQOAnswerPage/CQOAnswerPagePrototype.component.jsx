import { useState } from 'react';
import { ProgressBar } from '../../ProgressBar/ProgressBar.component';
import { ClosedQuestionsRadioButton } from '../ClosedQuestionsRadioButton/ClosedQuestionsRadioButton.component';
import { Radio } from 'antd';
import { useRouter } from 'next/router';

import styles from './CQOAnswerPage.module.scss';

// Custom Components and Utils
import { QuestionFeedback } from 'components';

export const CQOAnswerPage = ({ questions }) => {
  const [answer, setAnswer] = useState(''); // Saves the answer from the radio button group
  const answered = null;

  const [index, setIndex] = useState(0); // Saves the current question index
  const percent = Math.round(((index + 1) / questions?.length) * 100);

  const [question, setQuestion] = useState(questions[index]);
  const [showModal, setShowModal] = useState(false);

  const [feedbackPoints, setFeedPoints] = useState(0); // Saves the feedback points from Slider
  const [fixedKeywordsAsFeedback, setFixedKeywordsAsFeedback] = useState([]); // Saves the multi keyword as feedback
  const [customFeedback, setCustomFeedback] = useState(''); // Saves the custom feedback using the input field

  const [gotIninitalFeedback, setGotIninitalFeedback] = useState(false); // Decides to show the second feedback in combination with feedbackPoints
  const [secondFeedback, setSecondFeedback] = useState(false); // Decider to get fixedKeywordsAsFeedback or customFeedback

  const [clickedNextCount, setClickedNextCount] = useState(0);

  const router = useRouter();

  const handleCloseModal = () => {
    // Closes the modal and resets the state
    setShowModal(false);
    setAnswer('');
    setGotIninitalFeedback(false);
    setFeedPoints(0);
    setFixedKeywordsAsFeedback([]);
    setCustomFeedback('');
    setSecondFeedback(false);
    setClickedNextCount(0);
  };

  const handleNextQuestion = () => {
    let totalQuestions = questions.length;
    if (index < totalQuestions - 1) {
      // Wait for 120ms before changing the question because popup is taking almost 120ms to close due to animation
      setTimeout(() => {
        // Closes the modal and resets the state and moves to the next question
        setShowModal(false);
        setFeedPoints(0);
        setIndex(index + 1);
        setQuestion(questions[index + 1]);
        setAnswer('');
        setGotIninitalFeedback(false);
        setSecondFeedback(false);
        setFixedKeywordsAsFeedback([]);
        setCustomFeedback('');
        setClickedNextCount(0);
      }, 120);
    } else {
      // Survey Outro if Active is more than current questions count
      router.push('/prototypes/panelists-outro');
    }
  };

  const handleFeedbackEvaluation = () => {
    setClickedNextCount(clickedNextCount + 1);

    if (gotIninitalFeedback) {
      setSecondFeedback(true);
    }

    if (feedbackPoints === 0) {
      setSecondFeedback(true);
    }

    if (clickedNextCount === 1) {
      handleNextQuestion();
    }
  };

  return (
    <div className={styles['cqo_main-container']}>
      <div className={`${styles['cqo_container']} max-w-950 mx-auto my-auto`}>
        <div
          className={`${styles['cqo_scroll-container']} scrollbar-style survey-open-main bg-black-survey rounded-8 pb-[30px] pt-[1px]`}
        >
          <div id="desktop-only">
            {/* Progress Bar */}
            <div
              className={`${styles['cqo_progress-bar']} progressBar-otr px-[100px] pt-[45px]`}
            >
              <ProgressBar percent={percent} />
            </div>

            {/* Question Text */}
            <p
              className={`${styles['cqo_question']} heading-M font-semibold text-white mt-[20px] mb-[25px] px-[100px]`}
            >
              {question?.question}
            </p>
            <div className={`${styles['cqo_radio-box']} px-[100px] mb-[80px]`}>
              <Radio.Group
                name="radiogroup1"
                className={`flex flex-col align-start gap-[16px] ${
                  answered?.date ? 'pointer-events-none' : ''
                }`}
                value={answer}
                onChange={(e) => {
                  setAnswer(e?.target?.value);
                  setShowModal(true);
                }}
              >
                {question?.choices.map((val, ind) => (
                  <ClosedQuestionsRadioButton
                    key={ind}
                    title={val}
                    disabled={false}
                    selected={answer === val}
                  />
                ))}
              </Radio.Group>
            </div>
          </div>
          {/* Suitable message for mobile or tablet screens */}
          <p className="suitable-message heading-M font-semibold text-white mt-24 mb-25 px-100  text-center">
            This content is only available on desktop screens.
          </p>

          <QuestionFeedback
            question={question?.question}
            visible={showModal}
            setVisible={handleCloseModal}
            evaluateFeedback={handleFeedbackEvaluation}
            feedbackPoints={feedbackPoints}
            setFeedPoints={setFeedPoints}
            gotIninitalFeedback={gotIninitalFeedback}
            setGotIninitalFeedback={setGotIninitalFeedback}
            secondFeedback={secondFeedback}
            setCustomFeedback={setCustomFeedback}
            setFixedKeywordsAsFeedback={setFixedKeywordsAsFeedback}
            fixedKeywordsAsFeedback={fixedKeywordsAsFeedback}
          />
        </div>

        <div className="flex items-center justify-between mt-[16px]">
          <div
            className="exit-text-otr cursor-pointer underline heading-M2 text-textGray2 text-[12px]"
            onClick={() => router.push('/questions')}
          >
            Exit
          </div>
        </div>
      </div>
    </div>
  );
};
