import { useState } from 'react';
import { ProgressBar } from '../../ProgressBar/ProgressBar.component';
import { ClosedQuestionsRadioButton } from '../ClosedQuestionsRadioButton/ClosedQuestionsRadioButton.component';
import { Radio } from 'antd';
import { useRouter } from 'next/router';

import styles from './CQOAnswerPage.module.scss';

// Custom Components and Utils
import { QuestionFeedbackTwo } from 'components';

export const CQOAnswerPage = ({ questions, questionsToReview }) => {
  const [answer, setAnswer] = useState(''); // Saves the answer from the radio button group
  const answered = null;
  const [index, setIndex] = useState(0);
  const percent = Math.round(((index + 1) / questions?.length) * 100);
  const [question, setQuestion] = useState(questions[index]);

  const [showModal, setShowModal] = useState(false);

  const router = useRouter();

  const handleNextQuestionForAnswer = () => {
    let totalQuestions = questions.length;
    if (index < totalQuestions - 1) {
      // Wait for 140ms before changing the question to give a realistic transition
      setTimeout(() => {
        setIndex(index + 1);
        setQuestion(questions[index + 1]);
        setAnswer('');
      }, 140);
    }
    if (index === totalQuestions - 1) {
      setShowModal(true);
    }
  };

  return (
    <div className={styles['cqo_main-container']}>
      <div className={`${styles['cqo_container']} max-w-950 mx-auto my-auto`}>
        <div
          className={`${styles['cqo_scroll-container']} scrollbar-style survey-open-main bg-black-survey rounded-8 pb-[30px] pt-[1px]`}
        >
          {/* Progress Bar */}
          <div
            className={`${styles['cqo_progress-bar']} progressBar-otr px-[100px] pt-[45px]`}
          >
            <ProgressBar percent={percent} />
          </div>
          {/* Question Text */}
          <p
            className={`${styles['cqo_question']} heading-M font-semibold text-white mt-[20px] mb-[25px] px-[100px]`}
          >
            {question?.question}
          </p>
          <div className={`${styles['cqo_radio-box']} px-[100px] mb-[80px]`}>
            <Radio.Group
              name="radiogroup1"
              className={`flex flex-col align-start gap-[16px] ${
                answered?.date ? 'pointer-events-none' : ''
              }`}
              value={answer}
              onChange={(e) => {
                setAnswer(e?.target?.value);
                handleNextQuestionForAnswer();
              }}
            >
              {question?.choices.map((val, ind) => (
                <ClosedQuestionsRadioButton
                  key={ind}
                  title={val}
                  disabled={false}
                  selected={answer === val}
                />
              ))}
            </Radio.Group>
          </div>
          <QuestionFeedbackTwo
            questionsToReview={questionsToReview}
            visible={showModal}
            setVisible={setShowModal}
          />
        </div>

        <div className="flex items-center justify-between mt-[16px]">
          <div
            className="exit-text-otr cursor-pointer underline heading-M2 text-textGray2 text-[12px]"
            onClick={() => router.push('/questions')}
          >
            Exit
          </div>
        </div>
      </div>
    </div>
  );
};
