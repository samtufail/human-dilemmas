import { useState, useEffect } from 'react';
import { ProgressBar } from '../../ProgressBar/ProgressBar.component';
import { ClosedQuestionsRadioButton } from '../ClosedQuestionsRadioButton/ClosedQuestionsRadioButton.component';
import { Timer } from '../Timer/Timer.component';
import { Radio } from 'antd';
import { useRouter } from 'next/router';
import { useAnswerTime } from 'hooks';
import { answerTimeStart, answerTimeEnd } from 'utils';
import { api } from 'utils/api';
import styles from './CQOAnswerPage.module.scss';
import { useMediaQuery } from 'react-responsive';

export const CQOAnswerPage = ({
  questions,
  active,
  setGraphData,
  setShowGraph,
  surveyId,
  percent,
  userData,
}) => {
  const [answer, setAnswer] = useState('');
  const [answered, setAnswered] = useState(null);
  const [, sendAnswerTimeEndRequest] = useAnswerTime(
    questions[active - 1]?.unique_QID,
    surveyId,
    answerTimeStart,
    answerTimeEnd
  );
  const router = useRouter();

  async function processAnswerTimeEnd() {
    sendAnswerTimeEndRequest();
  }

  const saveAnswer = async () => {
    const ans = localStorage.getItem('answerValue');
    const data = await api.put(`/v1/surveys/${surveyId}/update/`, {
      qid: questions[active - 1].unique_QID,
      answer: ans && answered === null ? ans : null,
    });

    // Sending answerTimeEnd request to backend
    processAnswerTimeEnd();

    setGraphData({
      data: data?.data,
      answer:
        ans && answered === null
          ? ans
          : answered !== null
          ? answered.answer
          : null,
    });
    setShowGraph(true);
    localStorage.removeItem('answerValue');
    setAnswer('');
  };

  useEffect(() => {
    const qid = questions[active - 1]?.unique_QID;
    const previousAnswer = userData?.length
      ? userData?.find((data) => data?.qid === qid)
      : null;
    if (previousAnswer) {
      setAnswered(previousAnswer);
      // console.log()
      setAnswer(previousAnswer?.answer);
    } else {
      setAnswered(null);
    }
  }, [active, userData, questions]);

  const isNestHub = useMediaQuery({ query: '(max-width: 1024px)' });

  return (
    <div className={styles['cqo_main-container']}>
      <div className={`${styles['cqo_container']} max-w-950 mx-auto my-auto`}>
        <div
          className={`${styles['cqo_scroll-container']} ${
            isNestHub ? 'mb-[10px]' : 'mb-[40px]'
          } scrollbar-style survey-open-main bg-black-survey rounded-8 pb-[30px] pt-[1px]`}
          style={{ zoom: isNestHub ? 0.9 : null }}
        >
          {/* Progress Bar */}
          <div
            className={`${styles['cqo_progress-bar']} progressBar-otr px-[100px] pt-[45px] sm:pt-[55px]`}
          >
            <ProgressBar percent={percent} />
          </div>
          {/* Lock Message if Already Answered */}
          {answered?.date ? (
            <div
              className={`${styles['cqo_already-answered']} h-[50px] mt-[20px] mx-[100px] bg-[#1A232E] px-[26px] flex items-center gap-[20px] rounded-[4px]`}
            >
              {/* Lock Icon */}
              <svg
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM8 15L3 10L4.41 8.59L8 12.17L15.59 4.58L17 6L8 15Z"
                  fill="white"
                  fillOpacity="0.4"
                />
              </svg>
              {/* Message */}
              <p className="text-[#fff] text-[15px] heading-M">
                You&apos;ve already answered the question on{' '}
                <b>{answered?.date}</b>
              </p>
            </div>
          ) : null}
          {/* Question Text */}
          <p
            className={`${styles['cqo_question']} heading-M font-semibold text-white mt-[20px] sm:mt-[30px] mb-[25px] px-[100px]`}
          >
            {questions[active - 1].question}
          </p>
          <div className={`${styles['cqo_radio-box']} px-[100px] mb-[80px]`}>
            <Radio.Group
              name="radiogroup1"
              className={`flex flex-col align-start gap-[16px] sm:gap-[24px] ${
                answered?.date ? 'pointer-events-none' : ''
              }`}
              value={answer}
              onChange={(e) => {
                setAnswer(e?.target?.value);
                localStorage.setItem('answerValue', e?.target?.value);
              }}
            >
              {questions[active - 1].choices.map((val, ind) => (
                <ClosedQuestionsRadioButton
                  key={ind}
                  title={val}
                  disabled={answered?.date ? true : false}
                />
              ))}
            </Radio.Group>
          </div>
          {/* Next Button + Timer */}
          <div
            className={`${styles['cqo_next-icon-container']} next-icon-otr flex items-center justify-between pt-[20px] px-[40px]`}
          >
            <p className="share-text-otr"></p>
            <div className="content-otr"></div>
            {answered?.date ? (
              <div
                className={`${styles['cqo_next-icon']} next-icon-inr flex w-w-56 h-h-48 justify-center bg-boxbg rounded-8 cursor-pointer items-center`}
                onClick={saveAnswer}
              >
                <img
                  className="object-contain"
                  width={32}
                  height={32}
                  src="/svg/next-arrow.svg"
                  alt="logo"
                />
              </div>
            ) : questions[active - 1].timer_length === '0:00:00' ? (
              <>
                {answer ? (
                  <div
                    className={`${styles['cqo_next-icon']} next-icon-inr flex w-w-56 h-h-48 justify-center bg-boxbg rounded-8 cursor-pointer items-center`}
                    onClick={saveAnswer}
                  >
                    <img
                      className="object-contain"
                      width={32}
                      height={32}
                      src="/svg/next-arrow.svg"
                      alt="logo"
                    />
                  </div>
                ) : (
                  <></>
                )}
              </>
            ) : (
              <div className="time-otr flex align-center w-[86px] h-[86px] justify-center">
                <Timer
                  {...{ setShowGraph, saveAnswer, active }}
                  length={questions[active - 1].timer_length}
                />
              </div>
            )}
          </div>
        </div>
        <div className="flex items-center justify-between mt-[16px]">
          <div
            className="exit-text-otr cursor-pointer underline heading-M2 text-textGray2 text-[12px]"
            onClick={() => router.push('/questions')}
          >
            Exit
          </div>
        </div>
      </div>
    </div>
  );
};
