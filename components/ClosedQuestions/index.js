import dynamic from 'next/dynamic';

// CQO: Closed Questions (Open)
export const CQOAnswerPage = dynamic(
  () =>
    import('./CQOAnswerPage/CQOAnswerPage.component').then(
      (module) => module.CQOAnswerPage
    ),
  { ssr: false }
);

export const CQOAnswerPagePrototype = dynamic(
  () =>
    import('./CQOAnswerPage/CQOAnswerPagePrototype.component').then(
      (module) => module.CQOAnswerPage
    ),
  { ssr: false }
);

export const CQOAnswerPagePrototypeTwo = dynamic(
  () =>
    import('./CQOAnswerPage/CQOAnswerPagePrototypeTwo.component').then(
      (module) => module.CQOAnswerPage
    ),
  { ssr: false }
);

export const CQOGraphPage = dynamic(
  () =>
    import('./CQOGraphPage/CQOGraphPage.component').then(
      (module) => module.CQOGraphPage
    ),
  { ssr: false }
);

// CQC: Closed Questions (Closed)
export const CQCAnswerPage = dynamic(
  () =>
    import('./CQCAnswerPage/CQCAnswerPage.component').then(
      (module) => module.CQCAnswerPage
    ),
  { ssr: false }
);
export const CQCGraphPage = dynamic(
  () =>
    import('./CQCGraphPage/CQCGraphPage.component').then(
      (module) => module.CQCGraphPage
    ),
  { ssr: false }
);

// Closed Questions Bar Chart
export const ClosedQuestionsBarChart = dynamic(
  () =>
    import('./ClosedQuestionsBarChart/ClosedQuestionsBarChart.component').then(
      (module) => module.ClosedQuestionsBarChart
    ),
  { ssr: false }
);
