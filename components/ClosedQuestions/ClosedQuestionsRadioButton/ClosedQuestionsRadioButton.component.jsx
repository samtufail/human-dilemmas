import { Radio } from 'antd';
import React from 'react';
import styles from './ClosedQuestionsRadioButton.module.scss';

export const ClosedQuestionsRadioButton = (props) => {
  return (
    <div
      className={`RadioBox2-main ${
        props?.disabled ? 'disabled-radio-button' : ''
      } ${styles['radio-wrapper']}`}
    >
      <Radio value={props.title}>
        <span className={styles['radio-text']}>{props.title}</span>
      </Radio>
    </div>
  );
};
