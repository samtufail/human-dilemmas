import { React, useState, useEffect } from 'react';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';

const renderTime = ({ remainingTime }) => {
  let minutes = Math.floor(remainingTime / 60);
  let minute1 = Math.floor(minutes / 10);
  let minute2 = minutes % 10;
  let seconds = remainingTime % 60;
  let second1 = Math.floor(seconds / 10);
  let second2 = seconds % 10;
  if (remainingTime === 0) {
    return <div className="timer">Time Up</div>;
  }
  return (
    <div className="timer">
      <div className="value heading-M2">
        {minute1}
        {minute2}:{second1}
        {second2}
      </div>
    </div>
  );
};

export const Timer = ({ setShowGraph, saveAnswer, active, answer, length }) => {
  function hmsToSecondsOnly(str) {
    var p = str.split(':'),
      s = 0,
      m = 1;

    while (p.length > 0) {
      s += m * parseInt(p.pop(), 10);
      m *= 60;
    }
    return s;
  }
  const totalTime = hmsToSecondsOnly(length);
  function startPlay() {
    setStatus((prevStatus) => {
      return !prevStatus;
    });
  }
  useEffect(() => {
    const timer = setTimeout(async () => {
      await saveAnswer();
    }, (totalTime + 1) * 1000);
    return () => clearTimeout(timer);
  }, [active]);

  return (
    <div className="App">
      <div className="timer-wrapper cursor-pointer">
        <CountdownCircleTimer
          key={active}
          isPlaying
          size={90}
          strokeWidth={2}
          colors={'#845EC2'}
          duration={totalTime}
          initialRemainingTime={totalTime}
        >
          {renderTime}
        </CountdownCircleTimer>
      </div>
    </div>
  );
};
