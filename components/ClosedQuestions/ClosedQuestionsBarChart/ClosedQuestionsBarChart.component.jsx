import React, { useState, useEffect } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Legend, Cell, Text } from 'recharts';

const CustomizedAxisTick = (props) => {
  const { x, y, payload } = props;

  return (
    <Text x={x} y={y} width={120} lineHeight="20px" fill="#fff" textAnchor="middle" verticalAnchor="start">
      {payload.value}
    </Text>
  );
};

export const ClosedQuestionsBarChart = ({ shareableLink, results, auth, verified }) => {
  const [barData, setBarData] = useState(null);
  useEffect(() => {
    if (!results) return;
    const keysArr = Object.keys(auth || verified ? results?.results : results?.data?.results);
    const valuesArr = Object.values(auth || verified ? results?.results : results?.data?.results);
    const updatedData = keysArr.map((val, ind) => ({
      name: val,
      pv: valuesArr[ind],
    }));
    setBarData(updatedData);
  }, [results, auth, verified]);
  if (!barData) return null;
  return (
    <>
      <BarChart
        width={520}
        height={320}
        data={barData}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid overlineThickness="3" horizontal={true} vertical={false} />
        <XAxis dataKey="name" tick={<CustomizedAxisTick />} interval={0} height={40} />
        <YAxis
          type="number"
          tick={{ fill: 'white' }}
          tickFormatter={(tick) => {
            return `${tick}%`;
          }}
        />
        <Legend />
        <Bar dataKey="pv" barSize={50} fill="#C4C4C4" radius={[4, 4, 0, 0]}>
          {barData.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={entry?.name === results?.answer && !shareableLink ? '#804771' : '#C4C4C4'} />
          ))}
        </Bar>
      </BarChart>
    </>
  );
};
