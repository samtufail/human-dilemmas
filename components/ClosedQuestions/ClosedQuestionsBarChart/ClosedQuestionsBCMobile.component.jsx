import React, { useState, useEffect } from 'react';
import { useMediaQuery } from 'react-responsive';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Legend,
  Cell,
  Text,
} from 'recharts';

const CustomizedAxisTickGalaxyfold = (props) => {
  const isGalaxy = useMediaQuery({ query: '(max-width: 380px)' });
  const isMobile = useMediaQuery({ query: '(max-width: 424px)' });

  const { x, y, payload } = props;

  return (
    <Text
      x={x}
      y={y}
      width={75}
      lineHeight={isMobile ? '15px' : '20px'}
      fill="#fff"
      textAnchor="middle"
      verticalAnchor="start"
      fontSize={10}
    >
      {payload.value}
    </Text>
  );
};

const CustomizedAxisTick = (props) => {
  const isGalaxyfold = useMediaQuery({ query: '(max-width: 280px)' });
  const isMobile = useMediaQuery({ query: '(max-width: 424px)' });

  const { x, y, payload } = props;

  return (
    <Text
      x={x}
      y={y}
      width={85}
      lineHeight={isMobile ? '15px' : '20px'}
      fill="#fff"
      textAnchor="middle"
      verticalAnchor="start"
      fontSize={isGalaxyfold ? '10px' : '12px'}
    >
      {payload.value}
    </Text>
  );
};

const ChartInstance = ({ barData, shareableLink, width, results }) => {
  const isMobile = useMediaQuery({ query: '(max-width: 424px)' });
  const isGalaxy = useMediaQuery({ query: '(max-width: 360px)' });
  const isSurfaceDuo = useMediaQuery({ query: '(width: 540px)' });

  return (
    <BarChart
      width={width}
      height={280}
      data={barData}
      barGap={1}
      barCategoryGap="1%"
      margin={{
        top: 40,
        right: 30,
        //   left: 20,
        bottom: 0,
      }}
    >
      <CartesianGrid overlineThickness="3" horizontal={true} vertical={false} />
      <XAxis
        dataKey="name"
        tick={
          isGalaxy ? <CustomizedAxisTickGalaxyfold /> : <CustomizedAxisTick />
        }
        interval={0}
        height={90}
      />
      <YAxis
        type="number"
        tick={{ fill: 'white' }}
        tickFormatter={(tick) => {
          return `${tick}%`;
        }}
      />
      <Legend />
      <Bar dataKey="pv" barSize={30} fill="#C4C4C4" radius={[4, 4, 0, 0]}>
        {barData.map((entry, index) => (
          <Cell
            key={`cell-${index}`}
            fill={
              entry?.name === results?.answer && !shareableLink
                ? '#804771'
                : '#C4C4C4'
            }
          />
        ))}
      </Bar>
    </BarChart>
  );
};

export const ClosedQuestionsBCMobile = ({
  shareableLink,
  results,
  auth,
  verified,
}) => {
  const [barData, setBarData] = useState(null);
  useEffect(() => {
    if (!results) return;
    const keysArr = Object.keys(
      auth || verified ? results?.results : results?.data?.results
    );
    const valuesArr = Object.values(
      auth || verified ? results?.results : results?.data?.results
    );
    const updatedData = keysArr.map((val, ind) => ({
      name: val,
      pv: valuesArr[ind],
    }));
    setBarData(updatedData);
  }, [results, auth, verified]);

  //   Media Queries
  //   const isDesktop = useMediaQuery({ query: '(min-width: 769px)' });
  const isGalaxyfold = useMediaQuery({ query: '(max-width: 280px)' });
  const isGalaxy8plus = useMediaQuery({ query: '(max-width: 360px)' });

  if (!barData) return null;
  if (isGalaxyfold) {
    return (
      <ChartInstance
        barData={barData}
        shareableLink={shareableLink}
        width={260}
        results={results}
      />
    );
  } else if (isGalaxy8plus) {
    return (
      <ChartInstance
        barData={barData}
        shareableLink={shareableLink}
        width={300}
        results={results}
      />
    );
  } else {
    return (
      <ChartInstance
        barData={barData}
        shareableLink={shareableLink}
        width={325}
        results={results}
      />
    );
  }
};
