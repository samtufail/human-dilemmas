import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';

const antIcon = (
  <LoadingOutlined style={{ fontSize: 36, color: '#A46CFF' }} spin />
);

export const Loader = () => {
  return <Spin indicator={antIcon} />;
};
