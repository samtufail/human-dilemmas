import Image from 'next/image';
import { useRouter } from 'next/router';

export const SurveyIntro = ({
  setActive,
  active,
  author,
  intro,
  type,
  image,
}) => {
  const router = useRouter();

  return (
    <div
      className={`survey_intro max-w-750 mx-[20px] ${
        type ? 'bg-black-survey2' : 'bg-black-survey'
      } rounded-8 pt-[25px] px-[10px] sm:px-[20px] pb-[20px]`}
    >
      <div
        className="close-icon-otr mr-[10px] flex justify-end"
        onClick={() => router.push('/questions')}
      >
        <Image
          className="object-contain cursor-pointer"
          src="/svg/close-icon.svg"
          width={28}
          height={28}
          alt="icon"
        />
      </div>
      <div className="img-contet-otr flex flex-col justify-center items-center sm:flex-row sm:items-center mt-[24px] mb-[24px] sm:gap-3">
        <div className="img-otr w-[100px] h-[100px] sm:w-[200px] sm:h-[210px] sm:mr-[20px]">
          <img className="object-contain h-full w-full" src={image} alt="img" />
        </div>
        <div className="content-otr mt-[10px] sm:mt-[20px] text-center sm:text-start">
          <h3 className="text-[16px] sm:text-[18px] lg:mr-[0] sm:heading-h3 font-normal text-white">
            {intro}
          </h3>
          <div className="action-otr flex mt-[20px] mb-[20px] w-full justify-center sm:justify-start">
            <p
              className="cursor-pointer heading-S text-white rounded-50 bg-black-cta2 hover:bg-[#263547] hover:text-white pt-[6px] pl-[25px] pr-[25px] pb-[6px] sm:text-[16px]"
              onClick={() => setActive(active + 1)}
            >
              {type ? 'Closed' : 'Open'}
            </p>
          </div>
          <p className="heading-S text-white sm:text-[16px]">By {author}</p>
        </div>
      </div>
      <div className="video-icon-otr flex justify-end">
        <div
          className="mr-[10px] sm:mr-[15px] sm:mb-[15px] p-[10px] rounded-[12px] sm:rounded-[16px] h-[36px] w-[42px] sm:h-h-48  sm:w-[56px] flex justify-center bg-boxbg cursor-pointer items-center"
          onClick={() => setActive(active + 1)}
        >
            <img
            className="object-contain"
            width={32}
            height={32}
            src="/svg/next-arrow.svg"
            alt="logo"
          />
        </div>
      </div>
      {/* <Image className="object-contain cursor-pointer	" src="/svg/video-icon.svg" width="95px" height="50px" alt="icon" /> */}
    </div>
  );
};
