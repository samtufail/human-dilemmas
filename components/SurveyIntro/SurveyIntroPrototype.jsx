import Image from 'next/image';
import { useRouter } from 'next/router';

export const SurveyIntroPrototype = ({
  setActive,
  active,
  questions_count,
  type,
}) => {
  const router = useRouter();

  return (
    <div
      className={`survey_intro max-w-750 mx-[20px] ${
        type ? 'bg-black-survey2' : 'bg-black-survey'
      } rounded-8 pt-[25px] px-[10px] sm:px-[20px] pb-[20px]`}
    >
      <div id="desktop-only">
        <div
          className="close-icon-otr mr-[10px] flex justify-end"
          onClick={() => router.back()}
        >
          <Image
            className="object-contain cursor-pointer"
            src="/svg/close-icon.svg"
            width={32}
            height={32}
            alt="icon"
          />
        </div>
        <div className="img-content-otr flex flex-col sm:flex-row items-center mt-[24px] mb-[24px] sm:gap-3">
          <div className="img-otr w-[10px] h-[50px] sm:w-[100px] sm:h-[110px] sm:mr-[0px]">
            {/* <img className="object-contain h-full w-full" src={image} alt="img" /> */}
          </div>
          <div className="content-otr mt-[10px] sm:mt-0 text-center sm:text-start">
            <h3 className="text-[20px] sm:heading-h3 font-normal text-white mt-4 mb-4">
              {`In the following you see ${questions_count} question and you're asked to rate them
            on a scale from 0 to 10. Where 10 is most interesting and 0 is least
            interesting
            `}
            </h3>
          </div>
        </div>
        <div className="video-icon-otr flex justify-end">
          <div
            className="mr-[10px] sm:mr-[15px] sm:mb-[15px] p-[10px] rounded-[12px] sm:rounded-[16px] h-[36px] w-[42px] sm:h-h-48  sm:w-[56px] flex justify-center bg-boxbg cursor-pointer items-center"
            onClick={() => setActive(active + 1)}
          >
           <Image
            className="object-contain"
            width={32}
            height={32}
            src="/svg/next-arrow.svg"
            alt="logo"
          />
          </div>
        </div>
      </div>
      <p className="suitable-message heading-M font-semibold text-white mt-20 mb-25 px-100 pb-24 text-center">
        This content is only available on desktop screens.
      </p>
    </div>
  );
};
