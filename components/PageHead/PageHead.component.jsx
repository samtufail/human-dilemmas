import Head from 'next/head';

const PageHead = ({ title, description }) => {
  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
      <meta name="google" content="notranslate" />
    </Head>
  );
};

export default PageHead;
