import Image from 'next/image';
import { useOnClickOutside } from 'hooks/useOutsideClick.hook';
import { useRouter } from 'next/router';
import { logout } from 'store';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useRef, useState } from 'react';

const loggedOutLinks = [
  { title: 'Questions', link: '/questions' },
  { title: 'Create Profile', link: '/sign-up' },
  { title: 'About humans', link: '/about' },
  { title: 'Contact humans', link: '/contact-us' },
  { title: 'Log in', link: '/sign-in' },
];

const loggedInLinks = [
  { title: 'Questions', link: '/questions' },
  { title: 'My Profile', link: '/my-profile' },
  { title: 'About humans', link: '/about' },
  { title: 'Contact humans', link: '/contact-us' },
  { title: 'Log out', link: '/logout' },
];

export const Navbar = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [links, setLinks] = useState([]);

  const { auth } = useSelector((state) => state?.auth);

  useEffect(() => {
    if (auth) {
      setLinks(loggedInLinks);
    } else {
      setLinks(loggedOutLinks);
    }
  }, [auth]);

  const ref = useRef();

  useOnClickOutside(ref, () => setOpen(false));

  return (
    <div
      className="w-full flex items-center justify-between p-[25px] md:xxs:pr-[45px]"
      ref={ref}
    >
      <div className="logo cursor-pointer" onClick={() => router.push('/home')}>
      <Image
        className="object-contain"
        width={54}  // Use numeric value without "px"
        height={80} // Use numeric value without "px"
        src="/svg/logo.svg"
        alt="logo"
      />

      </div>
      <div className="menu mt-[-25px] relative">
        {/* Dropdown Trigger */}
        <div
          className="cursor-pointer"
          onClick={() => {
            setOpen((open) => !open);
          }}
        >
          <Image src="/svg/menu.svg" alt="menu" width={24} height={16} />
        </div>
        {/* Dropdown El */}
        <div
          className="absolute top-[28px] right-0 transition-all"
          style={{
            opacity: open ? 1 : 0,
            zIndex: open ? 999 : -999,
            background: '#fff',
            borderRadius: '8px',
          }}
        >
          {links?.map(({ title, link }) => {
            return (
              <div
                className="bg-white text-[#000000] first:rounded-t-[8px] last:rounded-b-[8px] min-w-[204px] min-h-[35px] sm:min-h-[45px] flex items-center hover:bg-slate-300 transition-all cursor-pointer"
                key={title}
                onClick={() => {
                  if (title === 'Log out') {
                    dispatch(logout());
                    router.push('/');
                  } else {
                    router.push(link);
                  }
                  setOpen(false);
                }}
              >
                <span className="ml-[16px]">{title}</span>
              </div>
            );
          })}
        </div>

        {/* <Dropdown placement="bottomRight" open={open} overlay={<div></div>} /> */}
      </div>
    </div>
  );
};
