import Image from 'next/image';
import { useOnClickOutside } from 'hooks/useOutsideClick.hook';
import { useRouter } from 'next/router';
import { logout } from 'store';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useRef, useState } from 'react';

const loggedOutLinks = [
  { title: 'Questions', link: '/questions' },
  { title: 'Create Profile', link: '/sign-up' },
  { title: 'About humans', link: '/about' },
  { title: 'Contact humans', link: '/contact-us' },
  { title: 'Log in', link: '/sign-in' },
];

const loggedInLinks = [
  { title: 'Questions', link: '/questions' },
  { title: 'My Profile', link: '/my-profile' },
  { title: 'About humans', link: '/about' },
  { title: 'Contact humans', link: '/contact-us' },
  { title: 'Log out', link: '/logout' },
];

export const BrickNav = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [links, setLinks] = useState([]);

  const { auth } = useSelector((state) => state?.auth);

  useEffect(() => {
    if (auth) {
      setLinks(loggedInLinks);
    } else {
      setLinks(loggedOutLinks);
    }
  }, [auth]);

  const ref = useRef();

  useOnClickOutside(ref, () => setOpen(false));

  return (
    <div
      className="md:w-[calc(100vw_-_75px)] w-[100vw] flex items-center justify-between p-[25px] pb-[0px] md:pr-[0px] pr-[25px] fixed z-[20]"
      ref={ref}
    >
      <div className="logo cursor-pointer" onClick={() => router.push('/home')}>
        <Image
          className="object-contain"
          width={54}
          height={80}
          src="/svg/logo.svg"
          alt="logo"
        />
      </div>
      <div className="menu relative">
        {/* Dropdown Trigger */}
        <div
          className="cursor-pointer"
          onClick={() => {
            setOpen((open) => !open);
          }}
        >
          <Image src="/svg/menu.svg" alt="menu" width={24} height={16} />
        </div>
        {/* Dropdown El */}
        <div
          className={`absolute top-[28px] right-0 transition-all ${
            open ? 'showing' : 'not-showing'
          }`}
          style={{ background: '#fff', borderRadius: '8px' }}
        >
          {links?.map(({ title, link }) => (
            <div
              className="bg-white text-[#000000] first:rounded-t-[8px] last:rounded-b-[8px] min-w-[204px] min-h-[45px] flex items-center hover:bg-slate-300 transition-all cursor-pointer"
              key={title}
              onClick={() => {
                if (title === 'Log out') {
                  dispatch(logout());
                  router.push('/');
                } else {
                  router.push(link);
                }
                setOpen(false);
              }}
            >
              <span className="ml-[16px]">{title}</span>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
