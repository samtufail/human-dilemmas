import { useRouter } from 'next/router';
import React, { useRef } from 'react';

import { DateComponent, formatDate } from 'sections/Surveys/Brick.section';

const CarouselItem = ({
  survey,
  width,
  maxHeight,
  maxHeightHovered,
  setHoveredSurvey,
  setSelectedSurvey,
  isSelected,
  isHovered,
  isMobile,
  setActive,
  index,
  setActiveIndex,
}) => {
  const surveyRef = useRef();
  const router = useRouter();

  const boxShadow = () => {
    if (isHovered) {
      return '17px 12px 20px rgba(0, 0, 0, 0.5), 0px 0px 200px 2px rgba(255, 255, 255, 0.175)'; //'rgba(255, 255, 255, 0.25) 0px 0px 11px 9px';
    } else {
      return '';
    }
  };

  const handleSurveyClick = () => {
    if (isMobile && isHovered) {
      setSelectedSurvey(survey.gid);

      setTimeout(() => {
        setActive(0); // This is to reset the active index to 0 So next time when the user clicks on the survey it will open the first survey
        if (survey.linked_video_question_id) {
          router.push(`/dilemmas/${survey.linked_video_question_id}`);
        } else {
          router.push(`/${survey?.name}/${survey?.gid}`);
        }
      }, 300);
    } else if (isMobile) {
      setActiveIndex(index);
    } else {
      setActive(0);
      if (survey.linked_video_question_id) {
        router.push(`/dilemmas/${survey.linked_video_question_id}`);
      } else {
        router.push(`/${survey?.name}/${survey?.gid}`);
      }
    }
  };

  return (
    <div
      onClick={handleSurveyClick}
      className={` h-full rounded-[2%] flex flex-col items-center justify-between	 relative ${
        isMobile ? 'p-[5px]' : 'p-[10px]'
      }   ${isHovered ? 'bg-[#262440]   h-full ' : 'bg-black-survey  h-full'}`}
      onMouseEnter={() => {
        setHoveredSurvey(survey);
      }}
      onMouseLeave={() => {
        isMobile ? null : setHoveredSurvey(null);
      }}
      style={{
        flex: '1 0 auto',
        width,
        transition: 'all 0.3s ease-in-out',
        background: isSelected && isMobile ? '#574081' : '',
        boxShadow: boxShadow(),
        maxHeight: isHovered ? maxHeightHovered : maxHeight,
        // height: isHovered ? '82%' : '68%',
      }}
      ref={surveyRef}
    >
      <img
        className="text-img h-[25%] pl-[5px] pt-[3px] self-start"
        src={survey?.cover_headline}
        alt="lies-text"
      />
      <img
        className="object-contain h-[57%] pb-[9px] lg:pb-[40px] lg:h-[55%]"
        src={survey?.cover_image}
        alt="lies-text"
      />
      <div
        className={`self-end items-end  text-right text-[3.6px] lg:text-[11px] pr-[10px] pb-[3px]`}
      >
        <div className="hidden">
          <span>
            Online since <br />
          </span>
          <DateComponent key={345} date={formatDate(survey?.date_published)} />
        </div>
      </div>
    </div>
  );
};

export default CarouselItem;
