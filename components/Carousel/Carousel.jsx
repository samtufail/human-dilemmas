import Image from 'next/image';
import CarouselItem from './CarouselItem';
import React, { useRef, useState } from 'react';
import { useEffect } from 'react';

const INTERVAL_SPEED = { SLOW: 250, MEDIUM: 150, FAST: 100 };
const SPEED_TRANSFORM_VALUE = 200;
const LARGE_BRICK_HEIGHT_RATIO = 0.5763;
const LARGE_BRICK_HEIGHT_RATIO_MOBILE = 0.5824;

export const Carousel = ({ openStatusSurveys, setActive }) => {
  const [surveys, setSurveys] = useState(openStatusSurveys);
  const [activeIndex, setActiveIndex] = useState(null);
  const [hoveredSurvey, setHoveredSurvey] = useState(null);
  const [selectedSurvey, setSelectedSurvey] = useState(null);
  const [slideInterval, setSlideInterval] = useState(null);
  const [accelerationTimeout, setAccelerationTimeout] = useState(null);
  const [intervalSpeed, setIntervalSpeed] = useState(INTERVAL_SPEED.SLOW);
  const [slidingDirection, setSlidingDirection] = useState(null);
  const [forceUpdate, setForceUpdate] = useState(false);

  //MOBILE
  const isMobile =
    window.innerWidth <= 1180 &&
    ('ontouchstart' in window ||
      navigator.maxTouchPoints > 0 ||
      navigator.msMaxTouchPoints > 0);
  const carouselRef = useRef(null);
  const [carouselOffset, setCarouselOffset] = useState(0);
  const [touchStartX, setTouchStartX] = useState(0);

  const largeBrickWidth = isMobile
    ? window.innerWidth * 0.36
    : window.innerWidth * 0.3;
  const largeBrickHoveredWidth = isMobile
    ? window.innerWidth * 0.52
    : window.innerWidth * 0.39;
  const mediumBrickWidth = largeBrickWidth * 0.66;
  const mediumBrickHoveredWidth = largeBrickHoveredWidth * 0.66;
  const smallBrickWidth = largeBrickWidth * 0.33;
  const smallBrickHoveredWidth = largeBrickHoveredWidth * 0.33;
  const gap = isMobile ? window.innerWidth * 0.064 : window.innerWidth * 0.02;

  useEffect(() => {
    activateIndexOnInitialLoad();
    window.addEventListener('resize', handleResize);
    window.addEventListener('orientationchange', handleResize);

    return () => {
      clearTimeoutAndInterval();
      window.removeEventListener('resize', handleResize);
      window.removeEventListener('orientationchange', handleResize);
    };
  }, []);

  const handleResize = () => {
    setForceUpdate((prev) => !prev);
  };

  const shuffleArray = (array) => {
    const newArray = [...array];
    for (
      let currentIndex = newArray.length - 1;
      currentIndex > 0;
      currentIndex--
    ) {
      const randomIndex = Math.floor(Math.random() * (currentIndex + 1));
      [newArray[currentIndex], newArray[randomIndex]] = [
        newArray[randomIndex],
        newArray[currentIndex],
      ];
    }
    return newArray;
  };

  const handleShuffleOnMobile = () => {
    const shuffledSurveys = shuffleArray(surveys);
    setSurveys(shuffledSurveys);
    let randomSurveyIndex;
    do {
      randomSurveyIndex = Math.floor(Math.random() * shuffledSurveys.length);
    } while (
      shuffledSurveys[randomSurveyIndex].size !== 'Large' &&
      randomSurveyIndex !== activeIndex
    );
    // Wait for 300 ms to let the shuffle animation finish
    setTimeout(() => {
      setActiveIndex(randomSurveyIndex);
    }, 300);
  };

  const handleShuffleOnDesktop = () => {
    const shuffledSurveys = shuffleArray(surveys);
    setSurveys(shuffledSurveys);
  };

  const activateIndexOnInitialLoad = () => {
    let randomSurveyIndex;
    do {
      randomSurveyIndex = Math.floor(Math.random() * surveys.length);
    } while (
      surveys[randomSurveyIndex]?.size !== 'Large' &&
      randomSurveyIndex !== activeIndex
    );

    setActiveIndex(randomSurveyIndex);
  };

  const getTransform = () => {
    const activeIndexOffset = surveys
      .slice(0, activeIndex + 1)
      .reduce((total, survey) => total + getSurveyWidth(survey) + gap, 0);
    const translateSum =
      window.innerWidth / 2 -
      activeIndexOffset +
      gap +
      getSurveyWidth(surveys[activeIndex]) / 2;

    const overlapWidth = checkIfHoveredOverlapping(translateSum);
    let totalTranslate = translateSum + overlapWidth + carouselOffset;

    const minimalTransformValue =
      window.innerWidth / 2 - getSurveyWidth(surveys[0]) / 2;
    const maxTransformValue =
      surveys.reduce(
        (total, survey) => total + getSurveyWidth(survey) + gap,
        0
      ) -
      window.innerWidth / 2 -
      getSurveyWidth(surveys.at(-1)) / 2 -
      gap;

    if (totalTranslate > minimalTransformValue) {
      totalTranslate = minimalTransformValue;
    } else if (totalTranslate < -maxTransformValue) {
      totalTranslate = -maxTransformValue;
    }

    return `translateX(${totalTranslate}px)`;
  };

  const getSurveyWidth = (survey) => {
    const isSurveyHovered = hoveredSurvey?.gid === survey.gid;
    if (survey.size === 'Large') {
      return isSurveyHovered ? largeBrickHoveredWidth : largeBrickWidth;
    }
    if (survey.size === 'Medium') {
      return isSurveyHovered ? mediumBrickHoveredWidth : mediumBrickWidth;
    }
    if (survey.size === 'Small') {
      return isSurveyHovered ? smallBrickHoveredWidth : smallBrickWidth;
    }
  };

  const moveHovered = () => {
    const hoveredIndex = surveys.findIndex(
      (surv) => surv.gid === hoveredSurvey?.gid
    );

    if (hoveredIndex === -1) {
      return 0;
    }

    let hoverSizeDifference = 0;
    if (surveys[hoveredIndex].size === 'Large') {
      hoverSizeDifference = largeBrickHoveredWidth - largeBrickWidth;
    }
    if (surveys[hoveredIndex].size === 'Medium') {
      hoverSizeDifference = mediumBrickHoveredWidth - mediumBrickWidth;
    }
    if (surveys[hoveredIndex].size === 'Small') {
      hoverSizeDifference = smallBrickHoveredWidth - smallBrickWidth;
    }

    if (activeIndex < hoveredIndex) {
      return -hoverSizeDifference / 2;
    }
    if (activeIndex > hoveredIndex) {
      return hoverSizeDifference / 2;
    }
    if (activeIndex === hoveredIndex) {
      return 0;
    }
  };
  const checkIfHoveredOverlapping = (translateSum) => {
    if (!hoveredSurvey || hoveredSurvey.offsetLeft === null) {
      return 0;
    }

    const hoveredIndex = surveys.findIndex(
      (surv) => surv.gid === hoveredSurvey.gid
    );
    const edgeWidth = 0.1 * window.innerWidth;
    const halfScreen = 0.5 * window.innerWidth;

    const surveyOffset = surveys
      .slice(0, hoveredIndex)
      .reduce(
        (total, survey, index) => total + getSurveyWidth(surveys[index]) + gap,
        0
      );

    const maxWidthBeforeOverlap = halfScreen - edgeWidth;

    let surveySize, surveySizeHovered;
    switch (hoveredSurvey.size) {
      case 'Large':
        surveySize = largeBrickWidth;
        surveySizeHovered = largeBrickHoveredWidth;
        break;
      case 'Medium':
        surveySize = mediumBrickWidth;
        surveySizeHovered = mediumBrickHoveredWidth;
        break;
      case 'Small':
        surveySize = smallBrickWidth;
        surveySizeHovered = smallBrickHoveredWidth;
        break;
    }
    // RIGHT SURVEY OF CENTERED ONE

    if (activeIndex < hoveredIndex) {
      const elementTranslationMiddlePosition = halfScreen - surveyOffset;

      const elementDistanceFromMiddle =
        elementTranslationMiddlePosition > 0
          ? translateSum - elementTranslationMiddlePosition
          : elementTranslationMiddlePosition - translateSum;

      const elementRealPosition =
        Math.abs(elementDistanceFromMiddle) + surveySize;
      const elementRealPositionHovered =
        Math.abs(elementDistanceFromMiddle) + surveySizeHovered;
      if (elementRealPosition > maxWidthBeforeOverlap) {
        return -(elementRealPositionHovered - maxWidthBeforeOverlap);
      }
      return moveHovered();
    }
    // LEFT SURVEY OF CENTERED ONE
    if (activeIndex > hoveredIndex) {
      const elementTranslationMiddlePosition =
        halfScreen - surveyOffset - surveySize;

      const elementDistanceFromMiddle =
        elementTranslationMiddlePosition < 0
          ? Math.abs(elementTranslationMiddlePosition) + translateSum
          : translateSum + elementTranslationMiddlePosition;

      const elementRealPosition =
        hoveredIndex === 0
          ? Math.abs(elementDistanceFromMiddle) +
            surveySizeHovered +
            window.innerWidth * 0.02
          : Math.abs(elementDistanceFromMiddle) + surveySize;

      // const elementRealPositionHovered =
      //   Math.abs(elementDistanceFromMiddle) + surveySizeHovered; not neccesary since translatex already sees it as hovered

      if (elementRealPosition > maxWidthBeforeOverlap) {
        return elementRealPosition - maxWidthBeforeOverlap;
      }
      return moveHovered();
    }
    return moveHovered();
  };

  // SLIDING ANIMATION RELATED CODE

  const handlePrev = () => {
    if (activeIndex === 0) {
      return;
    }
    setActiveIndex((prev) => prev - 1);
  };

  const handleNext = () => {
    if (activeIndex === surveys.length - 1) {
      return;
    }
    setActiveIndex((prev) => prev + 1);
  };

  const handleMouseEnter = (direction) => {
    if (isMobile || !direction) {
      return;
    }
    carouselRef.current.style.transition = 'all linear 0.3s';

    setSlidingDirection(direction);
  };

  const handleMouseLeave = () => {
    carouselRef.current.style.transition = 'all ease-in-out 0.3s ';
    centerCarouselOnStoppedSurvey();
    clearTimeoutAndInterval();
    setSlidingDirection(null);
    setIntervalSpeed(INTERVAL_SPEED.SLOW);
  };

  const centerCarouselOnStoppedSurvey = () => {
    const currentTransform = carouselRef.current.style.transform;
    const currentTransformValue = parseInt(currentTransform.slice(11), 10);
    const startingPoint = window.innerWidth / 2;
    surveys.reduce((total, survey, index) => {
      if (-total + startingPoint >= currentTransformValue) {
        setActiveIndex(index);
        if (activeIndex === index) {
          carouselRef.current.style.transform = `translateX(${
            -total + startingPoint - getSurveyWidth(surveys[index]) / 2
          }px)`;
        }
      }

      return total + getSurveyWidth(surveys[index]) + gap;
    }, 0);
  };
  const accelerateCarousel = () => {
    let speed;
    if (intervalSpeed === INTERVAL_SPEED.SLOW) {
      speed = INTERVAL_SPEED.MEDIUM;
    } else {
      speed = INTERVAL_SPEED.FAST;
    }
    setIntervalSpeed(speed);
  };

  useEffect(() => {
    if (isMobile) {
      setHoveredSurvey(surveys[activeIndex]);
    }
    if (activeIndex === surveys.length - 1 || activeIndex === 0) {
      clearTimeoutAndInterval();
      setSlidingDirection(null);
      return;
    }
  }, [activeIndex]);

  useEffect(() => {
    if (slidingDirection && carouselRef.current) { // Add a check for carouselRef.current
      clearTimeoutAndInterval();
  
      setSlideInterval(
        setInterval(() => {
          const currentTransform = carouselRef.current.style.transform;
  
          const minimalTransformValue =
            window.innerWidth / 2 - getSurveyWidth(surveys[0]) / 2;
  
          const maxTransformValue =
            surveys.reduce(
              (total, survey) => total + getSurveyWidth(survey) + gap,
              0
            ) -
            window.innerWidth / 2 -
            getSurveyWidth(surveys.at(-1)) / 2 -
            gap;
  
          const currentTransformValue = parseInt(
            currentTransform.slice(11),
            10
          );
  
          let updatedTransformValue =
            slidingDirection === 'forward'
              ? currentTransformValue - SPEED_TRANSFORM_VALUE
              : currentTransformValue + SPEED_TRANSFORM_VALUE;
  
          if (updatedTransformValue > minimalTransformValue) {
            updatedTransformValue = minimalTransformValue;
            carouselRef.current.style.transform = `translateX(${updatedTransformValue}px)`;
            clearTimeoutAndInterval();
          } else if (updatedTransformValue <= -maxTransformValue) {
            updatedTransformValue = -maxTransformValue;
            carouselRef.current.style.transform = `translateX(${updatedTransformValue}px)`;
            clearTimeoutAndInterval();
          }
  
          carouselRef.current.style.transform = `translateX(${updatedTransformValue}px)`;
        }, intervalSpeed)
      );
      setAccelerationTimeout(
        setTimeout(() => {
          clearTimeoutAndInterval();
          accelerateCarousel();
        }, 2000)
      );
    }
  }, [slidingDirection, intervalSpeed, carouselRef]); // Add carouselRef to the dependency array
  

  const clearTimeoutAndInterval = () => {
    clearTimeout(accelerationTimeout);
    clearInterval(slideInterval);
  };

  // MOBILE
  const handleTouchStart = (event) => {
    setTouchStartX(event.touches[0].clientX);
  };

  const handleTouchMove = (event) => {
    carouselRef.current.style.transition = '';
    const touchCurrentX = event.touches[0].clientX;
    const touchDifference = touchStartX - touchCurrentX;
    const maxValue = window.innerWidth;
    const minValue = 0;
    if (touchCurrentX > maxValue || touchCurrentX < minValue) {
      return;
    }
    setCarouselOffset(-touchDifference);
  };

  const handleTouchEnd = (event) => {
    carouselRef.current.style.transition = 'all 0.3s ease-in-out';
    const touchEndX = event.changedTouches[0].clientX;
    const touchDifference = touchStartX - touchEndX;

    if (touchDifference > 0) {
      // Swiped left
      handleNext();
    } else if (touchDifference < 0) {
      // Swiped right
      handlePrev();
    }
    setCarouselOffset(0);
  };

  return (
    <>
      {activeIndex !== null && (
        <>
          <div
            className={`carousel-container relative ${
              isMobile ? 'overflow-hidden' : ''
            }  lg:mt-0 mt-40`}
          >
            <div
              className="absolute w-[8.5%] bg-transparent h-full z-20 left-0 lg:h-[328px] top-1/2 -translate-y-1/2 flex pl-[1.5%]"
              onMouseEnter={() => handleMouseEnter('reverse')}
              onMouseLeave={handleMouseLeave}
            >
              {slidingDirection === 'reverse' && (
                <Image
                  src="/svg/slider-left.svg"
                  width={32}
                  height={32}
                  alt="icon"
                />
              )}
            </div>
            <div
              className="absolute w-[8.5%]  bg-transparent h-full z-20 right-0 lg:h-[328px] top-1/2 -translate-y-1/2  flex justify-end pr-[1.5%]"
              onMouseEnter={() => handleMouseEnter('forward')}
              onMouseLeave={handleMouseLeave}
            >
              {slidingDirection === 'forward' && (
                <Image
                  className=""
                  src="/svg/slider-right.svg"
                  width={32}
                  height={32}
                  alt="icon"
                />
              )}
            </div>

            <div
              className="absolute w-full pointer-events-none z-10 h-[300px] lg:h-[650px] 2xl:h-[750px]"
              style={{
                background:
                  'linear-gradient(90deg, rgba(0,0,0,1) 0%, rgba(0,0,0,1) 7%, rgba(0,0,0,0) 15%, rgba(0,0,0,0) 85%, rgba(0,0,0,1) 93%, rgba(0,0,0,1) 100%)',
              }}
            ></div>
            <div
              className={`carousel-wrapper flex h-[300px] lg:h-[calc(100vh_-_45px)] items-center gap-[6.4%] lg:gap-[2%]`}
              style={{
                transform: getTransform(),
                transition: 'all 0.3s ease-in-out',
              }}
              onTouchStart={handleTouchStart}
              onTouchEnd={handleTouchEnd}
              onTouchMove={(e) => handleTouchMove(e)}
              ref={carouselRef}
            >
              {surveys.map((survey, i) => (
                <CarouselItem
                  key={i}
                  survey={survey}
                  width={getSurveyWidth(survey)}
                  maxHeight={
                    isMobile
                      ? largeBrickWidth * LARGE_BRICK_HEIGHT_RATIO_MOBILE
                      : largeBrickWidth * LARGE_BRICK_HEIGHT_RATIO
                  }
                  maxHeightHovered={
                    isMobile
                      ? largeBrickHoveredWidth * LARGE_BRICK_HEIGHT_RATIO_MOBILE
                      : largeBrickHoveredWidth * LARGE_BRICK_HEIGHT_RATIO
                  }
                  hoveredSurvey={hoveredSurvey}
                  setHoveredSurvey={setHoveredSurvey}
                  setSelectedSurvey={setSelectedSurvey}
                  isSelected={survey.gid === selectedSurvey}
                  isHovered={
                    isMobile
                      ? activeIndex === i
                      : hoveredSurvey?.gid === survey.gid
                  }
                  isMobile={isMobile}
                  setActive={setActive}
                  index={i}
                  setActiveIndex={setActiveIndex}
                />
              ))}
            </div>
          </div>
          <div
            onClick={isMobile ? handleShuffleOnMobile : handleShuffleOnDesktop}
            className=" text-boxtext text-[18px] font-bold flex items-center gap-[12px] cursor-pointer mt-8 lg:absolute bottom-0 lg:bottom-[10%] z-10 w-full justify-center"
          >
            <Image
              className="object-contain cursor-pointer "
              src="/svg/shuffle-icon.svg"
              width={32}
              height={32}
              alt="icon"
            />
            <span>Shuffle</span>
          </div>
        </>
      )}
    </>
  );
};
