import { useEffect, useState, useRef } from 'react';
import Image from 'next/image';
import { Radio } from 'antd';

import { api } from 'utils/api';
import { ProgressBar } from '../../ProgressBar/ProgressBar.component';
import { WorldMapQuestionsCheckBox } from '../WorldMapQuestionsCheckBox/WorldMapQuestionsCheckBox.component';
import { useAnswerTime } from 'hooks';
import { answerTimeStart, answerTimeEnd } from 'utils';
import { useRouter } from 'next/router';

const Answered = ({ answered, style }) => {
  return (
    <div
      className="py-2 md:py-4 mt-[20px] mx-[100px] bg-[#1A232E] px-3 md:px-[26px] flex items-center gap-[20px] rounded-[4px]"
      style={style}
    >
      {/* Lock Icon */}
      <svg
        width="20"
        height="20"
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM8 15L3 10L4.41 8.59L8 12.17L15.59 4.58L17 6L8 15Z"
          fill="white"
          fillOpacity="0.4"
        />
      </svg>
      {/* Message */}
      <p className="text-[#fff] heading_graph_responsive">
        You&apos;ve already answered the question on <b>{answered?.date}</b>
      </p>
    </div>
  );
};

const ContinentCheckBox = ({ continent, customClass = '' }) => {
  return (
    <div
      className={`absolute z-[20] ${customClass} continent_checkbox text-white`}
    >
      <div className="bg-[#0E161F] px-3 py-0.5 min-w-[100px] text-center text-sm whitespace-nowrap">
        <p className="relative z-[30]">{continent}</p>
      </div>
      <div className="bg-[#0E161F] p-[8px] w-fit rounded-full mt-[-10px] ml-[-20px]">
        <div className="RadioBox-main2">
          <Radio value={continent}>
            <span className="radio-text"></span>
          </Radio>
        </div>
      </div>
    </div>
  );
};

export const WMQOAnswerPage = ({
  questions,
  active,
  setShowGraph,
  setGraphData,
  surveyId,
  percent,
  userData,
}) => {
  const [answer, setAnswer] = useState('');
  const [answered, setAnswered] = useState(null);
  const questionMainContainer = useRef(null);

  const adjustingScale = () => {
    if (questionMainContainer && questionMainContainer.current) {
      const height = questionMainContainer.current.clientHeight;
      const windowHeight = window.innerHeight;
      if (windowHeight - height < 20) {
        questionMainContainer.current.classList.add('scale-down-wmqo');
      } else {
        if (
          questionMainContainer.current.classList.contains('scale-down-wmqo')
        ) {
          questionMainContainer.current.classList.remove('scale-down-wmqo');
        }
      }
      questionMainContainer.current.classList.add('opacity-full');
    }
  };

  useEffect(() => {
    adjustingScale();
  }, [questionMainContainer.current]);

  window.onresize = adjustingScale;

  const [, sendAnswerTimeEndRequest] = useAnswerTime(
    questions[active - 1].unique_QID,
    surveyId,
    answerTimeStart,
    answerTimeEnd,
  );

  const router = useRouter();

  async function processAnswerTimeEnd() {
    sendAnswerTimeEndRequest();
  }

  // const { user } = useSelector((state) => state?.auth);

  // const offlineResults = useLiveQuery(() => db?.answeredQuestions?.toArray());

  const saveAnswer = async () => {
    // if (user === null) {
    //   const answerSaved = offlineResults?.find((q) => q?.qid === questions[active - 1]?.unique_QID);
    //   if (!answerSaved?.date) {
    //     await db?.answeredQuestions?.add({
    //       date: moment().format('Do MMM YYYY'),
    //       qid: questions[active - 1]?.unique_QID,
    //       answer,
    //     });
    //   }
    // }
    const data = await api.put(`/v1/surveys/${surveyId}/update/`, {
      qid: questions[active - 1].unique_QID,
      answer: answer && answered === null ? answer : null,
    });

    // Sending answerTimeEnd request to backend
    processAnswerTimeEnd();

    setGraphData({
      data: data?.data,
      answer: answer && answered === null ? answer : null,
    });
    setShowGraph(true);
    setAnswer('');
  };

  useEffect(() => {
    const qid = questions[active - 1]?.unique_QID;
    const previousAnswer = userData?.length
      ? userData?.find((data) => data?.qid === qid)
      : null;
    // : offlineResults?.find((data) => data?.qid === qid);
    if (previousAnswer) {
      setAnswered(previousAnswer);
      setAnswer(previousAnswer?.answer);
    } else {
      setAnswered(null);
    }
  }, [active, userData, questions]);

  // const choices = questions[active - 1].choices;

  return (
    <div
      ref={questionMainContainer}
      className="opacity-0 wmqo_main_container relative max-w-950 mx-[30px] overflow-x-hidden"
    >
      <div
        className={`bg-black-survey rounded-8 pb-[20px] lg:pb-[30px] pt-[1px] wmqo_container`}
      >
        <div className="progressBar-otr pt-[40px] px-[30px] sm:px-[65px] md:px-[100px]">
          <ProgressBar percent={percent} />
        </div>
        <div className="hidden 2xl:block">
          {answered?.date ? <Answered answered={answered} /> : null}
        </div>
        <p className="heading-M wmqo_question font-semibold text-white mt-[20px] 2xl:mt-[25px] 2xl:mb-[10px] px-[30px] sm:px-[65px] md:px-[100px]">
          {questions[active - 1].question}
        </p>
        <Radio.Group
          className={`map-radio-main select-none ${
            answered?.date ? 'pointer-events-none' : ''
          }`}
          name="radiogroup2"
          value={answer}
          onChange={(e) => {
            setAnswer(e.target.value);
          }}
        >
          <div
            className={`map-img-otr map-img-circle flex justify-center mt-[15px] mb-[15px] ${
              answered?.date ? 'map-img-otr-disabled' : ''
            }`}
          >
            <div className="max-h-[350px] max-w-[650px] aspect-[650/350] relative mx-[10px] sm:mx-[65px] md:mx-[100px]">
              <img className="h-full w-full" src="/map-img.png" alt="map"></img>
              <div className="continent_container absolute inset-0 h-full w-full grid grid-cols-10 grid-rows-5 gap-1">
                <ContinentCheckBox
                  continent={'North America'}
                  customClass="north_america_responsive north_america"
                />
                <ContinentCheckBox
                  continent={'Europe'}
                  customClass="europe_responsive europe"
                />
                <ContinentCheckBox
                  continent={'Africa'}
                  customClass="africa_responsive africa"
                />
                <ContinentCheckBox
                  continent={'Asia'}
                  customClass="asia_responsive asia"
                />
                <ContinentCheckBox
                  continent={'South America'}
                  customClass="south_america_responsive south_america"
                />
                <ContinentCheckBox
                  continent={'Australia'}
                  customClass="australia_responsive australia "
                />
              </div>
            </div>
          </div>
          <div
            className={`leave_blank_option surveyOpen-check px-[30px] sm:px-[65px] md:px-[100px]  ${
              answered?.date ? 'surveyOpen-check-disabled' : ''
            }`}
          >
            <WorldMapQuestionsCheckBox />
          </div>
        </Radio.Group>
        <div className="next-icon-otr flex items-center justify-between pt-[0px] ">
          <p className="share-text-otr"></p>
          <div className="content-otr"></div>
          {answer && (
            <div
              className="next-icon-inr flex mr-[30px] rounded-[12px] sm:rounded-[16px] h-[30px] w-[36px] p-[6px] sm:h-h-48  sm:w-[56px] justify-center bg-boxbg cursor-pointer -mt-[40px] select-none"
              onClick={saveAnswer}
            >
              <Image
                className="object-contain"
                width={32}
                height={32}
                src="/svg/next-arrow.svg"
                alt="logo"
              />
            </div>
          )}
        </div>
      </div>
      <div className="flex items-center mt-[5px] gap-x-10 w-full">
        <div
          className="exit-text-otr cursor-pointer underline heading-M2 text-textGray2 text-[12px]"
          onClick={() => router.push('/questions')}
        >
          Exit
        </div>
        <div className="2xl:hidden flex-grow flex justify-end">
          {answered?.date ? (
            <Answered
              answered={answered}
              style={{ width: 'fit-content', margin: 0 }}
            />
          ) : null}
        </div>
      </div>
    </div>
  );
};
