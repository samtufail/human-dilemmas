import Image from 'next/image';
import Link from 'next/link';
import { ProgressBar } from '../../ProgressBar/ProgressBar.component';
import { api } from 'utils/api';
import { useRouter } from 'next/router';
import { useEffect, useRef } from 'react';

export const WMQCAnswerPage = ({
  questions,
  active,
  setShowGraph,
  setGraphData,
  surveyId,
  percent,
}) => {
  const router = useRouter();

  const questionMainContainer = useRef(null);

  const adjustingScale = () => {
    if (questionMainContainer && questionMainContainer.current) {
      const height = questionMainContainer.current.clientHeight;
      const windowHeight = window.innerHeight;
      if (windowHeight - height < 20) {
        questionMainContainer.current.classList.add('scale-down-wmqo');
      } else {
        if (
          questionMainContainer.current.classList.contains('scale-down-wmqo')
        ) {
          questionMainContainer.current.classList.remove('scale-down-wmqo');
        }
      }
      questionMainContainer.current.classList.add('opacity-full');
    }
  };

  useEffect(() => {
    adjustingScale();
  }, [questionMainContainer.current]);

  window.onresize = adjustingScale;

  const moveNext = async () => {
    const data = await api.put(`/v1/surveys/${surveyId}/update/`, {
      qid: questions[active - 1].unique_QID,
    });
    setGraphData({ data: data?.data });
    setShowGraph(true);
  };

  const ContinentInfoBox = ({ continent, customClass = '' }) => {
    return (
      <div
        className={`absolute z-[20] continent_checkbox text-white ${customClass}`}
      >
        <div className="bg-black-survey2 px-3 py-0.5 min-w-[100px] text-center text-sm whitespace-nowrap">
          <p className="relative z-[30]">{continent}</p>
        </div>
        <div className="bg-black-survey2 p-[8px] w-fit mt-[-10px] ml-[-20px] flex items-center justify-center">
          <Image
            className="object-contain lock-img"
            width={25}
            height={25}
            src="/svg/lock-img.svg"
            alt="logo"
          />
        </div>
      </div>
    );
  };

  return (
    <div
      ref={questionMainContainer}
      className="opacity-0 wmqo_main_container relative max-w-950 mx-[30px] overflow-x-hidden"
    >
      <div
        className={`bg-black-survey2 rounded-8 pb-[20px] lg:pb-[30px] pt-[1px] wmqo_container`}
      >
        <div className="progressBar-otr pt-[40px] px-[30px] sm:px-[65px] md:px-[100px]">
          <ProgressBar percent={percent} />
        </div>
        <p className="heading-M wmqo_question font-semibold text-white mt-[20px] 2xl:mt-[25px] 2xl:mb-[10px] px-[30px] sm:px-[65px] md:px-[100px]">
          {questions[active - 1].question}
        </p>
        <div>
          <div
            className={`map-img-otr map-img-circle flex justify-center mt-[15px] mb-[15px]`}
          >
            <div className="max-h-[350px] max-w-[650px] aspect-[650/350] relative mx-[10px] sm:mx-[65px] md:mx-[100px]">
              <img className="h-full w-full" src="/map-img.png" alt="map"></img>
              <div className="continent_container absolute inset-0 h-full w-full grid grid-cols-10 grid-rows-5 gap-1">
                <ContinentInfoBox
                  continent={'North America'}
                  customClass="north_america_responsive north_america"
                />
                <ContinentInfoBox
                  continent={'Europe'}
                  customClass="europe_responsive europe"
                />
                <ContinentInfoBox
                  continent={'Africa'}
                  customClass="africa_responsive africa"
                />
                <ContinentInfoBox
                  continent={'Asia'}
                  customClass="asia_responsive asia"
                />
                <ContinentInfoBox
                  continent={'South America'}
                  customClass="south_america_responsive south_america"
                />
                <ContinentInfoBox
                  continent={'Australia'}
                  customClass="australia_responsive australia "
                />
              </div>
            </div>
          </div>
          <div
            className={`leave_blank_option surveyOpen-check px-[30px] sm:px-[65px] md:px-[100px]`}
          >
            <div className="RadioBox-main">
              <label className="ant-radio-wrapper">
                <span className="ant-radio flex items-center gap-3">
                  <Image
                    className="object-contain"
                    width={25}
                    height={25}
                    src="/svg/lock-img.svg"
                    alt="logo"
                  />
                  <span className="text-white text-base">Leave blank</span>
                </span>
              </label>
            </div>
          </div>
        </div>
        <div className="next-icon-otr flex items-center justify-between pt-[0px] ">
          <p className="share-text-otr"></p>
          <div className="content-otr"></div>
          <div
            className="next-icon-inr flex mr-[30px] rounded-[12px] sm:rounded-[16px] h-[30px] w-[36px] p-[6px] sm:h-h-48  sm:w-[56px] justify-center bg-boxbg cursor-pointer -mt-[40px] select-none"
            onClick={moveNext}
          >
            <Image
              className="object-contain"
              width={32}
              height={32}
              src="/svg/next-arrow.svg"
              alt="logo"
            />
          </div>
        </div>
      </div>
      <div className="flex items-center mt-[5px] gap-x-10 w-full">
        <div
          className="exit-text-otr cursor-pointer underline heading-M2 text-textGray2 text-[12px]"
          onClick={() => router.push('/questions')}
        >
          Exit
        </div>
      </div>
    </div>
  );
};
