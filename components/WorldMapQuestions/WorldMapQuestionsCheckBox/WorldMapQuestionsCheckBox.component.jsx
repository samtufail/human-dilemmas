import { Radio } from 'antd';
import React from 'react';

export const WorldMapQuestionsCheckBox = () => {
  return (
    <>
      <div className="RadioBox-main">
        <Radio value={'Leave Blank'}>
          <span className="text-white text-base">Leave blank</span>
        </Radio>
      </div>
    </>
  );
};
