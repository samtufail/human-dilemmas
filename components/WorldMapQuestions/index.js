import dynamic from 'next/dynamic';

// WMQC: World Map Questions (Closed)
export const WMQCAnswerPage = dynamic(
  () =>
    import('./WMQCAnswerPage/WMQCAnswerPage.component').then(
      (module) => module.WMQCAnswerPage
    ),
  { ssr: false }
);
export const WMQCGraphPage = dynamic(
  () =>
    import('./WMQCGraphPage/WMQCGraphPage.component').then(
      (module) => module.WMQCGraphPage
    ),
  { ssr: false }
);

// WMQO: World Map Questions (Open)
export const WMQOAnswerPage = dynamic(
  () =>
    import('./WMQOAnswerPage/WMQOAnswerPage.component').then(
      (module) => module.WMQOAnswerPage
    ),
  { ssr: false }
);
export const WMQOGraphPage = dynamic(
  () =>
    import('./WMQOGraphPage/WMQOGraphPage.component').then(
      (module) => module.WMQOGraphPage
    ),
  { ssr: false }
);
