import { useState, useEffect, useRef } from 'react';
import { Modal } from 'antd';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { useGraphTime } from 'hooks';
import { start, end } from 'utils';
import { Loader } from 'components/Loader.component';
import shareIcon from 'public/svg/share-icon.svg';

export const WMQCGraphPage = ({
  graphData,
  setGraphData,
  setShowGraph,
  active,
  setActive,
  questions,
  surveyId,
  shareableLink,
  verified,
}) => {
  const router = useRouter();
  const [copied, setCopied] = useState(false);
  const { auth } = useSelector((state) => state?.auth);
  const { user } = useSelector((state) => state?.auth);
  const [activeTab, setActiveTab] = useState('Unverified');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [results, setResults] = useState(null);
  const [hideShare, setHideShare] = useState(false);
  const [, sendEndRequest] = useGraphTime(
    questions[active - 1].unique_QID,
    'closed',
    start,
    end,
  );
  const graphMainContainer = useRef(null);
  const [isLoading, setLoading] = useState(true);

  const adjustingScale = () => {
    if (graphMainContainer && graphMainContainer.current) {
      const height = graphMainContainer.current.clientHeight;
      const windowHeight = window.innerHeight;
      if (windowHeight - height < 100) {
        graphMainContainer.current.classList.add('scale-down-wmgo');
      } else {
        if (graphMainContainer.current.classList.contains('scale-down-wmgo')) {
          graphMainContainer.current.classList.remove('scale-down-wmgo');
        }
      }
      graphMainContainer.current.classList.add('opacity-full');
      setLoading(false);
    }
  };

  useEffect(() => {
    adjustingScale();
  }, [graphMainContainer.current]);

  window.onresize = adjustingScale;

  const verified_surveytabopt = [
    ['All', 'all'],
    ['Women', 'women'],
    ['Men', 'men'],
    ['Under 40', 'under_forty'],
    ['40 or above', 'forty_or_above'],
  ];

  async function processGraphTimeEnd() {
    sendEndRequest();
  }

  //const results = graphData?.data?.results
  useEffect(() => {
    if (auth || verified) {
      setResults(graphData?.data?.results[0].all);
      if ((user?.gender && user?.year_of_birth) || verified) {
        setActiveTab('All');
      } else {
        setActiveTab('Verified');
      }
    } else {
      setResults(graphData?.data?.results);
    }
  }, [graphData]);
  useEffect(() => {
    const timer = setTimeout(() => {
      if (copied) {
        setHideShare(true);
      }
    }, 2000);
    return () => clearTimeout(timer);
  }, [copied]);
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const ContinentResult = ({ continent, customClass = '' }) => {
    return (
      <div
        className={`absolute z-[20] ${customClass} continent_checkbox text-white heading_graph_responsive`}
      >
        <div className="bg-black-survey2 px-3 py-0.5 min-w-[100px] text-center text-sm whitespace-nowrap">
          <p className="relative z-[30]">{continent}</p>
        </div>
        <div className="bg-black-survey2 p-[8px] h-10 w-14 rounded-[16px] mt-[-10px] ml-[-20px] flex items-center justify-center text-[20px]">
          {results[continent] ? Math.round(results[continent]) : 0}%
        </div>
      </div>
    );
  };

  const SurveyTabOption = ({ className = '', value, index }) => {
    const temp = graphData?.data?.results[index][value[1]];
    const conditionalLiClass = () => {
      switch (index) {
        case 2:
          return 'survey_li_3';
        case 3:
          return 'col-start-2 xs:col-start-auto survey_li_4';
        case 4:
          return 'survey_li_5';
        default:
          return '';
      }
    };
    const conditionalBtnClass = () => {
      switch (index) {
        case 0:
          return 'border-x-transparent';
        case 1:
          return 'rounded-0 survey_btn_2';
        case 2:
          return 'rounded-r-[5px] sm:rounded-r-8 xs:rounded-0 border-x-white survey_btn_3';
        case 3:
          return 'rounded-l-[5px] sm:rounded-l-8 border-x-transparent xs:border-x-white xs:rounded-0 survey_btn_4';
        case 4:
          return 'border-x-white rounded-l-0 survey_btn_5';
        default:
          return '';
      }
    };
    return (
      <li
        className={`survey-tab-li col-span-2 h-[25px] sm:h-[36px] flex items-center ${conditionalLiClass()} ${
          index > 2 ? 'mt-[6px] xs:mt-0' : ''
        }`}
      >
        <button
          className={`heading_graph_responsive survey-tab-button ${
            activeTab === value[0] ? 'active-survey-tab' : ''
          } w-w-110 whitespace-nowrap border h-full flex items-center justify-center sm:px-[8px] bg-tabbg border-x-white border-r-0 border-y-0 ${conditionalBtnClass()} ${className}`}
          onClick={() => {
            setResults(temp);
            setActiveTab(value[0]);
          }}
        >
          <span>{value[0]}</span>
        </button>
      </li>
    );
  };

  const RespondentsSection = () => {
    return (
      <div className="content-otr date-content heading_graph_responsive world_map_respondents">
        <p className=" text-white text-center font-normal">
          {/* Respondents = {graphData?.data?.respondents} */}
        </p>
        <p className="text-white text-center font-normal">
          {/* {graphData?.data?.range} */}
        </p>
      </div>
    );
  };

  if (!results) return null;
  return (
    <>
      {isLoading && (
        <div className="h-screen w-[98%] overflow-hidden flex items-center justify-center">
          <Loader />
        </div>
      )}
      <div
        ref={graphMainContainer}
        className="opacity-0 wmqo_main_container relative max-w-950 mx-[30px] overflow-x-hidden scrollbar-none"
      >
        <div className="bg-black-survey2 rounded-8 pb-[20px] lg:pb-[30px] pt-[1px] wmqo_container">
          <p className="heading-M wmqo_question font-semibold text-white my-6 xl:my-10 px-[30px] sm:px-[65px] md:px-[100px]">
            {questions[active - 1].question}
          </p>
          <div className="map-img-otr map-img-circle flex relative justify-center mt-[15px] mb-[0px]">
            <div className="max-h-[350px] max-w-[650px] aspect-[650/350] relative mx-[10px] sm:mx-[65px] md:mx-[100px]">
              <img className="h-full w-full" src="/map-img.png" alt="map"></img>
              <div className="continent_container absolute inset-0 h-full w-full grid grid-cols-10 grid-rows-5 gap-1">
                <ContinentResult
                  continent={'North America'}
                  customClass="north_america_responsive north_america"
                />
                <ContinentResult
                  continent={'Europe'}
                  customClass="europe_responsive europe"
                />
                <ContinentResult
                  continent={'Africa'}
                  customClass="africa_responsive africa"
                />
                <ContinentResult
                  continent={'Asia'}
                  customClass="asia_responsive asia"
                />
                <ContinentResult
                  continent={'South America'}
                  customClass="south_america_responsive south_america"
                />
                <ContinentResult
                  continent={'Australia'}
                  customClass="australia_responsive australia"
                />
              </div>
            </div>
          </div>
          <div className="leave_blank_option leave_blank_option_graph flex items-center gap-2  px-[30px] sm:px-[65px] md:px-[100px]  text-[13px] lg:text-[16px] leave_blank_percent">
            <p className="text-white">
              {results['Leave Blank'] ? Math.round(results['Leave Blank']) : 0}%
            </p>
            <p className="text-white">Leave blank</p>
          </div>
          <div></div>
          <div className="survey-tab-main flex flex-col items-center gap-[6px] sm:gap-2 mt-[28px] xl:mt-10">
            <ul className="survey-tab-ul flex items-center rounded-8 mt-2 xs:px-[40px]">
              {(user?.gender && user?.year_of_birth) || verified ? (
                <>
                  <ul className="survey-tab-ul survey_tab_options grid grid-cols-6 xs:grid-cols-10">
                    {verified_surveytabopt.map((value, index) => (
                      <SurveyTabOption
                        key={index}
                        value={value}
                        index={index}
                        className={`${
                          index === 0 ? 'rounded-l-[5px] sm:rounded-l-8' : ''
                        } ${
                          index === verified_surveytabopt.length - 1
                            ? 'rounded-r-[5px] sm:rounded-r-8'
                            : ''
                        }`}
                      />
                    ))}
                  </ul>
                </>
              ) : (
                <ul className="survey-tab-ul grid grid-cols-2 notlogged">
                  <li className="survey-tab-li h-[25px] sm:h-[36px] flex items-center">
                    <button
                      className={`heading_graph_responsive ${
                        activeTab === 'Unverified' ? `active-survey-tab` : ''
                      } survey-tab-button w-w-110 border h-full sm:px-[8px] bg-tabbg  border-y-0 rounded-l-8`}
                    >
                      Unverified
                    </button>
                  </li>
                  <li className="survey-tab-li h-[25px] sm:h-[36px] flex items-center">
                    <button
                      className={`heading_graph_responsive survey-tab-button ${
                        activeTab === 'Verified' ? `active-survey-tab` : ''
                      } w-w-110 border h-full sm:px-[8px] bg-tabbg border-x-white border-r-0 border-y-0 rounded-r-8`}
                      onClick={() => {
                        if (!auth) {
                          showModal();
                        }
                      }}
                    >
                      Verified
                    </button>
                  </li>
                </ul>
              )}
            </ul>
            <RespondentsSection />
          </div>
          <div
            className="w-full flex items-center justify-between pt-[20px] relative px-[30px] wmap_graph_btmribbon"
            style={shareableLink ? { height: '50px' } : null}
          >
            {copied &&
              (hideShare ? (
                <div className="share-text-otr">
                  <p className="heading-XS"></p>
                </div>
              ) : (
                <div className="action-otr flex relative">
                  <a className="text-[#010202] font-semibold mb-[10px] md:mb-0 heading_graph_responsive w-fit whitespace-nowrap md:font-semibold rounded-50 bg-white hover:text-[#010202] pt-[8px] pl-[14px] pr-[14px] pb-[8px] sm:absolute am:get_shareable_link left-0 bottom-[-30px] md:bottom-[-20px]">
                    Link copied to clipboard.
                  </a>
                </div>
              ))}
            {!copied && !shareableLink && (
              <a
                className="share-text-otr share_txt_graph flex items-center gap-[12px]"
                onClick={() => {
                  navigator?.clipboard?.writeText(
                    `${
                      process.env.NEXT_PUBLIC_APP_URL
                    }/questions/${surveyId}?activeQID=${
                      questions[active - 1].unique_QID
                    }&verified=${
                      user?.year_of_birth && user?.gender ? 'true' : 'false'
                    }`,
                  );
                  setCopied(!copied);
                }}
              >
                <Image
                  className="h-[18px] w-[18px] object-contain share-icon"
                  src={shareIcon}
                  alt="logo"
                />
                <p className="heading_graph_responsive text-white">
                  Get shareable link
                </p>
              </a>
            )}
            {!shareableLink ? (
              // TODO: Add time to survey data
              <div
                className="flex rounded-[12px] mb-[10px] lg:mb-0 sm:rounded-[16px] h-[30px] w-[36px] p-[6px] sm:h-h-48  sm:w-[56px] justify-center bg-boxbg cursor-pointer select-none"
                onClick={() => {
                  setActive(active + 1);
                  setShowGraph(false);
                  setGraphData(null);
                  processGraphTimeEnd();
                }}
              >
                <Image
                  className="object-contain"
                  width={32}
                  height={32}
                  src="/svg/next-arrow.svg"
                  alt="logo"
                />
              </div>
            ) : (
              <div className="absolute right-[25px]  md:right-[42px] bottom-[20px] -mb-[15px] heading_graph_responsive src_txt_graph">
                Source:{' '}
                <u onClick={() => router.push('/')} className="cursor-pointer">
                  humandilemmas.com
                </u>
              </div>
            )}
          </div>
        </div>
      </div>
      <Modal
        title="Basic Modal"
        className="unverified_modal h-full absolute inset-0 flex items-center justify-center"
        open={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p className="heading_graph_responsive">
          If you want to see the answers given by verified users then make a
          profile{' '}
          <span
            className="underline cursor-pointer"
            onClick={() => {
              localStorage.setItem('surveyId', surveyId);
              localStorage.setItem('activeQuestion', active);
              localStorage.setItem('currentShuffle', JSON.stringify(questions));
              router.push('/sign-up');
            }}
          >
            here
          </span>
          .
        </p>
      </Modal>
    </>
  );
};
