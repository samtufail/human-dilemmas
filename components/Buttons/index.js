import dynamic from 'next/dynamic';

export const CTAButton = dynamic(
  () => import('./CTA.component').then((module) => module.CTAButton),
  { ssr: false }
);
