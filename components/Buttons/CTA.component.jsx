export const CTAButton = ({ children, width, onClick }) => {
  const className =
    'cta_button justify-center bg-[#845EC2] hover:bg-[#583d85] font-semibold text-[14px] sm:text-[18px] transition-all flex items-center cursor-pointer sm:h-[60px] py-[10px] px-[20px] sm:py-[16px] rounded-[16px] select-none';
  return (
    <div
      className={`${className}`}
      style={{ width: width ? width : "auto" }}
      onClick={onClick}
    >
      {children}
    </div>
  );
};
