import dynamic from 'next/dynamic';

export const ZoomButton = dynamic(
  () => import('./ZoomButton.component').then((module) => module.ZoomButton),
  { ssr: false }
);

export const Heading = dynamic(
  () => import('./Heading.component').then((module) => module.Heading),
  { ssr: false }
);
