import Icon from '@ant-design/icons';
import { Dropdown } from 'antd';
import { useOnClickOutside } from 'hooks/useOutsideClick.hook';
import { useRef, useState } from 'react';

export const Heading = ({
  active,
  setActive,
  setActiveSurveys,
  genres,
  setReleaseDate,
  releaseDate,
}) => {
  const [visible, setVisible] = useState(false);

  const ref = useRef();
  useOnClickOutside(ref, () => setVisible(false));

  return (
    <div
      className="all-surveys-heading flex items-center gap-[32px]"
      style={{ userSelect: 'none' }}
    >
      <h3
        className={`cursor-pointer text-[#845EC2] inline font-[700] text-[20px] ${
          active === 'release'
            ? 'border-0 border-b-[3px] border-[#845EC2] border-solid'
            : ''
        }`}
        onClick={() => {
          if (active !== 'release') {
            setActive('release');
          } else {
            setReleaseDate((prev) => !prev);
          }
        }}
      >
        Release Date {releaseDate ? `↑` : `↓`}
      </h3>
      <Dropdown
        placement="bottomLeft"
        visible={visible}
        overlayStyle={{
          overflow: 'hidden',
          borderRadius: '8px',
          background: '#ccc',
        }}
        overlay={
          <div className="max-h-[190px] overflow-y-scroll" ref={ref}>
            <div
              onClick={() => {
                setActiveSurveys(genres);
                setVisible((prev) => !prev);
              }}
              className="bg-white first:rounded-t-[8px] last:rounded-b-[8px] min-w-[250px] min-h-[45px] flex items-center hover:bg-slate-300 transition-all cursor-pointer"
            >
              <span className="ml-[16px]">All</span>
            </div>
            {genres?.map((item, key) => {
              return (
                <div
                  key={key}
                  onClick={() => {
                    setActiveSurveys(genres[key]);
                    setVisible((prev) => !prev);
                  }}
                  className="bg-white first:rounded-t-[8px] last:rounded-b-[8px] min-w-[250px] min-h-[45px] flex items-center hover:bg-slate-300 transition-all cursor-pointer"
                >
                  <span className="ml-[16px]">{item?.genre_name}</span>
                </div>
              );
            })}
          </div>
        }
      >
        <h3
          className={`cursor-pointer text-[#845EC2] inline font-[700] text-[20px] ${
            active === 'genre'
              ? 'border-0 border-b-[3px] border-[#845EC2] border-solid'
              : ''
          }`}
          onClick={() => {
            if (active !== 'genre') {
              setActive('genre');
            } else {
              setVisible((prev) => !prev);
            }
          }}
        >
          <div className="flex items-center gap-[15px] ">
            <span className="text-[#845EC2] inline font-[700] text-[20px]">
              Genre
            </span>
            <Icon
              component={() => (
                <svg
                  width="10"
                  height="7"
                  viewBox="0 0 10 7"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M0 0.650391L5 6.53274L10 0.650391H0Z"
                    fill="#845EC2"
                  />
                </svg>
              )}
            />
          </div>
        </h3>
      </Dropdown>
    </div>
  );
};
