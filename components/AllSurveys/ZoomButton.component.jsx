import { Slider } from 'antd';
import Image from 'next/image';

export const ZoomButton = ({
  zoomLevel,
  setZoomLevel,
  active,
  isMoreThanLargeMobile,
}) => {
  const zoomPlus = () => {
    if (zoomLevel < 8 && zoomLevel >= 1) {
      setTimeout(() => setZoomLevel((zoomLevel) => zoomLevel + 1), 0.01);
    }
  };
  const zoomPMinus = () => {
    if (zoomLevel > 1 && zoomLevel <= 8) {
      setTimeout(() => setZoomLevel((zoomLevel) => zoomLevel - 1), 0.01);
    }
  };

  const onChange = (number) => {
    setTimeout(() => setZoomLevel(number), 0.01);
  };

  return (
    <>
      {active === 'genre' ? null : (
        <div
          className="zoom-icon-otr flex items-center gap-[4px] mt-[4px] z-10 mr-[0px] fixed mdbw:right-[50px] right-[15px]"
          style={{ userSelect: 'none' }}
        >
          <div
            onClick={zoomPlus}
            className="zoom-in-icon cursor-pointer xxs:h-[unset] mdbw:h-[unset] mdbw:w-[unset]"
          >
            {!isMoreThanLargeMobile ? (
              <img
                className="object-cover"
                src="/svg/zoom-icon-small.svg"
                alt="lies-text"
              />
            ) : (
              <Image
                className="object-contain"
                src="/svg/zoom-icon.svg"
                width={27}
                height={25}
                alt="lies-text"
              />
            )}
          </div>
          <Slider
            min={1}
            max={8}
            className="mdbw:w-[200px] w-[123px] hidden mdbw:block"
            // style={{ width: 200 }}
            onChange={onChange}
            value={zoomLevel}
            reverse
            tooltip={{
              formatter: (value) =>
                value === 1 ? `0%` : `${Math.round((value / 8) * 100)}%`,
            }}
          />
          <div
            onClick={zoomPMinus}
            className="zoom-in-icon cursor-pointer xxs:h-[unset] mdbw:h-[unset] mdbw:w-[unset]"
          >
            {!isMoreThanLargeMobile ? (
              <img
                className="object-cover"
                src="/svg/zoom-out-icon-small.svg"
                alt="lies-text"
              />
            ) : (
              <Image
                className="object-contain"
                src="/svg/zoom-out-icon.svg"
                width={27}
                height={25}
                alt="lies-text"
              />
            )}
          </div>
        </div>
      )}
    </>
  );
};
