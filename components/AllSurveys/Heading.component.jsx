import Icon from '@ant-design/icons';
import { Dropdown } from 'antd';
import { useOnClickOutside } from 'hooks/useOutsideClick.hook';
import { useRef, useState } from 'react';

export const Heading = ({
  active,
  setActive,
  setActiveSurveys,
  genres,
  setReleaseDate,
}) => {
  const [visible, setVisible] = useState(false);

  const ref = useRef();
  useOnClickOutside(ref, () => setVisible(false));

  return (
    <div
      className="all-surveys-heading flex items-center gap-[12px] fixed bottom-[10px] xxs:bottom-[20px] mdbw:bottom-[56px] mdbw:left-[unset] left-[20px] z-20"
      style={{ userSelect: 'none' }}
    >
      {active === 'genre' ? (
        <h3
          className={`cursor-pointer inline font-[700] text-[20px] ${
            active === 'genre' ? '' : ''
          }`}
          onClick={() => {
            if (active !== 'release') {
              setActive('release');
            } else {
              setReleaseDate((prev) => !prev);
            }
          }}
        >
          <div className="flex items-center justify-center gap-[16px] bg-[#845EC2] w-[50px] h-[40px] xxs:h-[45px] rounded-[12px]">
            <span className="text-white inline font-[500] text-[23px] leading-[0] md:leading-[unset] md:text-[20px]">
              ←
            </span>
          </div>
        </h3>
      ) : (
        <></>
      )}
      <div>
        <Dropdown
          placement="topRight"
          open={visible}
          overlayStyle={{
            overflow: 'hidden',
            borderRadius: '8px',
            background: '#ccc',
          }}
          overlay={
            <div className="max-h-[190px] overflow-y-scroll" ref={ref}>
              <div
                onClick={() => {
                  setActiveSurveys(genres);
                  setActive('genre');
                  setVisible((prev) => !prev);
                }}
                className="bg-white first:rounded-t-[8px] last:rounded-b-[8px] min-w-[250px] min-h-[45px] flex items-center hover:bg-slate-300 transition-all cursor-pointer"
              >
                <span className="ml-[16px]">All</span>
              </div>
              {genres?.map((item, key) => {
                return (
                  <div
                    key={key}
                    onClick={() => {
                      setActiveSurveys(genres[key]);
                      setActive('genre');
                      setVisible((prev) => !prev);
                    }}
                    className="bg-white first:rounded-t-[8px] last:rounded-b-[8px] min-w-[250px] min-h-[45px] flex items-center hover:bg-slate-300 transition-all cursor-pointer"
                  >
                    <span className="ml-[16px]">{item?.genre_name}</span>
                  </div>
                );
              })}
            </div>
          }
        >
          <h3
            className={`cursor-pointer text-[#845EC2] inline font-[700] text-[20px]`}
            onClick={() => {
              setVisible((prev) => !prev);
            }}
          >
            <div className="flex items-center justify-center gap-[8px] bg-[#845EC2] w-[120px] h-[40px] xxs:h-[45px] mdbw:w-[150px] mdbw:h-[45px] rounded-[12px]">
              <span className="text-white inline font-[500] text-[20px]">
                Genre
              </span>
              <Icon
                style={{ transform: 'rotate(180deg)' }}
                component={() => (
                  <svg
                    width="10"
                    height="7"
                    viewBox="0 0 10 7"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M0 0.650391L5 6.53274L10 0.650391H0Z"
                      fill="#fff"
                    />
                  </svg>
                )}
              />
            </div>
          </h3>
        </Dropdown>
      </div>
    </div>
  );
};
