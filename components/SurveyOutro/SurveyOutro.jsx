import { Carousel } from 'components/Carousel/Carousel';
import Image from 'next/image';
import { useRouter } from 'next/router';

export const SurveyOutro = ({ openStatusSurveys, setActive }) => {
  const router = useRouter();

  return (
    <div className=" w-full h-screen overflow-hidden">
      <div className=" lg:top-[9.5vh] z-40 relative lg:left-[85%] w-[32px] left-[87%] top-[100px]">
        <Image
          className="object-contain cursor-pointer "
          src="/svg/close-icon.svg"
          width={32}
          height={32}
          alt="icon"
          onClick={() => router.push('/questions')}
        />
      </div>

      <div className="top-[26%] lg:top-[14%] w-[70%] img-content-otr px-[10px] sm:px-[20px] absolute left-1/2  -translate-y-1/2 -translate-x-1/2">
        <p className="lg:text-[24px] font-normal text-center text-[18px]">
          You&apos;ve reached the end.
        </p>
        <p className="lg:text-[18px] font-normal text-center text-[16px]">
          More content here
        </p>
      </div>
      <Carousel openStatusSurveys={openStatusSurveys} setActive={setActive} />
    </div>
  );
};
