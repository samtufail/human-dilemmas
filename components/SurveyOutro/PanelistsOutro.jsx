import Image from 'next/image';
import { useRouter } from 'next/router';

export const PanelistsOutro = ({ type }) => {
  const router = useRouter();
  return (
    <div
      className={`survey_outro mx-[30px] max-w-[650px] ${
        type ? 'bg-black-survey2' : 'bg-black-survey'
      } rounded-8 pt-[25px] px-[10px] sm:px-[20px] pb-[20px]`}
    >
      <div
        className="close-icon-otr mr-[10px] flex justify-end"
        onClick={() => router.push('/admin/dashboard')}
      >
        <Image
          className="object-contain cursor-pointer"
          src="/svg/close-icon.svg"
          width={32}
          height={32}
          alt="icon"
        />
      </div>
      <div className="img-content-otr px-[10px] sm:px-[20px] mt-[40px] mb-[60px]">
        <p className="text-[18px] font-normal">
          You&apos;ve reached the end. We hope you found it interesting.
        </p>
      </div>
      <div
        className="video-icon-otr flex justify-end"
        onClick={() => router.push('/admin/dashboard')}
      >
        <div className="action-otr">
          <a className="finish-btn heading-S text-white hover:bg-black-cta2 hover:text-white pt-[10px] pl-[40px] pr-[40px] pb-[10px] mr-[10px] sm:mr-[15px] sm:mb-[15px] p-[10px] rounded-[12px] sm:rounded-[16px] h-[36px] w-[42px] sm:h-h-48  sm:w-[56px] flex justify-center bg-boxbg cursor-pointer items-center">
            Finish
          </a>
        </div>
      </div>
    </div>
  );
};
