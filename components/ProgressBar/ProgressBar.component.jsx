import React from 'react';
import styled, { keyframes } from 'styled-components';

const commonStyles = `
  height: 15px;
  border-radius: 14px;
`;

const ProgressBarOuter = styled.div`
  width: 100%;
  background-color: #6e7378;
  ${commonStyles}

  @media (max-width: 840px) {
    height: 7px;
  }
`;

const ProgressBarInner = styled.div`
  width: ${(props) => props?.percent}%;
  background-color: #fff;
  position: relative;
  ${commonStyles}

  @media (max-width: 840px) {
    height: 7px;
  }
`;

export const ProgressBar = ({ percent }) => {
  return (
    <ProgressBarOuter>
      <ProgressBarInner percent={percent}>
        <p
          className="absolute text-[#fff] text-[12px] md:text-[16px] -top-[20px] md:-top-[30px] -right-[10px] font-[600] sm:font-[500]"
          style={{ transform: 'translateX(5px)' }}
        >
          {percent}%
        </p>
      </ProgressBarInner>
    </ProgressBarOuter>
  );
};
