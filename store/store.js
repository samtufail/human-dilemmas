import auth from './auth/auth.reducer';
import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { createWrapper, HYDRATE } from 'next-redux-wrapper';
import globalReducer from './global';
import surveysReducer from './surveys';

const combinedReducer = combineReducers({
  auth,
  global: globalReducer,
  surveys: surveysReducer,
});

const reducer = (state, action) => {
  if (action.type === HYDRATE) {
    return { ...state, ...action.payload };
  }

  return combinedReducer(state, action);
};

const makeStore = () => configureStore({ reducer });

const wrapper = createWrapper(makeStore);

export { wrapper };

export const store = makeStore();