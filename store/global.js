import { createSlice } from '@reduxjs/toolkit';
import { api } from 'utils/api';

// ----------------------------------------------------------------------

const initialState = {
  countryCode: '+1',
  countryDetails: {
    code: 'US',
    name: 'United States',
  },

  graphTimeID: null,
};

const slice = createSlice({
  name: 'global',
  initialState,
  reducers: {
    setCountryCode: (state, action) => {
      state.countryCode = action.payload;
    },

    setCountryDetails: (state, action) => {
      state.countryDetails = action.payload;
    },

    setGraphTimeID: (state, action) => {
      state.graphTimeID = action.payload;
    },
  },
});

// Reducer
export default slice.reducer;
export const { actions } = slice; // actions = { setCountryDetails }

// ----------------------------------------------------------------------
// Actions

export const setCountryCode = (countryCode) => async (dispatch) => {
  dispatch(actions.setCountryCode(countryCode));
};

export const setCountryDetails = (countryDetails) => async (dispatch) => {
  dispatch(actions.setCountryDetails(countryDetails));
};

export const startGraphTime = (data) => async (dispatch) => {
  try {
    // { qid: qid, gid: gid, type: type === 'closed' ? 0 : 1 }
    const response = await api.post('/v1/questions/graph-time-start/', data);
    dispatch(actions.setGraphTimeID(response.data.id));
  } catch (error) {} // Will fail silently , no need to show error to user
};

export const endGraphTime = (id, data) => async (dispatch) => {
  // processID, { qid: qid }
  try {
    await api.put(`/v1/questions/graph-time-end/${id}`, data);
    dispatch(actions.setGraphTimeID(null));
  } catch (error) {} // Will fail silently , no need to show error to user
};
