import { createSlice } from '@reduxjs/toolkit';
import { api } from 'utils/api';

// ----------------------------------------------------------------------

const processQuestions = (response, firstQuestionID) => {
  if (!response) return [];

  const newQuestionsArray = [];
  let foundFirstQuestion = false; // Corrected to let

  response.forEach((survey) => {
    const { gid, name, questions, status } = survey;

    questions.forEach((question) => {
      const { unique_QID, question: questionText, ...rest } = question;

      const newQuestionObject = {
        gid,
        name,
        status,
        qid: unique_QID,
        question: questionText,
        ...rest,
      };

      if (!foundFirstQuestion && unique_QID === firstQuestionID) {
        // If this is the first question with the specified qid, add it to the beginning
        newQuestionsArray.unshift(newQuestionObject);
        foundFirstQuestion = true;
      } else {
        // Otherwise, add it to the end of the array
        newQuestionsArray.push(newQuestionObject);
      }
    });
  });

  return newQuestionsArray;
};

const processSingleQustion = (survey) => {
  if (!survey) return {};

  const { gid, name, questions, status } = survey;
  const processedQuestions = questions.map((question) => {
    const { unique_QID, question: questionText, ...rest } = question;
    return {
      gid,
      name,
      status,
      qid: unique_QID,
      question: questionText,
      ...rest,
    };
  });

  return processedQuestions; // Return the array of processed questions
};

const initialState = {
  openVideoSurveyQuestions: [],

  firstQuestionID: null,
  firstSurveyGID: null,
  firstQuestion: {},

  firstQuestionError: null,

  isLoading: false,
  error: null,
  currentIndex: 0,

  answeredQuestions: [], // Array of answered questions (qid) For the current Session on Dilemmas Page
  myAttemptedQuestions: [], // Array of answered questions (qid) For the current User
};

const slice = createSlice({
  name: 'surveys',
  initialState,

  reducers: {
    setFirstQuestionID: (state, action) => {
      state.firstQuestionID = action.payload;
    },

    setFirstSurveyGID: (state, action) => {
      state.firstSurveyGID = action.payload;
    },

    getOpenVideoSurveysStart: (state) => {
      state.isLoading = true;
    },

    setQuestionAsAnswered: (state, action) => {
      state.answeredQuestions.push(action.payload);
    },

    setCurrentIndex: (state, action) => {
      state.currentIndex = action.payload;
    },

    getOpenVideoSurveysSuccess: (state, action) => {
      state.isLoading = false;
      state.openVideoSurveyQuestions = processQuestions(
        action.payload,
        state.firstQuestionID
      ); // Process the questions here
    },

    pushIntoOpenVideoSurveyArray: (state, action) => {
      state.openVideoSurveyQuestions.push(action.payload);
    },

    getOpenVideoSurveysFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },

    getSurveybyGIDStart: (state) => {
      state.isLoading = true;
    },

    getSurveybyGIDSuccess: (state, action) => {
      state.isLoading = false;
      state.firstQuestion = processSingleQustion(action.payload)[0];
    },

    getSurveybyGIDFailure: (state, action) => {
      state.isLoading = false;
      state.firstQuestionError = action.payload;
    },

    getMyAttemptedQuestionsSuccess: (state, action) => {
      const uniqueObjects = [];
      action.payload.forEach((obj) => {
        if (!uniqueObjects.find((o) => o.qid === obj.qid)) {
          uniqueObjects.push(obj);
        }
      });
      state.myAttemptedQuestions = uniqueObjects;
    },

    getMyAttemptedQuestionsFailure: (state, action) => {
      console.log(action.payload);
    },

    resetState: () => initialState,
  },
});

// Reducer
export default slice.reducer;
export const { actions } = slice;

// ----------------------------------------------------------------------
// Actions

export const setFirstQuestionID = (firstQuestionID) => async (dispatch) => {
  dispatch(actions.setFirstQuestionID(firstQuestionID));
};

export const setFirstSurveyGID = (firstSurveyGID) => async (dispatch) => {
  dispatch(actions.setFirstSurveyGID(firstSurveyGID));
};

export const setQuestionAsAnswered = (object) => async (dispatch) => {
  dispatch(actions.setQuestionAsAnswered(object));
};

export const setCurrentIndex = (index) => async (dispatch) => {
  dispatch(actions.setCurrentIndex(index));
};

export const getOpenVideoSurveys = () => async (dispatch) => {
  dispatch(actions.getOpenVideoSurveysStart());

  try {
    const response = await api.get(
      '/v1/surveys/open-closed-random-video-surveys'
    );
    dispatch(actions.getOpenVideoSurveysSuccess(response?.data));
  } catch (error) {
    dispatch(actions.getOpenVideoSurveysFailure(error?.message));
  }
};

export const getSurveybyGID = (gid) => async (dispatch) => {
  dispatch(actions.getSurveybyGIDStart());

  try {
    const response = await api.get(`/v1/surveys/${gid}/video-survey`);
    dispatch(actions.getSurveybyGIDSuccess(response?.data));
  } catch (error) {
    dispatch(actions.getSurveybyGIDFailure(error?.message));
  }
};

export const pushIntoOpenVideoSurveyArray = (object) => async (dispatch) => {
  dispatch(actions.pushIntoOpenVideoSurveyArray(object));
};

export const getMyAttemptedQuestions = () => async (dispatch) => {
  try {
    const response = await api.get('/v1/questions/my-attempted-questions/');
    dispatch(actions.getMyAttemptedQuestionsSuccess(response?.data));
  } catch (error) {
    dispatch(actions.getMyAttemptedQuestionsFailure(error?.message));
  }
};

export const resetState = () => async (dispatch) => {
  dispatch(actions.resetState());
};
