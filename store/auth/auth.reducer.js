import { createSlice } from '@reduxjs/toolkit';
import { loginUser, loggedInUser } from './auth.actions';

const initialState = {
  user: null,
  auth: null,
  loading: false,
  initialLoading: true,
};

export const authSlice = createSlice({
  initialState,
  name: 'authSlice',
  reducers: {
    logout: (state) => {
      localStorage.removeItem('AuthToken');
      localStorage.removeItem('RefreshToken');
      return {
        ...state,
        user: null,
        auth: null,
        loading: false,
        initialLoading: false,
      };
    },
    setAuthToken: (state, { payload }) => {
      return {
        ...state,
        auth: payload,
      };
    },
  },
  extraReducers(builder) {
    // Login Start
    builder
      .addCase(loginUser.pending, (state) => ({
        ...state,
        loading: true,
        initialLoading: true,
      }))
      .addCase(loginUser.fulfilled, (state, { payload }) => ({
        ...state,
        auth: payload,
        loading: false,
        initialLoading: false,
      }))
      .addCase(loginUser.rejected, (state) => ({
        ...state,
        auth: null,
        loading: false,
        initialLoading: false,
      }))
      // Login End

      // Logged In User Start
      .addCase(loggedInUser.pending, (state) => ({
        ...state,
        loading: true,
        initialLoading: true,
      }))
      .addCase(loggedInUser.fulfilled, (state, { payload }) => ({
        ...state,
        user: payload,
        loading: false,
        initialLoading: false,
      }))
      .addCase(loggedInUser.rejected, (state) => ({
        ...state,
        loading: false,
        initialLoading: false,
      }));
    // Logged In User End
  },
});
const { actions, reducer } = authSlice;

export const { logout, setAuthToken } = actions;

export default reducer;
