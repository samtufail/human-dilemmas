import { createAsyncThunk } from '@reduxjs/toolkit';
import { api } from 'utils';
import { toast } from 'react-toastify';

// Get Logged In User
export const loggedInUser = createAsyncThunk('users/loggedInUser', async () => {
  const res = await api.get('/v1/user/detail/');
  return res?.data;
});

// Login User
export const loginUser = createAsyncThunk(
  'auth/login',
  async ({ phone, password, router }) => {
    try {
      const surveyId = localStorage?.getItem('surveyId');
      const activeQuestion = localStorage?.getItem('activeQuestion');
      const res = await api.post('/token/', { phone_number: phone, password });
      localStorage.setItem('AuthToken', res?.data?.access);
      localStorage.setItem('RefreshToken', res?.data?.refresh);
      toast.success('Logged In');
      if (surveyId && activeQuestion) {
        router.push(`/surveys/${surveyId}?backFromRegister=true`);
      } else {
        router.push('/');
      }
      return res?.data;
    } catch (error) {
      if (error?.response?.data?.non_field_errors?.length) {
        error?.response?.data?.non_field_errors?.forEach((error) => {
          toast.error(error);
        });
      } else if (error?.response?.data?.detail) {
        toast.error(error?.response?.data?.detail);
      } else {
      }
      throw error;
    }
  }
);
