import { useState, useEffect } from 'react'

/**
 * A custom hook for sending start and end requests.
 *
 * @param {String} qid - The qid of the question.
 * @param {String} type - The type of the Survey.
 * @param {Function} start - The function to send the start request.
 * @param {Function} end - The function to send the end request.
 * @returns {Array} An array containing the process ID and a function for sending the end request.
 */
export function useGraphTime(qid, type, start, end) {
  const [processID, setProcessID] = useState(null)
  const url = window.location.href
  const urlArray = url.split('/')
  const gid = 'G-' + urlArray[urlArray.length - 1].split('-')[1]

  useEffect(() => {
    const startRequest = async () => {
      const processID = await start({ qid: qid, gid: gid, type: type === 'closed' ? 0 : 1 })
      setProcessID(processID)
    }
    startRequest()
  }, [qid])

  /**
   * Sends an end request for the specified qid and process ID.
   */
  async function sendEndRequest() {
    await end(processID, { qid: qid })
  }

  return [processID, sendEndRequest]
}
