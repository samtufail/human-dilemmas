import { useState, useEffect } from 'react'

/**
 * A custom hook for sending answerTimeStart and answerTimeEnd requests.
 *
 * @param {String} qid - The qid of the question.
 * @param {String} surveyId - The survey ID.
 * @param {Function} answerTimeStart - The answerTimeStart function.
 * @param {Function} answerTimeEnd - The answerTimeEnd function.
 * @returns {Array} An array containing the process ID and a function for sending the end request.
 */
export function useAnswerTime(qid, surveyId, answerTimeStart, answerTimeEnd) {
  const [processID, setProcessID] = useState(null)

  useEffect(() => {
    const startRequest = async () => {
      const processID = await answerTimeStart({ qid: qid, gid: surveyId })
      setProcessID(processID)
    }
    startRequest()
  }, [qid])

  /**
   * Sends an end request for the specified qid and process ID.
   */
  async function sendAnswerTimeEndRequest() {
    await answerTimeEnd(processID, { qid: qid, gid: surveyId })
  }

  return [processID, sendAnswerTimeEndRequest]
}