import { useState, useEffect } from 'react';

export function useLongPress(
  upCallback = () => {},
  downCallback = () => {},
  ms = 300
) {
  const [startLongPress, setStartLongPress] = useState('');

  useEffect(() => {
    let timerId;
    if (startLongPress === 'down') {
      timerId = setTimeout(downCallback, ms);
    } else {
      clearTimeout(timerId);
    }

    if (startLongPress === 'up') {
      upCallback();
    }

    return () => {
      clearTimeout(timerId);
    };
  }, [upCallback, downCallback, ms, startLongPress]);

  return {
    onMouseDown: () => setStartLongPress('down'),
    onMouseUp: () => setStartLongPress('up'),
    // onMouseLeave: () => setStartLongPress('up'),
    onTouchStart: () => setStartLongPress('down'),
    onTouchEnd: () => setStartLongPress('up'),
  };
}
