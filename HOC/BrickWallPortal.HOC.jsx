import { useEffect, useState } from 'react';
import { createPortal } from 'react-dom';

export const BrickWallPortal = ({ children }) => {
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
    // const el = document.getElementById('myportal');
    // el.classList.add('h-screen');
    // el.classList.add('w-screen');
    // el.classList.add('absolute');
    // el.classList.add('top-[0px]');
    return () => {
      setMounted(false);
    };
  }, []);

  return mounted
    ? createPortal(children, document.querySelector('#brick-wall-portal'))
    : null;
};
