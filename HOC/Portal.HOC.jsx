import { useEffect, useState } from 'react';
import { createPortal } from 'react-dom';

export const Portal = ({ children }) => {
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
    const el = document.getElementById('myportal');
    el.style.width = '100vw';
    el.style.height = '100vh';
    el.style.position = 'absolute';
    el.style.top = '0px';
    return () => {
      setMounted(false);
      el.style.removeProperty('width');
      el.style.removeProperty('height');
      el.style.removeProperty('position');
      el.style.removeProperty('top');
    };
  }, []);

  return mounted
    ? createPortal(children, document.querySelector('#myportal'))
    : null;
};
