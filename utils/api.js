import axios from 'axios';

// const KEY = process.env.NEXT_PUBLIC_YT_API;

export const youtubeAPI = axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  // params: {
  //   key: KEY,
  // },
});

export const api = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  headers: {
    'content-type': 'application/json',
  },
});


