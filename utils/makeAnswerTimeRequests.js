import { api } from './api';

/**
 * Sends a POST request to start the answer time for a question.
 *
 * @param {Object} data - Data payload for the API request.
 * @returns {Number} ID of the started answer time.
 */
export const answerTimeStart = async (data) => {
  try {
    const response = await api.post('/v1/questions/answer-time-start/', data);
    return response.data.id;
  } catch (error) {} // Will fail silently , no need to show error to user
};

/**
 * Sends a PUT request to end the answer time for a question.
 *
 * @param {Number} id - ID of the answer time to be ended.
 * @param {Object} data - Data payload for the API request.
 * @returns {Number} ID of the ended answer time.
 */
export const answerTimeEnd = async (id, data) => {
  try {
    const response = await api.put(`/v1/questions/answer-time-end/${id}`, data);
    return response.data.id;
  } catch (error) {} // Will fail silently , no need to show error to user
};
