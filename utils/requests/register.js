export const createUser = '/v1/user/create/';
export const verifyUser = '/v1/user/verify/';
export const setPassword = '/v1/user/set-password/';
export const resendOtp = '/v1/user/resend-otp/';
