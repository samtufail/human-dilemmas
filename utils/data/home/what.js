export const what = [
  {
    id: '1a2',
    img: '/svg/home/qa.svg',
    title: 'Questions',
    description:
      "It's survey questions about topics such as abortion or consent.",
  },
  {
    id: '1a3',
    img: '/svg/home/tough.svg',
    title: 'Tough choices',
    description: `Questions are picked to make you pause. And second guess yourself.`,
  },
  {
    id: '1a4',
    img: '/svg/home/taboos.svg',
    title: 'Taboos',
    description: `It's a platform where we also ask questions about taboo matters.`,
  },
  {
    id: '1a5',
    img: '/svg/home/mirror.svg',
    title: 'A mirror ',
    description: `It's a mirror you can use to get to know yourself better.`,
  },
  {
    id: '1a6',
    img: '/svg/home/decision.svg',
    title: 'Anonymity',
    description: `You are 100% anonymous unless you decide not to be.`,
  },
  {
    id: '1a7',
    img: '/svg/home/human.svg',
    title: 'Human',
    description: `Fake accounts and bots ruin the results, so we verify all signups. `,
  },
];


export const unverified = [
  {
      "question": "Do we use cookies to track user's activity on site?",
      "answer": "No"
  },
  {
      "question": "Do we store year of birth?",
      "answer": "No"
  },
  {
      "question": "Do we know your name?",
      "answer": "No"
  },
  {
      "question": "Do we store phone no.?",
      "answer": "No"
  },
  {
      "question": "Do we store country?",
      "answer": "No"
  },
  {
      "question": "Do we store gender?",
      "answer": "No"
  },
//   {
//       "question": "Do we store your information when you answer on individual level (Do we know who is answering)?",
//       "answer": "No"
//   },
  {
      "question": "Do we store your information when you answer on aggregate level?",
      "answer": "Yes"
  },
  {
      "question": "Do we use analytics tools for demographical information?",
      "answer": "Yes"
  },
  {
      "question": "Do we use analytics tools to improve user experience?",
      "answer": "Yes"
  },
  {
      "question": "Do we use analytics tools to track personal and sensitive information?",
      "answer": "No"
  },
  {
      "question": "Do we use analytics tools for ads suggestion on other platforms?",
      "answer": "No"
  },
  {
      "question": "Do we use microphone monitoring?",
      "answer": "No"
  },
  {
      "question": "Do we use camera monitoring?",
      "answer": "No"
  },
  {
      "question": "Do we use session recording tools?",
      "answer": "Yes"
  },
  {
      "question": "Do we use heatmaps to track traffic on website?",
      "answer": "No"
  },
  {
      "question": "Do we allow 3rd parties to track user's information?",
      "answer": "No"
  },
  {
      "question": "Do we sell data on personal level?",
      "answer": "No"
  },
  {
      "question": "Do we sell data on aggregate level ",
      "answer": "No"
  },
  {
      "question": "Do we store geolocation by using IP-Address?",
      "answer": "No"
  },
  {
      "question": "Do we store IP-Addresses?",
      "answer": "Yes"
  },
  {
      "question": "Do we store your browser and operating system information?",
      "answer": "No"
  }
]

export const verified = [
  {
      "question": "Do we use essential cookies when logging in?",
      "answer": "Yes"
  },
  {
      "question": "Do we store year of birth?",
      "answer": "Yes"
  },
  {
      "question": "Do we know your name?",
      "answer": "No"
  },
  {
      "question": "Do we store phone no.?",
      "answer": "Yes"
  },
  {
      "question": "Do we store country?",
      "answer": "Yes"
  },
  {
      "question": "Do we store gender?",
      "answer": "Yes"
  },
//   {
//       "question": "Do we store your information when you answer on individual level (Do we know who is answering)?",
//       "answer": "No"
//   },
  {
      "question": "Do we store your information when you answer on aggregate level?",
      "answer": "Yes"
  },
  {
      "question": "Do we use analytics tools for demographical information?",
      "answer": "Yes"
  },
  {
      "question": "Do we use analytics tools to improve user experience?",
      "answer": "Yes"
  },
  {
      "question": "Do we use analytics tools to track personal and sensitive information?",
      "answer": "No"
  },
  {
      "question": "Do we use analytics tools for ads suggestion on other platforms?",
      "answer": "No"
  },
  {
      "question": "Do we use microphone monitoring?",
      "answer": "No"
  },
  {
      "question": "Do we use camera monitoring?",
      "answer": "No"
  },
  {
      "question": "Do we use session recording tools?",
      "answer": "Yes"
  },
  {
      "question": "Do we use heatmaps to track traffic on website?",
      "answer": "No"
  },
  {
      "question": "Do we allow 3rd parties to track user's information?",
      "answer": "No"
  },
  {
      "question": "Do we sell data on personal level?",
      "answer": "No"
  },
  {
      "question": "Do we sell data on aggregate level ",
      "answer": "No"
  },
  {
      "question": "Do we store geolocation by using IP-Address?",
      "answer": "No"
  },
  {
      "question": "Do we store IP-Addresses?",
      "answer": "Yes"
  },
  {
      "question": "Do we store your browser and operating system information?",
      "answer": "No"
  }
]