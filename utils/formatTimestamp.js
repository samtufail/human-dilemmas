export const formatTimestamp = (timestamp) => {
  const date = new Date(timestamp);
  const options = { day: '2-digit', month: 'long', year: 'numeric' };
  return date.toLocaleDateString(undefined, options);
};
