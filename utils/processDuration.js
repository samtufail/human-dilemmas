import moment from 'moment';
import 'moment-duration-format';

export const processDuration = (duration) => {
  return moment.duration(duration).format('hh:mm:ss').padStart(4, '0:0');
};
