import Dexie from 'dexie';

export const db = new Dexie('myDatabase');
db.version(1).stores({
  answeredQuestions: '++id, qid, date, answer', // Primary key and indexed props
});
