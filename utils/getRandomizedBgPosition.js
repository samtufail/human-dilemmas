/**
 * Generates a random background position for an image based on the provided size.
 *
 * @param {string} size - The size of the image ('Large', 'Medium', or 'Small').
 * @returns {string} The background position in the format 'x% 50%'.
 */
export const randomBackgroundPosition = (size) => {
  let minLine;
  let maxLine;

  switch (size) {
    case 'Large':
      return 'center';
    case 'Medium':
    case 'Small':
      minLine = 1;
      maxLine = 10;
      break;
    default:
      minLine = 2;
      maxLine = 4;
  }

  const line = Math.floor(Math.random() * (maxLine - minLine + 1)) + minLine;
  const backgroundPosition = `${line * 10}% 50%`; // Set background position based on the selected line

  return backgroundPosition;
};
