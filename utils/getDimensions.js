export const getDimensions = ({ increasePercentage, width, isMoreThanLargeMobile, isLessThanMobile }) => {
  let gap;
  let minHeight;
  let headlineImageHeight;
  let coverImageWidth;
  let onlineSinceTextSize;
  let viewResultsTextSize;
  let cardPadding;
  let viewResultsTop;
  let viewResultsRight;
  let borderRadiusOfCard;
  let areaWithoutSpaces;
  let widthOfCard;
  let oneThirdCard;
  let twoThirdCard;
  let totalWidth;
  if (increasePercentage !== 'genre') {
    gap = 10 * increasePercentage;
    minHeight = 120 * increasePercentage;
    headlineImageHeight = 26 * increasePercentage;
    coverImageWidth = 48 * increasePercentage;
    onlineSinceTextSize = 5 * increasePercentage;
    viewResultsTextSize = 10 * increasePercentage;
    cardPadding = 7 * increasePercentage;
    viewResultsTop = 5 * increasePercentage;
    viewResultsRight = 5 * increasePercentage;
    borderRadiusOfCard = 4 * increasePercentage;
    totalWidth = width * increasePercentage;
    areaWithoutSpaces = totalWidth - gap * 6 - minHeight;
    widthOfCard = areaWithoutSpaces / 7;
    oneThirdCard = widthOfCard / 3;
    twoThirdCard = oneThirdCard * 2;
  } else {
    gap = 24;
    minHeight = 288;
    headlineImageHeight = 62;
    coverImageWidth = 115;
    onlineSinceTextSize = 12;
    viewResultsTextSize = 14;
    cardPadding = 17;
    viewResultsTop = 12;
    viewResultsRight = 12;
    borderRadiusOfCard = 9.6;
    totalWidth = width;
    areaWithoutSpaces = totalWidth - gap * 6;
    widthOfCard = (areaWithoutSpaces / 8) * 3;
    oneThirdCard = areaWithoutSpaces / 8;
    twoThirdCard = (areaWithoutSpaces / 8) * 2;
  }
  totalWidth =
    // width < 500 && increasePercentage === 'genre' ? totalWidth * 4 :
    //   width < 800 && increasePercentage === 'genre' ? totalWidth * 2.4 :
    //     width < 1300 && increasePercentage === 'genre' ? totalWidth * 1.4 :
    // Genre
    width < 300 && increasePercentage === 'genre' ? totalWidth * 3.5 :
      width < 500 && increasePercentage === 'genre' ? totalWidth * 2.5 :
        width < 700 && increasePercentage === 'genre' ? totalWidth * 1.8 :
          width < 800 && increasePercentage === 'genre' ? totalWidth * 1.6 :
            width < 920 && increasePercentage === 'genre' ? totalWidth * 2 :
              width < 1026 && increasePercentage === 'genre' ? totalWidth * 1.5 :
                width < 1300 && increasePercentage === 'genre' ? totalWidth * 1.2 :
                  // Brickwall
                  width < 300 && increasePercentage !== 'genre' ? totalWidth * 4 :
                    width < 500 && increasePercentage !== 'genre' ? totalWidth * 2.5 :
                      width < 700 && increasePercentage !== 'genre' ? totalWidth * 1.8 :
                        width < 800 && increasePercentage !== 'genre' ? totalWidth * 1.4 :
                          width < 920 && increasePercentage !== 'genre' ? totalWidth * 1.9 :
                            width < 1026 && increasePercentage !== 'genre' ? totalWidth * 1.5 :
                              width < 1300 && increasePercentage !== 'genre' ? totalWidth * 1.2 :
                                totalWidth;

  gap = width < 800 ? gap * 0.65 : gap;
  minHeight = width < 800 ? minHeight * 0.65 : minHeight;
  headlineImageHeight = width < 800 ? headlineImageHeight * 0.65 : headlineImageHeight;
  coverImageWidth = width < 800 ? coverImageWidth * 0.65 : coverImageWidth;
  onlineSinceTextSize = width < 800 ? onlineSinceTextSize * 0.65 : onlineSinceTextSize;
  viewResultsTextSize = width < 800 ? viewResultsTextSize * 0.65 : viewResultsTextSize;
  cardPadding = width < 800 ? cardPadding * 0.65 : cardPadding;
  viewResultsTop = width < 800 ? viewResultsTop * 0.65 : viewResultsTop;
  viewResultsRight = width < 800 ? viewResultsRight * 0.65 : viewResultsRight;
  borderRadiusOfCard = width < 800 ? borderRadiusOfCard * 0.65 : borderRadiusOfCard;

  return {
    totalWidth: Math.round(totalWidth),
    gap: Math.round(gap),
    minHeight: Math.round(minHeight),
    headlineImageHeight: Math.round(headlineImageHeight),
    coverImageWidth: Math.round(coverImageWidth),
    onlineSinceTextSize: Math.round(onlineSinceTextSize),
    viewResultsTextSize: Math.round(viewResultsTextSize),
    cardPadding: Math.round(cardPadding),
    viewResultsTop: Math.round(viewResultsTop),
    viewResultsRight: Math.round(viewResultsRight),
    borderRadiusOfCard: Math.round(borderRadiusOfCard),
  };
};

export const getCurrentWidth = ({ zoomLevel, width, isMoreThanLargeMobile, isLessThanMobile }) => {
  switch (zoomLevel) {
    case 1:
      return getDimensions({ increasePercentage: 1, width, isMoreThanLargeMobile, isLessThanMobile });
    case 2:
      return getDimensions({ increasePercentage: 1.2, width, isMoreThanLargeMobile, isLessThanMobile });
    case 3:
      return getDimensions({ increasePercentage: 1.4, width, isMoreThanLargeMobile, isLessThanMobile });
    case 4:
      return getDimensions({ increasePercentage: 1.6, width, isMoreThanLargeMobile, isLessThanMobile });
    case 5:
      return getDimensions({ increasePercentage: 1.8, width, isMoreThanLargeMobile, isLessThanMobile });
    case 6:
      return getDimensions({ increasePercentage: 2.0, width, isMoreThanLargeMobile, isLessThanMobile });
    case 7:
      return getDimensions({ increasePercentage: 2.2, width, isMoreThanLargeMobile, isLessThanMobile });
    case 8:
      return getDimensions({ increasePercentage: 2.4, width, isMoreThanLargeMobile, isLessThanMobile });
    case 'genre':
      return getDimensions({ increasePercentage: 'genre', width, isMoreThanLargeMobile, isLessThanMobile });
    default:
      break;
  }
};

// export const getCurrentWidth = ({ zoomLevel, width }) => {
//   switch (zoomLevel) {
//     case 1:
//       return getDimensions({ increasePercentage: 1, width });
//     case 2:
//       return getDimensions({ increasePercentage: 1.14, width });
//     case 3:
//       return getDimensions({ increasePercentage: 1.28, width });
//     case 4:
//       return getDimensions({ increasePercentage: 1.42, width });
//     case 5:
//       return getDimensions({ increasePercentage: 1.56, width });
//     case 6:
//       return getDimensions({ increasePercentage: 1.7, width });
//     case 7:
//       return getDimensions({ increasePercentage: 1.84, width });
//     case 8:
//       return getDimensions({ increasePercentage: 1.98, width });
//     case 9:
//       return getDimensions({ increasePercentage: 2.12, width });
//     case 10:
//       return getDimensions({ increasePercentage: 2.26, width });
//     case 11:
//       return getDimensions({ increasePercentage: 2.4, width });
//     case 'genre':
//       return getDimensions({ increasePercentage: 'genre', width });
//     default:
//       break;
//   }
// };

// export const getCurrentWidth = ({ zoomLevel, width }) => {
//   switch (zoomLevel) {
//     case 1:
//       return getDimensions({ increasePercentage: 1, width });
//     case 2:
//       return getDimensions({ increasePercentage: 1.15, width });
//     case 3:
//       return getDimensions({ increasePercentage: 1.25, width });
//     case 4:
//       return getDimensions({ increasePercentage: 1.56, width });
//     case 5:
//       return getDimensions({ increasePercentage: 2.4, width });
//     case 'genre':
//       return getDimensions({ increasePercentage: 'genre', width });
//     default:
//       break;
//   }
// };
