const switchingAlertState = (event, landscapeAlert) => {
  if (event.matches) {
    landscapeAlert.style.display = 'none';
  } else {
    const width = window.innerHeight;
    if (width < 560) {
      landscapeAlert.style.display = 'flex';
    }
  }
};

export const lockOrientation = () => {
  const isMobile =
    'ontouchstart' in window ||
    navigator.maxTouchPoints > 0 ||
    navigator.msMaxTouchPoints > 0;

  if (isMobile) {
    const portrait = window.matchMedia('(orientation: portrait)');
    const landscapeAlert = document.getElementById('landscape-alert');
    if (landscapeAlert) {
      switchingAlertState(portrait, landscapeAlert);
    }
    portrait.addEventListener('change', (event) => {
      if (landscapeAlert) {
        switchingAlertState(event, landscapeAlert);
      }
    });
  }
};
