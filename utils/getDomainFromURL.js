export const getDomainFromUrl = (fullPath) => {
  try {
    const url = new URL(fullPath);
    return url.origin;
  } catch (error) {
    console.error('Invalid URL:', error);
    return 'https://humandilemmas.com';
  }
};
