import axiosMain from 'axios';
import { logout } from 'store/auth/auth.reducer';
import { api } from './api';

function getCurrentTokenState() {
  const authToken = localStorage.getItem('AuthToken');
  const refreshToken = localStorage.getItem('RefreshToken');
  return { authToken, refreshToken };
}

function refreshToken() {
  const current = getCurrentTokenState();
  return axiosMain.post(
    `${process.env.NEXT_PUBLIC_API_URL}/token/refresh/`,
    {
      refresh: current?.refreshToken,
    },
    {
      headers: {
        'Content-type': 'application/json',
      },
    }
  );
}

const setUpInterceptor = ({ store }) => {
  const handleError = async (error) => {
    return Promise.reject(error);
  };

  api.interceptors.request.use(async (config) => {
    /* your logic here */
    const currentTokenState = getCurrentTokenState();
    const token = currentTokenState?.authToken;
    if (token) {
      config.headers = {
        ...config.headers,
        'Content-type': 'application/json',
        Authorization: `Bearer ${token}`,
      };
    } else {
      config.headers = {
        'Content-type': 'application/json',
        ...config.headers,
      };
    }
    return config;
  }, handleError);

  let refreshing_token = null;
  api.interceptors.response.use(
    async (response) => response,
    async (error) => {
      const config = error.config;
      if (error.response && error.response.status === 401 && !config._retry) {
        config._retry = true;
        try {
          refreshing_token = refreshing_token
            ? refreshing_token
            : refreshToken();
          let res = await refreshing_token;
          refreshing_token = null;
          if (res?.data) {
            localStorage.setItem('AuthToken', res?.data?.access);
            localStorage.setItem('RefreshToken', res?.data?.refresh);
          }
          return api(config);
        } catch (err) {
          store.dispatch(logout());
          return Promise.reject(error);
        }
      } else {
        return Promise.reject(error);
      }
    }
  );
};

export default setUpInterceptor;
