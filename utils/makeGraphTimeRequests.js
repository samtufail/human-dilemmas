import { api } from './api'

export const start = async (data) => {
  try {
    const response = await api.post('/v1/questions/graph-time-start/', data)
    return response.data.id
  } catch (error) {} // Will fail silently , no need to show error to user
}

export const end = async (id, data) => {
  try {
    const response = await api.put(`/v1/questions/graph-time-end/${id}`, data)
    return response.data.id
  } catch (error) {} // Will fail silently , no need to show error to user
}
