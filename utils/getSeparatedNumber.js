import { countryCodes } from './data';

export const getSeparatedNumber = (phone) => {
  const cc = countryCodes?.find((code) => {
    if (phone?.includes(code?.dial_code)) {
      return true;
    }
  })?.dial_code;

  const remainingNumber = phone?.slice(cc?.length);

  return {
    countryCode: cc,
    phoneNumber: remainingNumber,
  };
};
