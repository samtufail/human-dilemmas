import Link from 'next/link';
import { AiOutlineClose } from 'react-icons/ai';
import { useState } from 'react';
import { Field, Form, Formik } from 'formik';
import { useRouter } from 'next/router';
import { FaEyeSlash, FaEye } from 'react-icons/fa';
import { api } from 'utils';
import { toast } from 'react-toastify';

const Back = () => {
  return (
    <svg
      width="12"
      height="20"
      viewBox="0 0 12 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <mask id="path-1-inside-1_3205_5734" fill="white">
        <path d="M12.0005 1.77L10.2305 0L0.230469 10L10.2305 20L12.0005 18.23L3.77047 10L12.0005 1.77Z" />
      </mask>
      <path
        d="M12.0005 1.77L10.2305 0L0.230469 10L10.2305 20L12.0005 18.23L3.77047 10L12.0005 1.77Z"
        fill="white"
      />
      <path
        d="M12.0005 1.77L15.536 5.30553L19.0715 1.77L15.536 -1.76553L12.0005 1.77ZM10.2305 0L13.766 -3.53553L10.2305 -7.07107L6.69493 -3.53553L10.2305 0ZM0.230469 10L-3.30507 6.46447L-6.8406 10L-3.30507 13.5355L0.230469 10ZM10.2305 20L6.69493 23.5355L10.2305 27.0711L13.766 23.5355L10.2305 20ZM12.0005 18.23L15.536 21.7655L19.0715 18.23L15.536 14.6945L12.0005 18.23ZM3.77047 10L0.234935 6.46447L-3.3006 10L0.234935 13.5355L3.77047 10ZM15.536 -1.76553L13.766 -3.53553L6.69493 3.53553L8.46494 5.30553L15.536 -1.76553ZM6.69493 -3.53553L-3.30507 6.46447L3.766 13.5355L13.766 3.53553L6.69493 -3.53553ZM-3.30507 13.5355L6.69493 23.5355L13.766 16.4645L3.766 6.46447L-3.30507 13.5355ZM13.766 23.5355L15.536 21.7655L8.46493 14.6945L6.69493 16.4645L13.766 23.5355ZM15.536 14.6945L7.306 6.46447L0.234935 13.5355L8.46493 21.7655L15.536 14.6945ZM7.306 13.5355L15.536 5.30553L8.46493 -1.76553L0.234935 6.46447L7.306 13.5355Z"
        fill="#ffffff"
        mask="url(#path-1-inside-1_3205_5734)"
      />
    </svg>
  );
};

export const NewPassword = ({ setActive }) => {
  const [label1, setLabel1] = useState(false);
  const [pass, setPass] = useState(false);

  const router = useRouter();
  const { from } = router.query;

  return (
    <div className=" flex min-h-full flex-col justify-center py-12 sm:px-6 lg:px-8">
      <div className="bg-tabbg rounded-2xl mt-8 p-[20px] max-w-[547px] mx-auto w-full ">
        <div className="flex justify-between items-center mb-[54px]">
          <div className="flex items-center gap-[20px]">
            <div
              className="cursor-pointer flex items-center"
              onClick={() => {
                localStorage.removeItem('code');
                setActive('verification');
              }}
            >
              <Back />
            </div>

            <h2 className="text-white font-normal text-[24px] leading-[42px]">
              New Password
            </h2>
          </div>
          <Link href="/" passHref>
            <div
              className="flex justify-end text-3xl cursor-pointer"
              onClick={() => {
                localStorage.removeItem('phoneNumber');
                localStorage.removeItem('code');
                router.push('/');
              }}
            >
              <AiOutlineClose />
            </div>
          </Link>
        </div>

        <Formik
          initialValues={{ password: '' }}
          onSubmit={async (values) => {
            const phone = localStorage.getItem('phoneNumber');
            const code = localStorage.getItem('code');
            try {
              await api.post('/v1/user/reset-password/', {
                phone_number: phone,
                alphanumeric_code: code,
                password: values.password,
              });
              toast.success('Your password is reset successfully!');
              if (from === 'login') {
                router.push('/sign-in');
              } else if (from === 'my-profile') {
                router.push('/my-profile');
              }
            } catch (e) {
              toast.error(
                'The code you entered is not correct, please go back and try again.'
              );
            }
          }}
        >
          <Form>
            <div>
              <div className="relative">
                <label
                  htmlFor="password"
                  className={` font-medium text-[10px] pt-[5px]  text-textGray absolute flex pl-[15px]`}
                >
                  {label1 ? 'Password' : ''}
                </label>
                <Field
                  id="password"
                  name="password"
                  type={pass ? 'text' : 'password'}
                  placeholder="Enter Password"
                  autoComplete="current-password"
                  onBlur={() => setLabel1(false)}
                  onFocus={() => setLabel1(true)}
                  required
                  className={` ${
                    label1 ? 'pt-[24px]' : ''
                  } placeholder:text-textGray border-white focus:border-boxtext  border-2 focus:shadow-inputShadow  h-[52px] text-dark bg-white block w-full appearance-none rounded-md  px-3 py-4 placeholder-textGray shadow-sm  focus:outline-none  sm:text-sm`}
                />
                <button
                  className="border-0 bottom-0 m-auto h-max absolute top-0 right-1 text-dark text-lg bg-transparent inline-flex"
                  onClick={() => setPass(!pass)}
                  type="button"
                >
                  {pass ? <FaEye /> : <FaEyeSlash />}
                </button>
              </div>
            </div>

            <div className="flex items-center justify-end">
              <button
                type="submit"
                className="bg-boxtext cursor-pointer text-white placeholder:font-normal mb-[34px] w-[152px] h-[48px] leading-7 capitalize border-0 items-center flex justify-center rounded-[16px] font-[500] text-lg mt-[50px]"
              >
                Save
              </button>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  );
};
