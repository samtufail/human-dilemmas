import Link from 'next/link';
import { AiOutlineClose } from 'react-icons/ai';
import { api, countryCodes } from 'utils';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { CustomPhoneInput } from 'components';
import { phone } from 'phone';

export const NewPhoneInput = ({ setActive }) => {
  const [label2, setLabel2] = useState(false);
  const { countryCode } = useSelector((state) => state?.global);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [error, setError] = useState('');

  const submitFn = async () => {
    const fullNumber = `${countryCode}${phoneNumber}`.trim();
    const result = phone(fullNumber, {
      country: countryCodes?.find((code) => code?.dial_code === countryCode)
        ?.code,
    });
    if (!result?.isValid) {
      if (!phoneNumber) {
        setError('Error: Invalid phone number.');
      } else {
        setError(`Error: "${phoneNumber}" is not a valid phone number.`);
      }
    } else {
      try {
        await api.post('/v1/user/forgot-password/', {
          phone_number: fullNumber,
        });
        localStorage.setItem('phoneNumber', fullNumber);
        setActive('verification');
      } catch (error) {
        localStorage.removeItem('phoneNumber');
        if (error?.response?.data?.non_field_errors?.length) {
          setError(error?.response?.data?.non_field_errors[0]);
        } else {
          if (error) {
            const errorMsg = error.response?.data?.detail || '';
            // console.log(errorMsg);
            if (errorMsg?.startsWith('Request was throttled')) {
              const seconds = Number(errorMsg?.match(/\d+/)[0]);
              const formatted = moment.utc(seconds * 1000).format('HH:mm:ss');
              setError(
                `Error: Too many attempts, Please try again after ~${formatted}`
              );
            }
          }
        }
      }
    }
  };

  return (
    <div className="flex min-h-full flex-col justify-center px-4 mt-[18px] md:mt-[81px]">
      <div className="bg-tabbg rounded-2xl px-[16px] pt-[16px] md:p-[20px] md:max-w-[547px] mx-auto w-full mb-[18px] md:mb-[81px]">
        <div className="flex justify-between items-center mb-[29px] md:mb-[54px]">
          <h2 className="text-white font-normal text-[22px] md:text-[24px] leading-[33px] md:leading-[42px]">
            Forgot your password?
          </h2>
          <Link href="/" passHref>
            <div className="flex justify-end text-[28px] md:text-3xl cursor-pointer">
              <AiOutlineClose />
            </div>
          </Link>
        </div>
        <CustomPhoneInput
          phoneNumber={phoneNumber}
          setPhoneNumber={setPhoneNumber}
          showLabel={label2}
          setShowLabel={setLabel2}
          phoneInputClassName={'phone-input forgot-password-gap'}
          phoneFlagClassName={'phone-flag'}
        />
        {/* Error */}
        <div className="mb-6 mt-[12px] flex items-center justify-end">
          <p className="text-[13px]">{error ? error : <>&nbsp;</>}</p>
        </div>
        <div className="flex justify-end items-center mt-[9px] md:mt-8">
          <button
            type="button"
            onClick={submitFn}
            className="bg-boxtext cursor-pointer text-white placeholder:font-normal mb-[32px] md:mb-[34px] md:max-w-[266px] w-full h-[48px] leading-7 capitalize border-0 items-center flex justify-center rounded-[16px] font-[500] text-[16px] md:text-lg"
          >
            Request password reset
          </button>
        </div>
      </div>
    </div>
  );
};
