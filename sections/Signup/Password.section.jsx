import Link from 'next/link';
import { AiOutlineClose } from 'react-icons/ai';
import { useState } from 'react';
import { Field, Form, Formik } from 'formik';
import { useRouter } from 'next/router';
import { api, setPassword } from 'utils';
import { toast } from 'react-toastify';
import { useDispatch } from 'react-redux';
import { setAuthToken } from 'store';

export const Password = () => {
  const [label1, setLabel1] = useState(false);
  const [pass, setPass] = useState(false);
  const dispatch = useDispatch();

  const router = useRouter();

  return (
    <div className="flex min-h-full flex-col justify-center px-4">
      <div className="bg-tabbg rounded-2xl mt-8 p-4 pb-[24px] sm:p-[20px] max-w-[343px] sm:max-w-[542px] mx-auto w-full ">
        <div className="flex justify-between items-center mb-[29px] sm:mb-[54px]">
          <h2 className="text-white font-medium sm:font-normal text-[22px] sm:text-[24px] leading-[33px] sm:leading-[42px]">
            New Password
          </h2>
          <Link href="/" passHref>
            <div
              className="flex justify-end text-[28px] sm:text-3xl cursor-pointer"
              onClick={() => {
                localStorage.removeItem('phoneNumber');
                router.push('/');
              }}
            >
              <AiOutlineClose />
            </div>
          </Link>
        </div>

        <Formik
          initialValues={{ password: '' }}
          onSubmit={async (values) => {
            const phone = localStorage.getItem('phoneNumber');
            const AuthToken = localStorage.getItem('AuthToken');
            try {
              const res = await api.put(
                setPassword,
                {
                  phone_number: phone,
                  password: values.password,
                },
                {
                  headers: {
                    Authorization: `Bearer ${AuthToken}`,
                  },
                }
              );
              dispatch(setAuthToken(res?.data?.token));
              toast.success('Your profile is created successfully!');
            } catch (e) {
              toast.error('Something went wrong, please try again later.');
              localStorage.removeItem('phoneNumber');
              localStorage.removeItem('AuthToken');
              localStorage.removeItem('RefreshToken');
            } finally {
              localStorage.removeItem('phoneNumber');
              const surveyId = localStorage?.getItem('surveyId');
              const activeQuestion = localStorage?.getItem('activeQuestion');
              if (surveyId && activeQuestion) {
                router.push(`/questions/${surveyId}?backFromRegister=true`);
                // router.push(`/surveys/${surveyId}?backFromRegister=true`);
              } else {
                router.push('/');
              }
            }
          }}
        >
          <Form>
            <div>
              <div className="relative">
                <label
                  htmlFor="password"
                  className={` font-medium text-[10px] pt-[5px]  text-textGray absolute flex pl-[15px]`}
                >
                  {label1 ? 'Password' : ''}
                </label>
                <Field
                  id="password"
                  name="password"
                  type={pass ? 'text' : 'password'}
                  placeholder="Enter Password"
                  autoComplete="current-password"
                  onBlur={() => setLabel1(false)}
                  onFocus={() => setLabel1(true)}
                  required
                  className={` ${
                    label1 ? 'pt-[24px]' : ''
                  } placeholder:text-textGray border-white focus:border-boxtext  border-2 focus:shadow-inputShadow  h-[52px] text-dark bg-white block w-full appearance-none rounded-md px-3 py-4 placeholder-textGray shadow-sm focus:outline-none sm:text-sm font-normal`}
                />
                <button
                  className="border-0 bottom-0 cursor-pointer m-auto h-max absolute top-[4px] right-[15px] text-[#845EC2] font-normal text-[13px] sm:text-[15px] leading-[20px] sm:leading-[22px] bg-transparent inline-flex"
                  onClick={() => setPass(!pass)}
                  type="button"
                >
                  {pass ? 'Hide' : 'Show'}
                </button>
              </div>
            </div>

            <div className="flex items-center justify-end">
              <button
                type="submit"
                className="bg-boxtext cursor-pointer text-white placeholder:font-normal sm:mb-[34px] w-full sm:w-[152px] h-[48px] leading-7 capitalize border-0 items-center flex justify-center rounded-[16px] font-semibold sm:font-[500] text-[16px] sm:text-lg mt-[30px] sm:mt-[50px]"
              >
                Save
              </button>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  );
};
