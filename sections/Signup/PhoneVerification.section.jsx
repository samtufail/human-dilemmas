import { Field, Form, Formik } from 'formik';
import Link from 'next/link';
import { useState } from 'react';
import Countdown from 'react-countdown';
import { toast } from 'react-toastify';
import { api, resendOtp, verifyUser } from 'utils';

const Back = () => {
  return (
    <svg
      width="12"
      height="20"
      viewBox="0 0 12 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <mask id="path-1-inside-1_3205_5734" fill="white">
        <path d="M12.0005 1.77L10.2305 0L0.230469 10L10.2305 20L12.0005 18.23L3.77047 10L12.0005 1.77Z" />
      </mask>
      <path
        d="M12.0005 1.77L10.2305 0L0.230469 10L10.2305 20L12.0005 18.23L3.77047 10L12.0005 1.77Z"
        fill="white"
      />
      <path
        d="M12.0005 1.77L15.536 5.30553L19.0715 1.77L15.536 -1.76553L12.0005 1.77ZM10.2305 0L13.766 -3.53553L10.2305 -7.07107L6.69493 -3.53553L10.2305 0ZM0.230469 10L-3.30507 6.46447L-6.8406 10L-3.30507 13.5355L0.230469 10ZM10.2305 20L6.69493 23.5355L10.2305 27.0711L13.766 23.5355L10.2305 20ZM12.0005 18.23L15.536 21.7655L19.0715 18.23L15.536 14.6945L12.0005 18.23ZM3.77047 10L0.234935 6.46447L-3.3006 10L0.234935 13.5355L3.77047 10ZM15.536 -1.76553L13.766 -3.53553L6.69493 3.53553L8.46494 5.30553L15.536 -1.76553ZM6.69493 -3.53553L-3.30507 6.46447L3.766 13.5355L13.766 3.53553L6.69493 -3.53553ZM-3.30507 13.5355L6.69493 23.5355L13.766 16.4645L3.766 6.46447L-3.30507 13.5355ZM13.766 23.5355L15.536 21.7655L8.46493 14.6945L6.69493 16.4645L13.766 23.5355ZM15.536 14.6945L7.306 6.46447L0.234935 13.5355L8.46493 21.7655L15.536 14.6945ZM7.306 13.5355L15.536 5.30553L8.46493 -1.76553L0.234935 6.46447L7.306 13.5355Z"
        fill="#010202"
        mask="url(#path-1-inside-1_3205_5734)"
      />
    </svg>
  );
};

export const PhoneVerification = ({ setActive }) => {
  const [error, setError] = useState(false);
  const [countdownKey, setCountDownKey] = useState(41547);
  const [tries, setTries] = useState(0);

  // Renderer callback with condition
  const renderer = ({ minutes, seconds, completed }) => {
    if (completed) {
      // Render a completed state
      return (
        <>
          <p
            className="text-[#010202] cursor-pointer underline text-[15px] font-[300]"
            onClick={async () => {
              const phone = localStorage.getItem('phoneNumber');
              try {
                await api.post(resendOtp, { phone_number: phone });
                toast.success('We sent you a new SMS with a code.');
                setTries((prev) => prev + 1);
                if (error) {
                  setError('');
                }
              } catch (e) {
                setTries(2);
                setError('Operation failed, please go back and try again.');
              } finally {
                setCountDownKey((key) => key + 547);
              }
            }}
          >
            I didn&apos;t receive the code
          </p>
        </>
      );
    } else {
      // Render a countdown
      return (
        <span className="text-[#888493] text-[18px] font-[300] leading-[1]">
          {minutes}:{seconds < 10 ? `0${seconds}` : seconds}
        </span>
      );
    }
  };

  return (
    <div className="flex flex-col px-4 sm:px-[0px] min-h-full relative">
      <div className="bg-[#E6DFF3] rounded-[8px] py-[32px] px-[16px] sm:px-[65px] max-w-[343px] sm:max-w-[542px] mx-auto w-full">
        <div className="flex flex-col align-center justify-start">
          <div className="flex justify-center items-center pr-[17px]">
            {/* Arrow */}
            <div className="mr-[17px] pt-[5px] sm:pt-[3px]">
              <div
                className="cursor-pointer"
                onClick={() => {
                  setActive('phone');
                  localStorage.removeItem('phoneNumber');
                }}
              >
                <Back />
              </div>
            </div>
            <h2 className="text-[18px] xxs:text-[22px] sm:text-[24px] text-dark leading-[33px] sm:leading-[0.85] font-normal sm:font-[300]">
              Phone Verification
            </h2>
          </div>
          <>
            {error ? (
              <p className="mt-[15px] text-[#6E7378] text-center sm:text-start text-[15px] sm:text-[15px] font-[300] mb-[33px]">
                {error}
              </p>
            ) : (
              <p className="mt-[16px] sm:mt-[15px] text-[#6E7378] text-center sm:text-start text-[15px] sm:text-[16px] font-normal sm:font-[300] mb-[24px] sm:mb-[25px] leading-[21px] sm:leading-6">
                We just sent you an SMS with a confirmation code.
              </p>
            )}
          </>
          {/* Other Content */}
          <div>
            <Formik
              initialValues={{ code: '' }}
              onSubmit={async (values) => {
                const phone = localStorage.getItem('phoneNumber');
                const verifyBody = {
                  phone_number: phone,
                  otp: values?.code,
                };
                try {
                  const res = await api.post(verifyUser, verifyBody);
                  localStorage.setItem('AuthToken', res?.data?.token?.access);
                  localStorage.setItem(
                    'RefreshToken',
                    res?.data?.token?.refresh
                  );
                  setActive('password');
                } catch (e) {
                  setError(
                    'The code you entered is incorrect — please try again.'
                  );
                }
              }}
            >
              <Form>
                <Field
                  name="code"
                  type="text"
                  placeholder="Enter verification code"
                  className="outline-none appearance-none bg-white border-none rounded-[4px] w-full h-[42px] sm:h-[52px] px-[12px] text-dark placeholder:text-[#6E7378]"
                />
                <div className="mt-[30px] flex flex-col sm:flex-row items-center justify-between">
                  <button
                    type="submit"
                    className="bg-boxtext cursor-pointer text-white h-[42px] sm:h-[48px] sm:w-[136px] leading-7 capitalize border-0 items-center flex justify-center rounded-[16px] font-semibold sm:font-[500] text-[16px] sm:text-lg mx-auto w-full mb-[24px] sm:m-0"
                  >
                    Next
                  </button>
                  {tries >= 2 ? (
                    <p className="text-[#888493] cursor-pointer underline text-[14px] sm:text-[15px] font-normal sm:font-[300] pointer-events-none leading-5">
                      I didn&apos;t receive the code
                    </p>
                  ) : (
                    <Countdown
                      date={Date.now() + 35000}
                      key={countdownKey}
                      renderer={renderer}
                    />
                  )}
                </div>
              </Form>
            </Formik>
          </div>
        </div>
      </div>
      {tries >= 2 ? (
        <div className="relative text-white bottom-[-5px] text-right w-[547px] mx-auto">
          Didn&apos;t work?{' '}
          <Link href="/contact-us" passHref legacyBehavior>
            <a className="cursor-pointer text-white hover:text-white inline underline hover:underline">
              Contact us here.
            </a>
          </Link>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};
