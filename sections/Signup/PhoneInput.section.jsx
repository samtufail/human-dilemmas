import Link from 'next/link';
import { AiOutlineClose } from 'react-icons/ai';
import { api, countryCodes, createUser } from 'utils';
import { useSelector } from 'react-redux';
import { useState } from 'react';
import moment from 'moment';
import { phone } from 'phone';

import { CustomPhoneInput } from 'components/PhoneInput/PhoneInput.component';

export const PhoneInput = ({ setActive }) => {
  const [label2, setLabel2] = useState(false);
  const { countryCode } = useSelector((state) => state?.global);

  const [phoneNumber, setPhoneNumber] = useState('');
  const [error, setError] = useState('');

  const submitFn = async () => {
    if (!countryCode?.trim() || !phoneNumber?.trim()) {
      setError('Error: Invalid phone number.');
    } else {
      const fullNumber = `${countryCode}${phoneNumber}`.trim();
      const result = phone(fullNumber, {
        country: countryCodes?.find((code) => code?.dial_code === countryCode)
          ?.code,
      });
      if (!result?.isValid) {
        setError(`Error: "${phoneNumber}" is not a valid phone number.`);
      } else {
        try {
          await api.post(createUser, { phone_number: fullNumber });
          localStorage.setItem('phoneNumber', fullNumber);
          setActive('verification');
        } catch (error) {
          localStorage.removeItem('phoneNumber');
          if (error?.response?.data?.non_field_errors?.length) {
            setError(error?.response?.data?.non_field_errors[0]);
          } else {
            if (error) {
              const errorMsg = error.response?.data?.detail || '';
              // console.log(errorMsg);
              if (errorMsg?.startsWith('Request was throttled')) {
                const seconds = Number(errorMsg?.match(/\d+/)[0]);
                const formatted = moment.utc(seconds * 1000).format('HH:mm:ss');
                setError(
                  `Error: Too many attempts, Please try again after ~${formatted}`
                );
              }
            }
          }
        }
      }
    }
  };

  return (
    <div className="flex flex-col justify-center px-4 sm:px-0 mx-auto min-h-full">
      <div className="bg-tabbg rounded-2xl p-[16px] pb-[24px] sm:pt-[25px] sm:px-[32px] sm:pb-[58px] max-w-[343px] sm:max-w-[547px] mx-auto w-full">
        <div className="flex justify-between items-center mb-[29px] sm:mb-[54px]">
          <h2 className="text-white font-normal text-[22px] sm:text-[24px] leading-[33px] sm:leading-[42px]">
            Create Profile
          </h2>
          <Link href="/" passHref>
            <div className="flex justify-end text-[28px] sm:text-3xl cursor-pointer">
              <AiOutlineClose />
            </div>
          </Link>
        </div>
        <CustomPhoneInput
          phoneNumber={phoneNumber}
          setPhoneNumber={setPhoneNumber}
          showLabel={label2}
          setShowLabel={setLabel2}
          // countryCodeWidth={180}
          phoneInputClassName="phone-input create-profile-gap"
          phoneFlagClassName="phone-flag"
          dropdownWidthClassName="dropdown-width"
        />
        {/* Error */}
        <div className="mb-[15px] sm:mb-6 mt-[5px] sm:mt-[12px] flex items-center justify-end">
          <p className="text-[9px] xxs:text-[12px] sm:text-[12px] font-normal leading-4">
            {error ? error : <>&nbsp;</>}
          </p>
        </div>
        <div className="flex justify-center items-center flex-col">
          <p className="text-white text-[10px] xxs:text-[13px] sm:text-[14px] font-light leading-5 sm:font-[300] mb-[16px] sm:mb-[8px]">
            You will receive an activation code by text
          </p>
          <button
            type="button"
            onClick={submitFn}
            className="bg-boxtext cursor-pointer text-white placeholder:font-normal sm:max-w-[266px] w-full h-[42px] sm:h-[48px] capitalize border-0 items-center flex justify-center rounded-[16px] font-semibold sm:font-[500] text-[16px] sm:text-lg leading-7"
          >
            Send activation code
          </button>
        </div>
      </div>
      <p className="text-white font-normal sm:font-[300] text-[12px] xxs:text-[13px] sm:text-[15px] leading-5 mt-[16px] sm:mt-[8px] max-w-[343px] sm:max-w-[547px] mx-auto w-full text-right">
        Already have an account? Please{' '}
        <Link href="/sign-in">
          <span className="underline font-bold sm:font-semibold cursor-pointer">
            Log in
          </span>
        </Link>
      </p>
    </div>
  );
};
