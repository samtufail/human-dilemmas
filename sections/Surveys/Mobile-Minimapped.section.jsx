import { useState, useEffect } from 'react';
import { Brick } from './Brick.section';
import { TransformWrapper, TransformComponent } from 'react-zoom-pan-pinch';

export const MobileMinimapped = ({
  zoomLevel,
  allDimensions,
  surveys,
  isMoreThanLargeMobile,
}) => {
  const [localSurveys, setLocalSurveys] = useState([]);
  useEffect(() => {
    setLocalSurveys(surveys);
  }, []);

  return (
    <>
      {!localSurveys?.results?.length ? (
        <></>
      ) : (
        <TransformWrapper>
          <TransformComponent
            options={{ wheel: { disabled: true } }}
            wrapperStyle={{
              width: '100%',
              maxHeight: `calc(100vh - ${
                isMoreThanLargeMobile ? '175px' : '0px'
              })`,
              minHeight: `calc(100vh - ${
                isMoreThanLargeMobile ? '175px' : '0px'
              })`,
            }}
          >
            <div
              className={`px-[20px] pt-[150px] content grid grid-cols-[repeat(21,1fr)]`}
              style={{
                width: `${Math.round(allDimensions?.totalWidth) - 20}px`,
                gap: Math.round(allDimensions?.gap),
              }}
            >
              {localSurveys?.results?.map((survey) => {
                const { size } = survey;
                const type =
                  size === 'Small'
                    ? 'oneThird'
                    : size === 'Medium'
                    ? 'twoThird'
                    : 'full';
                return (
                  <Brick
                    key={survey?.gid + Math.random()}
                    survey={survey}
                    zoomLevel={zoomLevel}
                    allDimensions={allDimensions}
                    type={type}
                    closed={survey?.closed_at}
                    brick={survey?.brick}
                  />
                );
              })}
              <div
                className="bg-black flex text-white relative mt-[40px] pb-[20px] tab-card-main"
                style={{ gridColumn: 'span 21 / span 21' }}
              >
                <div
                  className={`sticky left-[50vw] h-[52px] items-center cursor-pointer px-[20px] py-[10px] rounded-[8px] ${
                    localSurveys?.results?.length > 93 ? 'flex' : 'hidden'
                  }`}
                  style={{
                    transform: 'translate(-50%, -40%)',
                    border: '1px solid rgba(255, 255, 255, 0.8)',
                    color: 'rgba(255, 255, 255, 0.8)',
                  }}
                >
                  Load More
                </div>
              </div>
            </div>
          </TransformComponent>
        </TransformWrapper>
      )}
    </>
  );
};
