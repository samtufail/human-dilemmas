import { useState, useEffect } from 'react';
import { useWindowDimensions } from 'hooks';
import { getCurrentWidth } from 'utils/getDimensions';
import dynamic from 'next/dynamic';
import { Loader } from 'components';

const DynamicMiniMapped = dynamic(
  () =>
    import('./Mobile-Minimapped.section').then(
      (module) => module.MobileMinimapped
    ),
  { ssr: false }
);

export const MobileBrickWall = ({
  zoomLevel,
  releaseSurveys,
  isMoreThanLargeMobile,
  isLessThanMobile,
}) => {
  const [allDimensions, setAllDimensions] = useState(undefined);
  const [surveys, setSurveys] = useState({
    count: 0,
    next: null,
    previous: null,
    results: [],
  });
  const [loading, setLoading] = useState(false);

  const dimensions = useWindowDimensions();
  useEffect(() => {
    const allDimensions = getCurrentWidth({
      zoomLevel,
      width: dimensions?.width,
      isMoreThanLargeMobile,
      isLessThanMobile,
    });
    setAllDimensions(allDimensions);
  }, [dimensions, zoomLevel]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      setSurveys(releaseSurveys);
      setLoading(false);
    })();
  }, []);

  return (
    <>
      {allDimensions !== undefined && !loading ? (
        <>
          <DynamicMiniMapped
            zoomLevel={zoomLevel}
            allDimensions={allDimensions}
            surveys={surveys}
            isMoreThanLargeMobile={isMoreThanLargeMobile}
            isLessThanMobile={isLessThanMobile}
          />
        </>
      ) : (
        <div className="flex items-center justify-center h-[100vh]">
          <Loader />
        </div>
      )}
    </>
  );
};
