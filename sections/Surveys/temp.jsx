import { Brick } from './Brick.section';

export const Three = ({ zoomLevel, allDimensions }) => (
  <>
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick
      type="full"
      closed
      zoomLevel={zoomLevel}
      allDimensions={allDimensions}
    />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />

    <Brick
      type="oneThird"
      zoomLevel={zoomLevel}
      allDimensions={allDimensions}
    />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick
      type="twoThird"
      zoomLevel={zoomLevel}
      allDimensions={allDimensions}
    />

    <Brick
      type="twoThird"
      zoomLevel={zoomLevel}
      allDimensions={allDimensions}
    />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick
      type="oneThird"
      zoomLevel={zoomLevel}
      allDimensions={allDimensions}
    />
  </>
);

export const ThreeGenre = ({ zoomLevel, allDimensions }) => (
  <>
    <Brick
      type="twoThird"
      zoomLevel={zoomLevel}
      allDimensions={allDimensions}
    />
    <Brick
      type="full"
      closed
      zoomLevel={zoomLevel}
      allDimensions={allDimensions}
    />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />

    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick
      type="oneThird"
      zoomLevel={zoomLevel}
      allDimensions={allDimensions}
    />

    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
    <Brick type="full" zoomLevel={zoomLevel} allDimensions={allDimensions} />
  </>
);
