import { useState, useEffect } from 'react';
import { useWindowDimensions } from 'hooks';
// import ScrollBooster from 'scrollbooster';
import 'react-minimap/dist/react-minimap.css';
import { getCurrentWidth } from 'utils/getDimensions';
import { Brick } from './Brick.section';

export const ZoomedIn = ({
  allDimensions,
  activeSurveys,
  isMoreThanLargeMobile,
  isMoreThanMobile,
}) => {
  return (
    <>
      <div>
        <h1 className="mdbw:mt-[48px] mdbw:mb-[32px] text-[40px] mdbw:px-[40px] mt-[40px] px-[25px] text-[#8A8B8B]">
          {activeSurveys?.genre_name}
        </h1>
        <div
          className={`mdbw:px-[40px] mdbw:mt-[42px] content mdbw:pb-[40px] px-[25px] mt-[20px] grid grid-cols-8`}
          style={{
            width: allDimensions.totalWidth,
            gap: Math.round(allDimensions?.gap),
          }}
        >
          {activeSurveys?.surveys?.map((survey) => {
            const { size } = survey;
            const type =
              size === 'Small'
                ? 'oneThird'
                : size === 'Medium'
                ? 'twoThird'
                : 'full';
            return (
              <Brick
                key={survey?.gid + Math.random()}
                survey={survey}
                zoomLevel={'genre'}
                allDimensions={allDimensions}
                type={type}
                closed={survey?.closed_at}
                brick={survey.brick}
              />
            );
          })}
        </div>
      </div>
    </>
  );
};

export const Genre = ({
  activeSurveys,
  isMoreThanLargeMobile,
  isLessThanMobile,
}) => {
  const [allDimensions, setAllDimensions] = useState(undefined);

  const dimensions = useWindowDimensions();
  useEffect(() => {
    const allDimensions = getCurrentWidth({
      zoomLevel: 'genre',
      width: dimensions?.width,
      isMoreThanLargeMobile,
      isLessThanMobile,
    });
    setAllDimensions(allDimensions);
  }, [dimensions]);

  return (
    <>
      {allDimensions !== undefined ? (
        <>
          <ZoomedIn
            zoomLevel={5}
            isMoreThanLargeMobile={isMoreThanLargeMobile}
            isLessThanMobile={isLessThanMobile}
            allDimensions={allDimensions}
            activeSurveys={activeSurveys}
          />
        </>
      ) : (
        <></>
      )}
    </>
  );
};
