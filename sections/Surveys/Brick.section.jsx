import moment from 'moment';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import Countdown from 'react-countdown';
import { randomBackgroundPosition } from 'utils';
import { useDispatch } from 'react-redux';
import { setFirstSurveyGID } from 'store/surveys';

export const formatDate = (date) => {
  const superSc = moment(date).format('Do');
  const superScript = superSc.slice(-2);
  return {
    superScript,
    day: moment(date).format('DD'),
    monthYear: moment(date).format('MMM YYYY'),
  };
};

export const DateComponent = ({ date }) => {
  return (
    <>
      {date?.day}
      <sup>{date?.superScript}</sup>&nbsp;{date?.monthYear}
    </>
  );
};

export const Brick = ({
  type = 'oneThird',
  survey,
  closed,
  allDimensions,
  zoomLevel,
  brick,
}) => {
  const dispatch = useDispatch();
  const {
    minHeight,
    // oneThirdCard,
    // twoThirdCard,
    // widthOfCard,
    cardPadding,
    borderRadiusOfCard,
    headlineImageHeight,
    viewResultsTextSize,
    coverImageWidth,
    onlineSinceTextSize,
  } = allDimensions;

  const heightWidth =
    type === 'oneThird'
      ? {
          gridColumn: 'span 1 / span 1',
          height: `${minHeight}px`,
        }
      : type === 'twoThird'
      ? {
          gridColumn: 'span 2 / span 2',
          height: `${minHeight}px`,
        }
      : {
          gridColumn: 'span 3 / span 3',
          height: `${minHeight}px`,
        };

  const router = useRouter();

  let styles;
  if (zoomLevel !== 'genre' && type === 'oneThird') {
    styles = {
      position: 'absolute',
      top: '1%',
      right: '4%',
      textAlign: 'right',
      fontSize: viewResultsTextSize * 0.6,
    };
  } else if (zoomLevel !== 'genre' && type !== 'oneThird') {
    styles = {
      zIndex: 0,
      top: '3%',
      right: '4%',
      fontSize: viewResultsTextSize * 0.6,
    };
  } else {
    styles = {
      pointerEvents: 'all',
      fontSize: viewResultsTextSize,
      textAlign: 'right',
      top: '3%',
      right: '4%',
    };
  }

  // set release relevant properties
  const [isFutureSurvey, setIsFutureSurvey] = useState(false);
  useEffect(() => {
    const isFutureSurvey = moment().isBefore(moment(survey?.date_published));
    setIsFutureSurvey(isFutureSurvey);
  }, []);

  // Renderer callback with condition
  const renderer = ({ days, hours, minutes, seconds, completed }) => {
    if (completed) {
      setIsFutureSurvey(false);
      // Render a completed state
      return (
        <>
          {survey?.closed_at ? 'Closed' : 'Online since'} <br />{' '}
          {survey?.closed_at ? (
            <DateComponent date={formatDate(survey?.closed_at)} />
          ) : (
            <DateComponent
              key={345}
              date={formatDate(survey?.date_published)}
            />
          )}
        </>
      );
    } else if (days > 0) {
      return (
        <>
          {survey?.closed_at ? 'Closed' : 'Available at'} <br />{' '}
          {survey?.closed_at ? (
            <DateComponent date={formatDate(survey?.closed_at)} />
          ) : (
            <DateComponent
              key={345}
              date={formatDate(survey?.date_published)}
            />
          )}
        </>
      );
    } else {
      // Render a countdown
      return (
        <>
          {survey?.closed_at ? 'Closed' : 'Available in'} <br />{' '}
          {survey?.closed_at ? (
            <DateComponent date={formatDate(survey?.closed_at)} />
          ) : (
            <>
              {hours < 10 ? `0${hours}` : hours}:
              {minutes < 10 ? `0${minutes}` : minutes}:
              {seconds < 10 ? `0${seconds}` : seconds}
            </>
          )}
        </>
      );
    }
  };

  return (
    <>
      {/* {widthOfCard === undefined || oneThirdCard === undefined || twoThirdCard === undefined ? (
        <></>
      ) : ( */}
      <div
        className={`tab-card-main relative cursor-pointer bg-black-survey ${
          brick === 0
            ? 'hover:bg-[#262440] active:bg-[#574081]'
            : 'hover:bg-[#000] active:bg-[#574081] bg-[#000]'
        }  flex flex-col justify-between ${closed ? 'bg-[#262D35]' : ''} ${
          isFutureSurvey
            ? 'bg-[#262D35] cursor-not-allowed hover:bg-[#262D35] active:bg-[#262D35]'
            : ''
        }`}
        onClick={() => {
          if (isFutureSurvey) {
          } else if (survey.linked_video_question_id) {
            router.push(`/dilemmas/${survey?.linked_video_question_id}`);
            dispatch(setFirstSurveyGID(survey?.gid));
          } else {
            // Route to the default survey route
            router.push(`/${survey?.name}/${survey?.gid}`);
          }
        }}
        style={{
          ...heightWidth,
          transition: 'all 0.2s ease-in',
          userSelect: 'none',
          padding: cardPadding,
          borderRadius: borderRadiusOfCard,
          backgroundImage: `url(img/bricks/${brick}.webp)`,
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: randomBackgroundPosition(survey.size),
        }}
      >
        {closed ? (
          <div
            className="text-[#845EC2] absolute cursor-pointer"
            onClick={() => {
              if (survey.linked_video_question_id) {
                router.push(`/dilemmas/${survey?.linked_video_question_id}`);
                dispatch(setFirstSurveyGID(survey?.gid));
              } else {
                // Route to the default survey route
                router.push(`/${survey?.name}/${survey?.gid}`);
              }
            }}
            style={styles}
          >
            View results
          </div>
        ) : (
          <></>
        )}
        <div className="flex items-center justify-start w-[100%]">
          <img
            className="text-img"
            src={survey?.cover_headline}
            style={{ height: `${headlineImageHeight}px`, objectFit: 'contain' }}
            height={headlineImageHeight}
            alt="lies-text"
          />
        </div>
        <div
          className="flex items-center justify-center absolute left-[50%] top-[50%]"
          style={{ transform: 'translate(-50%, -50%)' }}
        >
          <img
            className="object-contain"
            style={{ width: coverImageWidth }}
            src={survey?.cover_image}
            alt="lies-text"
          />
        </div>
        <div className="flex justify-end">
          <div
            className="text-[#b4b4b4] text-right"
            style={{ fontSize: onlineSinceTextSize }}
          >
            <>
              {isFutureSurvey ? (
                <Countdown
                  date={survey?.date_published}
                  key={456}
                  renderer={renderer}
                />
              ) : (
                <>
                  {survey?.closed_at ? 'Closed' : 'Online since'} <br />{' '}
                  {survey?.closed_at ? (
                    <DateComponent date={formatDate(survey?.closed_at)} />
                  ) : (
                    <DateComponent date={formatDate(survey?.date_published)} />
                  )}
                </>
              )}
            </>
          </div>
        </div>
      </div>
      {/* )} */}
    </>
  );
};
