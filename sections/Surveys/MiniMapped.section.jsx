import { useEffect, useState } from 'react';
import Minimap from 'react-minimap';
import { Brick } from './Brick.section';

export const MiniMapped = ({
  zoomLevel,
  allDimensions,
  surveys,
  isMoreThanLargeMobile,
  isLessThanMobile,
}) => {
  const [localSurveys, setLocalSurveys] = useState([]);
  useEffect(() => {
    setLocalSurveys(surveys);
  }, []);

  const renderChild = ({ width, height, left, top, node }) => {
    // Get Headline
    const headlineBase =
      node?.children[0]?.innerHTML === 'View results'
        ? node?.children[1]?.innerHTML
        : node?.children[0]?.innerHTML;
    const headlineWithoutHeight = headlineBase?.replace(
      'height',
      'height="80px" '
    );
    const headlineWithoutWidth = headlineWithoutHeight?.replace(
      'width',
      'width="100%" '
    );
    const headline = headlineWithoutWidth;
    // Get Cover Image
    const coverBase =
      node?.children[0]?.innerHTML === 'View results'
        ? node?.children[2]?.innerHTML
        : node?.children[1]?.innerHTML;
    const coverWithoutHeight = coverBase?.replace('height', '');
    const coverWithoutWidth = coverWithoutHeight?.replace(
      'width=',
      'width="10%"'
    );
    const cover = coverWithoutWidth;
    // Online Since
    const onlineSinceBase =
      node?.children[0]?.innerHTML === 'View results'
        ? node?.children[3]?.innerHTML
        : node?.children[2]?.innerHTML;

    // Load More Button
    const loadMore = node?.children[0]?.innerText === 'Load More';
    const innerHTML = (
      <div
        style={{
          position: 'absolute',
          userSelect: 'none',
          width,
          height,
          left,
          top,
          padding: '0.5px',
          background: loadMore ? 'transparent' : '#0E161F',
        }}
        className="flex flex-col justify-between"
      >
        {loadMore ? (
          <>
            {localSurveys?.results?.length > 93 ? (
              <div
                className="flex items-center justify-center"
                style={{ zoom: 0.2 }}
              >
                <p
                  className="text-center rounded-[8px] px-[8px] py-[0px] mb-0"
                  style={{
                    border: '1px solid rgba(255, 255, 255, 0.8)',
                    color: 'rgba(255, 255, 255, 0.8)',
                  }}
                >
                  Load More
                </p>
              </div>
            ) : (
              <></>
            )}
          </>
        ) : (
          <>
            {/* Headline */}
            <div
              className="flex items-center justify-start"
              style={{ zoom: 0.06 }}
              dangerouslySetInnerHTML={{ __html: headline }}
            ></div>
            {/* Cover Image */}
            <div
              className="flex items-center justify-center"
              style={{ zoom: 0.08 }}
              dangerouslySetInnerHTML={{ __html: cover }}
            ></div>
            {/* Online Since */}
            <div
              className="flex items-center justify-end"
              style={{ zoom: 0.1 }}
              dangerouslySetInnerHTML={{ __html: onlineSinceBase }}
            ></div>
          </>
        )}
      </div>
    );

    return innerHTML;
  };

  return (
    <>
      {!localSurveys?.results?.length ? null : (
        <Minimap
          selector=".tab-card-main"
          className={`viewport viewport-zoom-${zoomLevel} `}
          keepAspectRatio={false}
          height={isMoreThanLargeMobile ? 190 : 190 * 0.7}
          width={isMoreThanLargeMobile ? 270 : 270 * 0.7}
          childComponent={renderChild}
        >
          <div
            className={`
            px-[20px] mt-[150px] content grid grid-cols-[repeat(21,1fr)] hide-scrollbar`}
            style={{
              width: `${Math.round(allDimensions?.totalWidth) - 20}px`,
              gap: Math.round(allDimensions?.gap),
              maxHeight: `calc(100vh - ${
                isMoreThanLargeMobile ? '175px' : '150px'
              })`,
              minHeight: `calc(100vh - ${
                isMoreThanLargeMobile ? '175px' : '150px'
              })`,
            }}
          >
            {localSurveys?.results?.map((survey) => {
              const { size } = survey;
              const type =
                size === 'Small'
                  ? 'oneThird'
                  : size === 'Medium'
                  ? 'twoThird'
                  : 'full';
              return (
                <Brick
                  key={survey?.gid + Math.random()}
                  survey={survey}
                  zoomLevel={zoomLevel}
                  allDimensions={allDimensions}
                  type={type}
                  closed={survey?.closed_at}
                  brick={survey.brick}
                />
              );
            })}
            <div
              className="bg-black flex text-white relative mt-[40px] pb-[20px] tab-card-main"
              style={{ gridColumn: 'span 21 / span 21' }}
            >
              <div
                className={`sticky left-[50vw] h-[52px] items-center cursor-pointer px-[20px] py-[10px] rounded-[8px] ${
                  localSurveys?.results?.length > 93 ? 'flex' : 'hidden'
                }`}
                style={{
                  transform: 'translate(-50%, -40%)',
                  border: '1px solid rgba(255, 255, 255, 0.8)',
                  color: 'rgba(255, 255, 255, 0.8)',
                }}
              >
                Load More
              </div>
            </div>
          </div>
        </Minimap>
      )}
    </>
  );
};
