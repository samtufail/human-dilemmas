import { Modal } from 'antd';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { unverified, verified } from 'utils/data/home/what';

export const InfoModal = ({ open, setOpen }) => {
  const user = useSelector((state) => state?.auth?.user);
  return (
    <Modal
      open={open}
      onCancel={() => setOpen(false)}
      centered
      className="custom-modal"
    >
      <h1 className="text-center text-white font-semibold mb-[12px] anony-modal-heading">
        {!user ? 'Anonymity' : 'Anonymity for signed-in users'}
      </h1>
      <div className="anony-modal-text">
        Whenever you visit a website on the internet you never know which data
        of yours is being registered. We want to be transparent. So we’ve listed
        it for you here. It felt like this was the most human thing to do.
        <br />
        <br />
        <Table user={user} />
        <br />
        So instead of writing 170 pages of Terms and Conditions, we’ve converted
        all our data tracking decisions into just one number. This number is
        currently {user ? '62%' : '82%'}. We call it your anonymity score, and
        you can see how it is calculated&nbsp;
        <Link href="https://docs.google.com/spreadsheets/d/1pUW7mquYuvzdlPjSeyNtsPkSX0kHOejFHE5UYHEAByw/edit#gid=0">
          <span className="underline cursor-pointer">here.</span>
        </Link>
        <br />
        {/* <br />
        We know your personal information is valuable, and we take our
        responsibility to protect it seriously. Rest assured that we designed
        the platform with your best interests in mind. We are committed to
        creating a safe and comfortable space where you can get meaningful
        results and know yourself better without fearing privacy breaches.
        <br />
        <br />
        We also want to provide full transparency in calculating our anonymity
        score, a crucial aspect of our platform. To achieve this, the above is a
        breakdown of how we calculated the anonymity score, so you know that we
        always protect your privacy. */}
      </div>
      <div className="flex mt-[20px]">
        <Link href="/contact-us">
          <p
            className="bg-boxtext text-white w-full rounded-[16px] text-sm lg:text-lg font-[400] md:font-[500] h-[45px] justify-center items-center"
            style={{ display: 'flex' }}
          >
            Send us Feedback
          </p>
          {/* cursor-pointer bg-boxtext text-white hover:text-white mb-[0px] w-full h-[48px] capitalize border-0 rounded-[16px] font-semibold text-sm sm:text-lg text-center */}
        </Link>
      </div>
    </Modal>
  );
};

const Table = ({ user }) => {
  const [data, setData] = useState([]);
  useEffect(() => {
    if (user) {
      setData(verified);
    } else {
      setData(unverified);
    }
  }, [user]);

  return (
    <table className="darkTable">
      <tbody>
        {data?.map((value) => {
          return (
            <tr key={value.question} className="anony-table-text">
              <td>{value.question}</td>
              <td>{value.answer === 'Yes' ? value.answer : <></>}</td>
              <td>{value.answer === 'No' ? value.answer : <></>}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};
