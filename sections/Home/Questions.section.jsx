import { CTAButton } from 'components';
import { shuffle } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { api } from 'utils';
import moment from 'moment';
import Countdown from 'react-countdown';

const formatDate = (date) => {
  const superSc = moment(date).format('Do');
  const superScript = superSc.slice(-2);
  return {
    superScript,
    day: moment(date).format('DD'),
    monthYear: moment(date).format('MMM YYYY'),
  };
};

const DateComponent = ({ date }) => {
  return (
    <>
      {date?.day}
      <sup>{date?.superScript}</sup>&nbsp;{date?.monthYear}
    </>
  );
};

const Box = ({ survey, className }) => {
  const router = useRouter();

  // set release relevant properties
  const [isFutureSurvey, setIsFutureSurvey] = useState(false);
  useEffect(() => {
    const isFutureSurvey = moment().isBefore(moment(survey?.date_published));
    setIsFutureSurvey(isFutureSurvey);
  }, []);

  // Renderer callback with condition
  const renderer = ({ days, hours, minutes, seconds, completed }) => {
    if (completed) {
      setIsFutureSurvey(false);
      // Render a completed state
      return (
        <>
          {survey?.closed_at ? 'Closed' : 'Online since'} <br />{' '}
          {survey?.closed_at ? (
            <DateComponent date={formatDate(survey?.closed_at)} />
          ) : (
            <DateComponent
              key={345}
              date={formatDate(survey?.date_published)}
            />
          )}
        </>
      );
    } else if (days > 0) {
      return (
        <>
          {survey?.closed_at ? 'Closed' : 'Available at'} <br />{' '}
          {survey?.closed_at ? (
            <DateComponent date={formatDate(survey?.closed_at)} />
          ) : (
            <DateComponent
              key={345}
              date={formatDate(survey?.date_published)}
            />
          )}
        </>
      );
    } else {
      // Render a countdown
      return (
        <>
          {survey?.closed_at ? 'Closed' : 'Available in'} <br />{' '}
          {survey?.closed_at ? (
            <DateComponent date={formatDate(survey?.closed_at)} />
          ) : (
            <>
              {hours < 10 ? `0${hours}` : hours}:
              {minutes < 10 ? `0${minutes}` : minutes}:
              {seconds < 10 ? `0${seconds}` : seconds}
            </>
          )}
        </>
      );
    }
  };

  return (
    <div
      className={`bg-black-survey homepage_question_box md:w-1/2 lg:homepage_question_box aspect-[300/220] select-none hover:bg-[#262440] active:bg-[#574081] relative justify-between p-[16px] flex flex-col rounded-[5px] cursor-pointer ${
        survey?.closed_at ? 'bg-[#262D35]' : ''
      } ${
        isFutureSurvey
          ? 'bg-[#262D35] cursor-not-allowed hover:bg-[#262D35] active:bg-[#262D35]'
          : ''
      } ${className}`}
      onClick={() =>
        isFutureSurvey ? null : router.push(`/${survey?.name}/${survey?.gid}`)
      }
    >
      {!survey ? (
        <></>
      ) : (
        <>
          <div className="w-[fit-content]">
            <img src={survey?.cover_headline} alt="Sex" className="w-[30%]" />
          </div>
          <div
            className="w-[fit-content] absolute flex justify-center"
            style={{
              top: '50%',
              left: '50%',
              transform: 'translate(-50%, -50%)',
            }}
          >
            <img src={survey?.cover_image} alt="Sex" className="w-[50%]" />
          </div>
          <div className="w-full flex justify-end">
            <p className="text-[#b4b4b4] text-right text-[12px]">
              <>
                {isFutureSurvey ? (
                  <Countdown
                    date={survey?.date_published}
                    key={456}
                    renderer={renderer}
                  />
                ) : (
                  <>
                    {survey?.closed_at ? 'Closed' : 'Online since'} <br />{' '}
                    {survey?.closed_at ? (
                      <DateComponent date={formatDate(survey?.closed_at)} />
                    ) : (
                      <DateComponent
                        date={formatDate(survey?.date_published)}
                      />
                    )}
                  </>
                )}
              </>
            </p>
          </div>
          {/* <div className="w-full flex justify-end">
            <div className="text-[12px]">
              {survey?.closed_at ? 'Closed' : 'Online since'} <br />{' '}
              {survey?.closed_at ? (
                <DateComponent date={formatDate(survey?.closed_at)} />
              ) : (
                <DateComponent date={formatDate(survey?.release_date)} />
              )}
            </div>
          </div> */}
        </>
      )}
    </div>
  );
};

export const Questions = () => {
  const [surveys, setSurveys] = useState([]);
  const router = useRouter();

  // Randomize Surveys Each time Homepage is loaded
  useEffect(() => {
    (async () => {
      try {
        const res = await api.get(`/v1/surveys/`);
        const filtered = res?.data?.results?.filter(
          (survey) =>
            survey?.status !== 'Closed' &&
            survey?.cover_image !== null &&
            survey?.release_date !== null
        );
        const allSurveys = shuffle(filtered);
        const firstTwo = [allSurveys?.[0], allSurveys?.[1]];
        setSurveys(firstTwo);
      } catch (err) {
        // console.log(err);
      }
    })();
  }, []);

  return (
    <div className="w-full pt-[40px] sm:pt-[112px] sm:pb-[112px] grid questionBoxHomepage lg:grid-cols-2 gap-[40px] items-start pl-[25px] xxs:pl-[40px] sm:pl-[64px]">
      <div className="flex flex-col gap-[40px] mr-[25px] xxs:mr-[40px] sm:mr-[64px]">
        <div>
          <h4 className="text-white text-[28px] sm:text-[34px] xl:text-[40px] question_section_question">
            Questions
          </h4>
          <p className="mt-0 mb-0 font-light text-[18px] leading-6 sm:leading-normal sm:text-[20px] xl:text-[24px] question_section_desc">
            Here you&apos;ll find questions about a variety of themes, including
            sex, drugs and tax break schemes.
          </p>
        </div>
        <div className="hidden lg:flex items-stretch justify-end">
          <div
            className = "justify-center bg-[#845EC2] hover:bg-[#583d85] font-semibold text-[14px] sm:text-[16px] transition-all flex items-center cursor-pointer py-[10px] rounded-[16px] select-none"
            style={{ width: '130px' }}
            onClick={() => router.push('/questions')}
          >
            Explore
          </div>
          {/* <CTAButton width="145px" onClick={() => router.push('/questions')}>
            Explore
          </CTAButton> */}
        </div>
      </div>
      <div className="flex pr-[25px] xxs:pr-[40px] sm:pr-0 overflow-x-scroll scrollbar-none gap-[32px]">
        {surveys?.map((survey, index) => (
          <Box
            key={survey?.gid}
            survey={survey}
          />
        ))}
      </div>
      <div className="w-full flex sm:justify-end lg:hidden bg-red-5">
        <div className="w-full sm:w-[200px] pr-[25px] xxs:pr-[40px] h-[10px]">
          <CTAButton onClick={() => router.push('/questions')}>
            Explore
          </CTAButton>
        </div>
      </div>
    </div>
  );
};
