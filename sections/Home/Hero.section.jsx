import { CTAButton } from 'components/Buttons';
import Link from 'next/link';
import { useRouter } from 'next/router';

export const Hero = () => {
  const router = useRouter();
  return (
    <>
      <div className="w-full mt-[40px] grid grid-cols-1 items-center px-[25px] xxs:px-[45px] md:px-[68px] sm:min-h-[45vh]">
        <div className="flex flex-col items-start">
          <div className="max-w-[523px]">
            <img
              className="w-full md:w-[523px] scale-[1.15] sm:scale-100 hero_image_homepage"
              src="/svg/home/headline.svg"
              alt="Human Dilemmas is a mirror that gives you the possibility to compare yourself with other humans."
            />
            <div className="flex justify-end items-center mt-[60px] sm:mt-[24px]">
              <Link href="/questions">
                <CTAButton onClick={() => router.push('/questions')}>Look in the mirror</CTAButton>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};