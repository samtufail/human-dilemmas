import Image from 'next/image';
import { what } from 'utils';
import { InfoModal } from './InfoModal.section';
import { useState } from 'react';
import { useSelector } from 'react-redux';

const InfoBox = ({ img, title, description }) => {
  return (
    <div className="what_section_wrapper_homepage grid grid-cols-[77px_auto] gap-[16px] sm:min-w-[340px] lg:min-w-[400px] lg:max-w-[400px] 2xl:max-w-[370px] items-start">
      <div className="relative flex items-center justify-center md:justify-start scale-105 sm:scale-100 what_section_icon_homepage">
        <Image width="77px" height="77px" src={img} alt={description} />
      </div>
      <div className="flex flex-col items-start h-full justify-center">
        <div className="text-[18px] what_section_title_homepage leading-4 lg:leading-6 sm:text-[20px] text-[#fefefe] font-semibold text-left mb-[4px] sm:mb-[7px]">
          {title}
        </div>
        <div className="font-[300] what_section_description_homepage text-white leading-5 text-[14px] sm:text-[16px] text-left">
          {description}
        </div>
      </div>
    </div>
  );
};

export const What = () => {
  const [open, setOpen] = useState(false);
  const user = useSelector((state) => state?.auth?.user);
  return (
    <div className="mt-[70px] sm:mt-[85px] bg-[#4C475D] py-[48px] relative">
      <InfoModal open={open} setOpen={setOpen} />
      <div className="text-white text-[40px] text-center mb-[56px] px-[20px] xxs:px-[30px] sm:px-[0] scale-90 sm:scale-90 md:scale-100 origin-left">
        <img
          src="/svg/home/what.svg"
          alt="What is Human Dilemmas?"
          className="w-full md:w-[481px]"
        />
      </div>

      <div className="px-[25px] xxs:px-[40px] sm:px-[60px] 2xl:px-[110px] grid grid-cols-1 md:grid-cols-2 2xl:grid-cols-3 gap-x-[10px] gap-y-[52px] mb-[146px] justify-items-start">
        {what?.map((el) => {
          let data = {};
          if (el.id === '1a6') {
            // Handle Anonimity
            data.id = el.id;
            data.title = el.title;
            data.img = el.img;
            data.description = (
              <>
                You are currently {`${user ? '62%' : '82%'}`} anonymous on the
                app. Find out more&nbsp;
                <span
                  className="underline cursor-pointer"
                  onClick={() => setOpen(true)}
                >
                  here.
                </span>
              </>
            );
          } else {
            data = el;
          }
          return (
            <InfoBox
              key={data?.id}
              img={data?.img}
              title={data?.title}
              description={data?.description}
            />
          );
        })}
      </div>
      <div className="absolute bottom-[36px] right-[40px]">
        <div className="relative flex items-center px-5">
          <Image
            className=""
            width={265}
            height={75}
            src="/svg/home/free.svg"
            alt="Everything is free of cost here."
          />
        </div>
      </div>
    </div>
  );
};
