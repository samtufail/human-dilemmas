import { useEffect, useState } from 'react';
import { api, countryCodes, getSeparatedNumber } from 'utils';
import { useDispatch, useSelector } from 'react-redux';
import { setCountryCode } from 'store/global';
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router';
import { loggedInUser } from 'store';
import { toast } from 'react-toastify';
import { DeleteModal } from './DeleteModal.section';
import { CustomPhoneInput, Loader } from 'components';
import { phone as validator } from 'phone';

export const YourProfile = ({ setLoading, setActive }) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const [name, setName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [label2, setLabel2] = useState(true);
  const [showDel, setShowDel] = useState(false);
  const [del, setDel] = useState(false);
  const [error, setError] = useState(false);

  const user = useSelector((state) => state?.auth?.user);
  const { countryCode } = useSelector((state) => state?.global);

  // Finding Country Code;

  useEffect(() => {
    const phone = user?.phone_number;
    const name = user?.name;

    if (phone) {
      const separatedNumber = getSeparatedNumber(phone);
      dispatch(setCountryCode(separatedNumber?.countryCode));
      setPhoneNumber(separatedNumber?.phoneNumber);
    }

    if (name) {
      setName(name);
    }

    const timer = setTimeout(() => {
      if (!user) {
        router.push('/');
      }
    }, 2000);

    return () => clearTimeout(timer);
  }, [user]);

  const submitFn = async () => {
    const storage = globalThis?.sessionStorage;
    const prevPath = storage.getItem('prevPath');
    const forwardLink =
      prevPath && prevPath?.includes('questions') ? '/questions' : '/';
    const separated = getSeparatedNumber(user?.phone_number);

    const fullNumber = `${countryCode}${phoneNumber}`.trim();
    const result = validator(fullNumber, {
      country: countryCodes?.find((code) => code?.dial_code === countryCode)
        ?.code,
    });
    if (!result?.isValid) {
      if (!phoneNumber) {
        setError('Error: Invalid phone number.');
      } else {
        setError(`Error: "${phoneNumber}" is not a valid phone number.`);
      }
    } else {
      // User Only Changes his Number
      if (
        (separated?.phoneNumber !== phoneNumber ||
          separated?.countryCode !== countryCode) &&
        user?.name === name
      ) {
        // Logic to Update Phone Number if user only changes his phone number
        try {
          const number = `${countryCode}${phoneNumber}`.trim();

          const res = await api.post('/v1/user/change-phone-number-request/', {
            phone_number: number,
          });
          if (res?.status === 200) {
            setActive('verification');
            localStorage.setItem('newNumber', number);
          }
        } catch (e) {
          const { data } = e?.response;
          if (data?.non_field_errors?.length) {
            data?.non_field_errors?.forEach((error) => {
              if (
                error ===
                'An account on this Phone number already exists, please try to login with this phone number'
              ) {
                toast.error(
                  'An account is already registered on this phone number.'
                );
              } else {
                toast.error(error);
              }
            });
          }
        }
      }
      // User Only Changes his name
      else if (
        separated?.phoneNumber === phoneNumber &&
        separated?.countryCode === countryCode &&
        user?.name !== name
      ) {
        // Logic to update name if user only changes his name
        setLoading(true);
        try {
          const res = await api.put('/v1/user/update/', {
            name: name,
          });
          if (res?.status === 200) {
            toast.success('...and just like that you switched identity.');
            router.push(forwardLink);
          } else {
            toast.error(
              'There was an error updating your profile. Please try again.'
            );
            setName(user?.name);
            setLoading(false);
          }
        } catch (e) {
          // console.log(e);
        }
      }
      // User Changes his Number and Name
      else if (
        (separated?.phoneNumber !== phoneNumber ||
          separated?.countryCode !== countryCode) &&
        user?.name !== name
      ) {
        // Logic to Update Name
        setLoading(true);
        try {
          await api.put('/v1/user/update/', {
            name: name,
          });
          await dispatch(loggedInUser());
        } catch (error) {
          toast.error(
            'There was an error updating your profile. Please try again.'
          );
          setName(user?.name);
        }
        // Logic to Update Phone Number
        try {
          const number = `${countryCode}${phoneNumber}`.trim();
          await api.post('/v1/user/change-phone-number-request/', {
            phone_number: number,
          });
          setActive('verification');
          toast.success('Just one more step to configure your new number.');
          localStorage.setItem('newNumber', number);
        } catch (e) {
          const { data } = e?.response;
          if (data?.non_field_errors?.length) {
            data?.non_field_errors?.forEach((error) => {
              if (
                error ===
                'An account on this Phone number already exists, please try to login with this phone number'
              ) {
                toast.error(
                  'An account is already registered on this phone number.'
                );
              } else {
                toast.error(error);
              }
            });
          }
        }
        setLoading(false);
      } else {
        router.push(forwardLink);
      }
    }
  };

  if (!user) {
    return (
      <div className="w-screen h-[400px] flex items-center justify-center">
        <Loader />
      </div>
    );
  }

  return (
    <div className="flex flex-col justify-start items-start px-4">
      <DeleteModal visible={del} setVisible={setDel} />
      <div className="bg-tabbg p-4 md:py-[25px] md:px-[32px] rounded-[16px] relative max-w-[547px] mx-auto w-full">
        <div className="flex flex-col justify-between">
          <h1 className="font-[400] text-[22px] md:text-[30px] uppercase text-[#fff] mb-[16px] md:mb-[20px] leading-[33px] md:leading-[1]">
            Your Profile
          </h1>
          {/* <div
            className="cursor-pointer text-3xl"
            onClick={() => router.back()}
          >
            <AiOutlineClose />
          </div> */}
        </div>

        {/* Name Input */}
        <div className="relative mb-[24px]">
          <label
            htmlFor="user"
            className={`font-medium pt-[5px] text-[10px] text-textGray transition-all duration-200 ease-in absolute flex pl-[15px]`}
          >
            User
          </label>
          <div className="mt-1">
            <input
              id="user"
              name="user"
              type="text"
              placeholder="Enter your name"
              required
              onChange={(e) => setName(e?.target?.value)}
              value={name}
              className={`pt-[24px] placeholder:textGray text-textLogin bg-white  border-white focus:border-boxtext  border-2 focus:shadow-inputShadow block w-full h-[52px] appearance-none rounded-md  px-3 py-4  shadow-sm  focus:outline-none text-[15px] placeholder-[15px]`}
            />
          </div>
        </div>
        {/* Phone Input */}
        <CustomPhoneInput
          phoneNumber={phoneNumber}
          setPhoneNumber={setPhoneNumber}
          showLabel={label2}
          setShowLabel={setLabel2}
          passwordType
          phoneInputClassName={'phone-input'}
          phoneFlagClassName={'phone-flag'}
        />
        {/* Error */}
        <div className="mb-[10px] mt-[3px] md:mb-[10px] md:mt-[4px] flex items-center justify-end">
          <p className="text-[9px] md:text-[13px]">
            {error ? error : <>&nbsp;</>}
          </p>
        </div>
        {/* Submit Button */}
        <div className="flex items-center justify-center">
          <button
            type="button"
            onClick={submitFn}
            className="bg-boxtext cursor-pointer text-white placeholder:font-normal mb-[16px] md:mb-[20px] md:max-w-[266px] w-full h-[48px] leading-7 capitalize border-0 items-center flex justify-center rounded-[16px] font-[500] text-[16px] md:text-lg"
          >
            Save Changes
          </button>
        </div>

        {/* Reset Password Link */}
        <p className="font-[300] text-[13px] md:text-[15px] text-center">
          Reset password?{' '}
          <span
            className="underline cursor-pointer"
            onClick={async () => {
              try {
                await api.post('/v1/user/forgot-password/', {
                  phone_number: user?.phone_number,
                });
                localStorage.setItem('phoneNumber', user?.phone_number);
                router.push('/reset-password/my-profile');
              } catch (error) {
                localStorage.removeItem('phoneNumber');
                if (error?.response?.data?.non_field_errors?.length) {
                  toast.error(error?.response?.data?.non_field_errors[0]);
                }
              }
            }}
          >
            Click here.
          </span>
        </p>
      </div>

      {/* Delete Account Link */}
      <div
        className="flex items-center gap-[10px] mt-[10px] md:mt-[10px] cursor-pointer text-[10px] md:text-[14px] font-normal leading-4"
        onClick={() => setShowDel((prev) => !prev)}
      >
        <p>Don&apos;t want to be part of the humans community anymore?</p>
        {showDel ? <UpOutlined /> : <DownOutlined />}
      </div>

      {/* Button To Remove Account */}
      {showDel ? (
        <button
          type="button"
          onClick={() => setDel(true)}
          className="bg-[#FD3131] cursor-pointer text-white placeholder:font-normal mb-[25px] md:max-w-[160px] w-full h-[30px] capitalize border-0 items-center flex justify-center rounded-[16px] font-[500] text-[14px] mt-[16px]"
        >
          Delete my profile
        </button>
      ) : (
        <></>
      )}
    </div>
  );
};
