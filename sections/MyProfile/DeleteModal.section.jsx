import { CloseOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { logout } from 'store';
import { api } from 'utils';

export const DeleteModal = ({ visible, setVisible }) => {
  const [text, setText] = useState('');
  const dispatch = useDispatch();
  const router = useRouter();
  return (
    <Modal
      open={visible}
      onCancel={() => setVisible(false)}
      bodyStyle={{
        padding: 25,
        background: '#3F3B4E',
        border: '1px solid #6E7378',
        borderRadius: '16px',
      }}
      className="max-w-[337px] top-[28%]"
      closeIcon={<CloseOutlined style={{ color: '#fff' }} />}
    >
      <div className="text-center flex items-center justify-center flex-col gap-[30px]">
        <h2 className="text-white text-[22px] font-[300] mt-[32px]">
          Type “delete” to confirm.
        </h2>

        <form
          onSubmit={async (e) => {
            e?.preventDefault();
            if (text === 'delete') {
              const res = await api.delete('/v1/user/delete/');
              if (res?.status === 204) {
                dispatch(logout());
                toast.success('Profile deleted successfully!');
                router.push('/');
              } else {
                toast.error('There was an error in deleting your profile!');
              }
            } else {
              toast.error("Please type 'delete' to confirm");
            }
          }}
          className="w-full"
        >
          <input
            type="text"
            id="first_name"
            style={{
              outline: 'none',
              border: '2px solid #fff',
              textAlign: 'center',
            }}
            onChange={(e) => setText(e?.target?.value)}
            value={text}
            className="bg-[#3f3b4e] text-white rounded-[16px] text-[20px] w-full p-[8px]"
            placeholder="delete"
            required
          />

          <div className="flex items-center justify-center mt-[30px]">
            <div className="flex flex-wrap items-center justify-center gap-[49px]">
              <button
                type="button"
                style={{ outline: 'none', border: 'none' }}
                onClick={() => setVisible(false)}
                className="min-w-[118px] h-[48px] flex items-center justify-center bg-[#3f3b4e] text-white text-[18px] cursor-pointer"
              >
                Cancel
              </button>
              <button
                type="submit"
                style={{ outline: 'none', border: 'none' }}
                className="min-w-[118px] h-[48px] flex items-center justify-center bg-[#FD3131] rounded-[16px] text-white text-[18px] cursor-pointer"
              >
                Confirm
              </button>
            </div>
          </div>
        </form>
      </div>
    </Modal>
  );
};
