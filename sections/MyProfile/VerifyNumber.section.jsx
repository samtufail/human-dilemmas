import { Field, Form, Formik } from 'formik';
import Link from 'next/link';
import { useState } from 'react';
import Countdown from 'react-countdown';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { loggedInUser } from 'store';
import { api } from 'utils';

const Back = () => {
  return (
    <svg
      width="12"
      height="20"
      viewBox="0 0 12 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <mask id="path-1-inside-1_3205_5734" fill="white">
        <path d="M12.0005 1.77L10.2305 0L0.230469 10L10.2305 20L12.0005 18.23L3.77047 10L12.0005 1.77Z" />
      </mask>
      <path
        d="M12.0005 1.77L10.2305 0L0.230469 10L10.2305 20L12.0005 18.23L3.77047 10L12.0005 1.77Z"
        fill="white"
      />
      <path
        d="M12.0005 1.77L15.536 5.30553L19.0715 1.77L15.536 -1.76553L12.0005 1.77ZM10.2305 0L13.766 -3.53553L10.2305 -7.07107L6.69493 -3.53553L10.2305 0ZM0.230469 10L-3.30507 6.46447L-6.8406 10L-3.30507 13.5355L0.230469 10ZM10.2305 20L6.69493 23.5355L10.2305 27.0711L13.766 23.5355L10.2305 20ZM12.0005 18.23L15.536 21.7655L19.0715 18.23L15.536 14.6945L12.0005 18.23ZM3.77047 10L0.234935 6.46447L-3.3006 10L0.234935 13.5355L3.77047 10ZM15.536 -1.76553L13.766 -3.53553L6.69493 3.53553L8.46494 5.30553L15.536 -1.76553ZM6.69493 -3.53553L-3.30507 6.46447L3.766 13.5355L13.766 3.53553L6.69493 -3.53553ZM-3.30507 13.5355L6.69493 23.5355L13.766 16.4645L3.766 6.46447L-3.30507 13.5355ZM13.766 23.5355L15.536 21.7655L8.46493 14.6945L6.69493 16.4645L13.766 23.5355ZM15.536 14.6945L7.306 6.46447L0.234935 13.5355L8.46493 21.7655L15.536 14.6945ZM7.306 13.5355L15.536 5.30553L8.46493 -1.76553L0.234935 6.46447L7.306 13.5355Z"
        fill="#010202"
        mask="url(#path-1-inside-1_3205_5734)"
      />
    </svg>
  );
};

export const VerifyNumber = ({ setActive }) => {
  const [error, setError] = useState(false);
  const [countdownKey, setCountDownKey] = useState(41547);
  const [tries, setTries] = useState(0);

  // Renderer callback with condition
  const renderer = ({ minutes, seconds, completed }) => {
    if (completed) {
      // Render a completed state
      return (
        <>
          <p
            className="text-[#010202] cursor-pointer underline text-[15px] font-[300]"
            onClick={async () => {
              const phone = localStorage.getItem('newNumber');
              try {
                await api.post('/v1/user/change-phone-number-request/', {
                  phone_number: phone,
                });
                toast.success('We sent you a new SMS with a code.');
                setTries((prev) => prev + 1);
                if (error) {
                  setError(false);
                }
              } catch (e) {
                setError('Operation failed, please go back and try again.');
                setTries(2);
              } finally {
                setCountDownKey((key) => key + 547);
              }
            }}
          >
            I didn&apos;t receive the code
          </p>
        </>
      );
    } else {
      // Render a countdown
      return (
        <span className="text-[#888493] text-[18px] font-[300] leading-[1]">
          {minutes}:{seconds < 10 ? `0${seconds}` : seconds}
        </span>
      );
    }
  };

  const dispatch = useDispatch();

  return (
    <div className=" flex min-h-full flex-col justify-center py-12 sm:px-6 lg:px-8 relative">
      <div className="bg-[#E6DFF3] rounded-[8px] mt-8 pt-[32px] px-[24px] pb-[55px] max-w-[547px] mx-auto w-full">
        <div className="grid grid-cols-[40px_auto]">
          {/* Arrow */}
          <div>
            <div
              className="cursor-pointer"
              onClick={() => {
                localStorage.removeItem('newNumber');
                setActive('your-profile');
              }}
            >
              <Back />
            </div>
          </div>
          {/* Other Content */}
          <div className="max-w-[420px]">
            <h2 className="text-[24px] text-dark leading-[0.85] font-[300]">
              Phone Verification
            </h2>
            <>
              {error ? (
                <p className="mt-[15px] text-[#6E7378] text-[16px] font-[300] mb-[33px]">
                  {error}
                </p>
              ) : (
                <p className="mt-[15px] text-[#6E7378] text-[16px] font-[300] mb-[33px]">
                  We just sent you an SMS with a confirmation code.
                </p>
              )}
            </>
            <Formik
              initialValues={{ code: '' }}
              onSubmit={async (values) => {
                const phone = localStorage.getItem('newNumber');
                const verifyBody = {
                  phone_number: phone,
                  otp: values?.code,
                };
                try {
                  await api.put(
                    '/v1/user/change-phone-number-confirm/',
                    verifyBody
                  );
                  toast.success('Phone Number Updated Successfully!');
                  await dispatch(loggedInUser());
                  setActive('your-profile');
                } catch (e) {
                  setError(
                    'The code you entered is incorrect — please try again.'
                  );
                }
              }}
            >
              <Form>
                <Field
                  name="code"
                  type="text"
                  placeholder="Enter verification code"
                  className="outline-none appearance-none bg-white border-none rounded-[4px] w-full h-[52px] px-[12px] text-dark placeholder:text-[#6E7378]"
                />
                <div className="mt-[45px] flex items-center justify-between">
                  <button
                    type="submit"
                    className="bg-boxtext cursor-pointer text-white  h-[48px] w-[136px] leading-7 capitalize border-0 items-center flex justify-center rounded-[16px] font-[500] text-lg"
                  >
                    Next
                  </button>
                  {tries >= 2 ? (
                    <p className="text-[#888493] cursor-pointer  underline text-[15px] font-[300] pointer-events-none">
                      I didn&apos;t receive the code
                    </p>
                  ) : (
                    <Countdown
                      date={Date.now() + 35000}
                      key={countdownKey}
                      renderer={renderer}
                    />
                  )}
                </div>
              </Form>
            </Formik>
          </div>
        </div>
      </div>
      {tries >= 2 ? (
        <div className="relative text-white bottom-[-5px] text-right w-[547px] mx-auto">
          Didn&apos;t work?{' '}
          <Link href="/contact-us" passHref>
          <span className="cursor-pointer text-white hover:text-white inline underline hover:underline">
            Contact us here.
          </span>
        </Link>

        </div>
      ) : (
        <></>
      )}
    </div>
  );
};
