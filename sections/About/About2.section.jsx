import Link from 'next/link';
import { useRouter } from 'next/router';

export const About2 = () => {
  const router = useRouter();
  return (
    <div className="px-[10px] md:px-[69px] pb-[20px]">
      <h1 className="font-light text-[17px] xxs:text-[20px] text-white -mt-[10px] xl:max-w-[900px] xl:mx-auto">
        What is Human Dilemmas?
      </h1>
      <div className="flex flex-col items-center md:flex-row md:items-start rounded-[4px] relative bg-black-survey px-[20px] md:pl-[0px] md:pr-[50px] pt-[40px] gap-[30px] md:gap-[0px] lg:pr-[120px] pb-[40px] mt-[10px] min-w-[80vw] xxs:min-w-0 xl:max-w-[900px] xl:mx-auto">
        {/* Close Btn */}
        <div
          className="absolute top-[10px] md:top-[24px] right-[10px] sm:right-[24px] cursor-pointer"
          onClick={() => router.push('/')}
        >
          <img
            className="minified_icon"
            src="/svg/close.svg"
            alt="close button"
          />
        </div>
        <div className="flex w-[175px] h-[134px] md:w-full md:h-full justify-center">
          <img
            src="/img/about/weight.png"
            alt="illustration of what is humans?"
          />
        </div>
        <div>
          <div className="pr-[10px] xss:pr-0 text-right md:mt-[60px] font-light text-[12px] xxs:text-[17px] md:text-[20px] lg:text-[24px] space-y-[10px] md:space-y-[15px]">
            <p>
              Human Dilemmas explores human beliefs, perceptions, and problems.
            </p>
            <p>
              you compare yourself with other humans. It allows you to pass
              judgment on others and even yourself.
            </p>
            <p>
              It helps you to figure out what normal looks like. Are you normal
              or are you an outlier?
            </p>
            <span className="inline-flex">
              Click&nbsp;
              <Link href="/questions" passHref legacyBehavior>
                <a className="text-white underline hover:underline">here</a>
              </Link>
              &nbsp;to find out.
            </span>

          </div>
        </div>
      </div>
    </div>
  );
};
