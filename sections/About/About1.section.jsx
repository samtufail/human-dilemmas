import { useRouter } from 'next/router';

export const About1 = ({ setCurrent }) => {
  const router = useRouter();
  return (
    <div className="xs:px-[69px] px-[10px] pb-[20px]">
      <h1 className="font-light text-[17px] xxs:text-[20px] sm:text-[24px] text-white -mt-[10px]">
        What is Humans?
      </h1>
      <div className="flex flex-col items-center md:flex-row md:items-start rounded-[4px] relative bg-black-survey px-[20px] md:pl-0 md:pr-[50px] lg:pl-[89px] pt-[40px] gap-[20px] xxs:gap-[30px] md:gap-[0px] lg:pr-[120px] pb-[20px] xxs:pb-[40px] mt-[10px] min-w-[80vw] xxs:min-w-0">
        {/* Close Btn */}
        <div
          className="absolute top-[10px] md:top-[16px] lg:top-[24px] right-[10px] sm:right-[16px] lg:right-[24px] cursor-pointer"
          onClick={() => router.push('/')}
        >
          <img
            className="minified_icon"
            src="/svg/close.svg"
            alt="close button"
          />
        </div>
        <div className="flex flex-col w-[178px] h-[144px] md:w-full md:h-full items-center ml-6 md:ml-0">
          <img
            src="/svg/about/person.svg"
            alt="illustration of what is humans?"
          />
        </div>
        <div>
          <div className="pr-[6px] xss:pr-0 text-right mt-0 md:mt-[40px] font-light text-[12px] xxs:text-[17px] md:text-[20px] lg:text-[24px] space-y-[10px] md:space-y-[15px]">
            <p>
              The Humans project is an investigation of the human condition.
            </p>
            <p>
              It&apos;s about taboos, sex, drugs, and the paranormal, to name a
              few.
            </p>
            <p>
              Sometimes it&apos;s about the unspeakable. Things that might make
              you want to go to incognito mode.
            </p>
          </div>
          <p
            className="flex items-center justify-end gap-4 text-right mt-[25px] md:mt-[40px] font-light text-[12px] xxs:text-[17px] md:text-[20px] lg:text-[24px] cursor-pointer"
            onClick={() => setCurrent(2)}
          >
            About Human Dilemmas
            <img
              className="minified_icon"
              src="/svg/about/next.svg"
              alt="next-button"
            />
          </p>
        </div>
      </div>
    </div>
  );
};
